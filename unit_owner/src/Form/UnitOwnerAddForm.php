<?php

namespace Drupal\unit_owner\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class Unit_OwnerEditForm.
 *
 * @package Drupal\unit_owner\Form\UnitOwnerAddForm
 *
 * Substitutions:
 * Tblname. Replace with Unit_Owner (init cap).
 * tblname.  Replace with unit_owner.
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 */
class UnitOwnerAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unit_owner_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $unit_owner_list_id = '') {

    $form['add'] = [
      '#type'  => 'fieldset',
      '#title' => t('Add a unit_owner entry'),
      '#prefix' => '<div id="formadd" style="background-color:Cornsilk"',
      '#suffix' => '</div>',
    ];

    $trueint = (int) 1;
    $falseint = (int) 0;
    $form['add']['unit_owned_id'] = [
      '#type'  => 'select',
      '#title' => t('Unit Owned ID'),
      '#required' => TRUE,
      '#options'  => Routines::tableOptions('unit', 1, 7, TRUE),
      '#description' => t("The Unit Owned by the person or organization below"),
      '#prefix' => '<table border=0 cellspacing=3 celladding=3><tr><td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['unit_owner_type'] = [
      '#type' => 'select',
      '#title' => t('Unit Owner Type'),
      '#options' => [t('P'), t('O')],
      '#default_value' => t('P'),
      '#description' => t("Owner Type.  Values:P - Person (default),O - Organization."),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['owner_person_id'] = [
      '#type'  => 'select',
      '#title' => t('Owner Person Identifier'),
      '#options'  => Routines::tableOptions('person', 1, 4),
      '#empty_value' => '0',
      '#default_value' => $falseint,
      '#description' => t("The person owning the unit"),
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
      '#states' => [
        'disabled' => [
          ':input[name="unit_owner_type"]' => ['value' => t("O")],
        ],
        'required' => [
          ':input[name="unit_owner_type"]' => ['value' => t("P")],
        ],
      ],
    ];
    $form['add']['unit_owner_primary_ind'] = [
      '#type'  => 'select',
      '#title' => t('Unit Owner Primary Indicator'),
      '#options' => [$trueint => t('Y'), $falseint => t('N')],
      '#default_value' => 'Y',
      '#description' => t("Indicates a primary unit owner when multiple owners exist.
      This this data element could be used to indicate the primary point of contact
      for the management office. Values are Y - Yes (default), N - No."),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['unit_owner_org_id'] = [
      '#type'  => 'select',
      '#title' => t('Unit Owner Organization ID'),
      '#options' => Routines::tableOptions('organization', 1, 2),
      '#empty_value' => 0,
      '#default_value' => $falseint,
      '#description' => t("Unit Owner Identifier, if the owner is an organization"),
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
      '#states' => [
        'disabled' => [
          ':input[name="unit_owner_type"]' => ['value' => t("P")],
        ],
        'required' => [
          ':input[name="unit_owner_type"]' => ['value' => t("O")],
        ],
      ],
    ];
    $form['add']['unit_owner_interest'] = [
    // Textfield.
      '#type'  => 'hidden',
      '#title' => t('unit_owner_interest'),
      '#default_value' => 1,
      '#size'  => 20,
      '#description' => t("A decimal number indicating the degree 
      of interst of an owner if multiple owners exist for a single unit. This number  takes the 
      form of a fraction, and, the sum of these interests should be one for each 
      unit. Multiple owners are given equal shares by default.  For example, two  owners would 
      be given an interest of 0.5.  This fraction can be adjusted manually but the sum of these 
      fractions should sum to one for each unit."),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['unit_owner_proxy_id'] = [
      '#type'  => 'select',
      '#title' => t('Unit Owner proxy ID'),
      '#options'  => Routines::tableOptions('person', 1, 4),
      '#empty_value' => $falseint,
      '#description' => t("A person authorized to act on behalf of the owner 
and who serves as the community's point of contact for all business relating to the 
unit."),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];

    $form['add']['submit'] = [
      '#type'  => 'submit',
      '#value' => \t('Add'),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
$form['add']['cancel'] = [
      '#type'  => 'submit',
      '#value' => \t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
      '#prefix' => '<td>',
      '#suffix' => '</td></tr></table>',
    ];

    return $form;

  }

  /**
   * Validates a form for unit_owner (optional).
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');

    $row = [];
    
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {

      $row[$key] = $value;
    }
    
    $tid = $db->startTransaction();
    try {
      $db->insert('unit_owner')->fields($row)->execute();
      $this->messenger()->addMessage(t('Insert of %unit_owner_list_id for unit %unit_owned_id successful',
        [
          '%unit_owner_list_id' => $row['unit_owner_list_id'],
          '%unit_owned_id' => $row['unit_owned_id'],
        ]
              )
        );
    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $this->messenger()->addMessage(\t('Insert Failed. Nothing done. Error message
        follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('unit_owner.list');
  }

  /**
   * Cancels unit_owner form addition.
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('unit_owner.list');
  }

}
