<?php

namespace Drupal\unit_owner\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\DatabaseExceptionWrapper;
Use Drupal\Core\Url;

/**
 * Class DefaultForm.
 *
 * @package Drupal\unit_owner\Form\UnitOwnerDeleteForm
 *
 *  * Substitutions:
 * Tblname. Replace with Unit_Owner (init cap).
 * tblname.  Replace with unit_owner.
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 */
class UnitOwnerDeleteForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unit_owner_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('unit_owner.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $unit_owner_list_id = '') {

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Do you wish to delete the unit-owner list ID below? '),
      '#value' =>  $this->t("$unit_owner_list_id"),
      '#size' => 60,
      '#maxlength' => 128,
    ];
    
    return parent::buildForm($form, $form_state, $unit_owner_list_id = '');

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');

    $unit_owner_list_id = (integer) \Drupal::request()->attributes->get('unit_owner_list_id');

    // $unit_owner_list_id = -1;
    $tid = $db->startTransaction();
    try {
      $nbr_deleted = $db->delete('unit_owner')
        ->condition('unit_owner_list_id', $unit_owner_list_id, '=')
        // ->condition('nocol', 'xyz' , '=')
        ->execute();
      
      if ($nbr_deleted != 1) {
        $tid->rollBack();
       $this->messenger()->addError(
           $nbr_deleted .     $this->t(' rows deleted, 1 expected. Nothing done.'));
     }
      else {
      $this->messenger()->addMessage($this->t('Unit_Owner list item deleted.'));
     }
    }
    
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      
      $this->messenger()->addMessage($this->t('Delete Failed. Nothing done. Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('unit_owner.list');
  }

}
