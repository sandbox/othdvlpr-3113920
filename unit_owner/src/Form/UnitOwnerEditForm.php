<?php

namespace Drupal\unit_owner\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class UnitOwnerEditForm.
 *
 * @package Drupal\unit_owner\Form\UnitOwnerEditForm
 *
 * Substitutions:
 * Tblname. Replace with Unit_Owner (init cap).
 * tblname.  Replace with unit_owner.
 * Note:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 * @todo. Consider moving BuildingAddForm::table_option routines.
 */
class UnitOwnerEditForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unit_owner_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $unit_owner_list_id = '') {
    
    $db = Database::getConnection('default', 'default');
    $select = $db->select('unit_owner', 'e')
      ->fields('e')
      ->condition('e.unit_owner_list_id', $unit_owner_list_id, '=')
      ->execute();
    $row = $select->fetchAssoc();

    $trueint = (int) 1;
    $falseint = (int) 0;
    $rowarr = (array) Routines::tableOptions('unit', 1, 7);
    $form['unit_owner_list_id'] = [
      '#type' => 'number',
      '#disabled' => TRUE,
      '#title' => t('Choose entry to update'),
      '#value' => $row['unit_owner_list_id'],
      '#description' => t("Updated Unit Owner Identifier"),
      '#ajax' => [
        'wrapper' => 'updateform',
        'callback' => 'unit_owner_form_update_callback',
      ],
    ];
    $form['unit_owned_id'] = [
      '#type'  => 'select',
      '#title' => t('Unit Owned ID'),
      '#options'  => $rowarr,
      '#default_value' => $row['unit_owned_id'],
      '#description' => t("The Unit Owned by the person or organization below"),
    ];
    $form['unit_owner_type'] = [
      '#type' => 'select',
      '#title' => t('Updated Unit Owner Type'),
      '#options' => [t('P'),
        t('O'),
      ],
      '#default_value' => $row['unit_owner_type'],
      '#description' => t("Owner Type.  Values:P - Person (default),O - Organization."),
    ];

    $form['owner_person_id'] = [
      '#type' => 'select',
      '#title' => t('Updated Person Owner ID'),
      '#options'  => Routines::tableOptions('person', 1, 4),
      '#empty_option' => t('None'),
      '#empty_value' => '0',
      '#default_value' => $row['owner_person_id'],
      '#description' => t("The person owning the unit"),
      '#states' => [
        'disabled' => [
          ':input[name="unit_owner_type"]' => ['value' => t("O")],
        ],
        'required' => [
          ':input[name="unit_owner_type"]' => ['value' => t("P")],
        ],
      ],
    ];

    $form['unit_owner_primary_ind'] = [
      '#type' => 'select',
      '#title' => t('Updated Unit Owner Primary Indicator'),
      '#options' => [
        $trueint => t('Y'),
        $falseint => t('N'),
      ],
      '#default_value' => $row['unit_owner_primary_ind'],
      '#description' => t("Indicates a primary unit owner when multiple owners exist.
      This this data element could be used to indicate the primary point of contact
      for the management office. Values are Y - Yes (default), N - No."),
    ];

    $form['unit_owner_org_id'] = [
      '#type' => 'select',
      '#title' => t('Updated Unit Owner Organization ID'),
      '#options'  => Routines::tableOptions('organization', 1, 4),
      '#empty_option' => t('None'),
      '#empty_value' => '0',
      '#default_value' => $row['unit_owner_org_id'],
      '#description' => t("The unit owner, if the owner is an organization."),
      '#states' => [
        'disabled' => [
          ':input[name="unit_owner_type"]' => ['value' => t("P")],
        ],
        'required' => [
          ':input[name="unit_owner_type"]' => ['value' => t("O")],
        ],
      ],
    ];

    $form['unit_owner_interest'] = [
      '#type' => 'textfield',
      '#title' => t('Updated Unit Owner Interest'),
      '#size' => 20,
      '#default_value' => $row['unit_owner_interest'],
      '#description' => t("A decimal number indicating the degree 
        of interst of an owner if multiple owners exist for a single unit. This number  takes the 
        form of a fraction, and, the sum of these interests should be one for each 
        unit. Multiple owners are given equal shares by default.  For example, two  owners would 
        be given an interest of 0.5.  This fraction can be adjusted manually but the sum of these 
        fractions should sum to one for each unit."),
    ];

    $form['unit_owner_proxy_id'] = [
      '#type' => 'select',
      '#title' => t('Updated unit_owner_proxy_id'),
      '#options'  => Routines::tableOptions('person', 1, 4),
      '#empty_value' => '0',
      '#empty_option' => t('None'),
      '#default_value' => $row['unit_owner_proxy_id'],
      '#description' => t("A person authorized to act on behalf of the owner 
        and who serves as the community's point of contact for all business relating to the 
        unit owner."),
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => t('Submit'),
    // C '#prefix' => '<tr><td>',.
    // C '#suffix' => '</td></tr></table>',.
    ];

    $form['actions']['cancel'] = [
      '#type'  => 'submit',
      '#value' => t('Cancel'),
      '#submit' => ['::cancelForm'],
    // C '#prefix' => '<tr><td>',.
    // C '#suffix' => '</td></tr></table>',.
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (empty($form_state->getValue('unit_owner_proxy_id'))) {
      $form_state->setValue('unit_owner_proxy_id', '0');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection();

    $row = [];

    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      $row[$key] = $value;
    }

    // $row['unit_owner_list_id'] = 0;
    $tid = $db->startTransaction();
    try {
      $nbr_edited = $db->update('unit_owner')->fields($row)
        ->condition('unit_owner_list_id', $row['unit_owner_list_id'], '=')
        ->execute();
      if ($nbr_edited != 1) {
        $tid->rollBack();
        $this->messenger()->addError(
            $nbr_edited . $this->t(' rows updated, 1 expected. Nothing done. Unit Owner List ID = ')
                 . $row['unit_owner_list_id'] ); 
      }
      else {
      $this->messenger()->addMessage($this->t('Unit-Owner list update Successful'));
      }
      
    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $this->messenger()->addMessage($this->t('Unit-Owner list update failed. Nothing done. 
        Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(), // $e->getTraceAsString()
      ]), 'error');
    }

    $form_state->setRedirect('unit_owner.list');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('unit_owner.list');
  }

}
