<?php

namespace Drupal\unit_owner\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;

/**
 * Class Unit_OwnerEditForm.
 *
 * @package Drupal\unit_owner\UnitOwnerListController
 *
 * Substitutions:
 * Tblname. Replace with Unit_Owner (init cap).
 * tblname.  Replace with unit_owner.
 * Notes:  Apply replacements to namespace and use statements above.
 *         Class names. Replace underscore and following letter with capital
 *         letter.
 */
class UnitOwnerListController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {

    $db = Database::getConnection('default', 'default');

    $null = NULL;
    $rows = [];
    $header = [
      [
        'data' => t('unit owner list ID'),
        'field' => 'e.unit_owner_list_id',
        'sort' => 'asc',
      ],
      ['data' => t('unit_owned_id'), 'field' => 'e.unit_owned_id'],
      ['data' => t('unit owner type'), 'field' => 'e.unit_owner_type'],
      ['data' => t('unit_owner_person_name_(ID)'), 'field' => 'e.unit_owner_person_id'],
      ['data' => t('unit owner primary ind'), 'field' => 'e.unit_owner_primary_ind'],
      ['data' => t('unit_owner_org_id'), 'field' => 'e.unit_owner_org_id'],
      ['data' => t('unit_owner_interest'), 'field' => 'e.unit_owner_interest'],
      ['data' => t('unit_owner_proxy_id'), 'field' => 'e.unit_owner_proxy_id'],
      ['data' => t('Delete'), 'field' => $null],
    ];

    $select = $db->select('unit_owner', 'e')
      ->fields('e')
      ->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header)
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit()
      ->execute();

    foreach ($select as $row) {

      $unit_owner_list_id = (string) $row->unit_owner_list_id;
      $row->unit_owned_id = (string) Routines::unitAddress($row->unit_owned_id);
      $row->unit_owner_type ==  '0' ?
        $row->unit_owner_type = 'P' : $row->unit_owner_type = 'O';
      
      $row->owner_person_id = (string) Routines::personName($row->owner_person_id);
      
      $row->unit_owner_proxy_id = empty($row->unit_owner_proxy_id) ?
        $row->unit_owner_proxy_id = NULL :
          (string) Routines::personName($row->unit_owner_proxy_id);
      
      $row->unit_owner_org_id = empty($row->unit_owner_org_id) ?
        $row->unit_owner_org_id = NULL : 
          (string) Routines::organizationName($row->unit_owner_org_id);
      
      $row->unit_owner_interest = \round($row->unit_owner_interest,30);
      
      $row->unit_owner_primary_ind = empty($row->unit_owner_primary_ind) ?
        $row->unit_owner_primary_ind = 'N' : $row->unit_owner_primary_ind = 'Y';

      $row->unit_owner_list_id = Link::fromTextAndUrl($this->t($unit_owner_list_id),
          Url::fromRoute('unit_owner.edit',
              ['unit_owner_list_id' => $unit_owner_list_id]))->toString();

      $row->eddel = Link::fromTextAndUrl('Delete', Url::fromRoute('unit_owner.delete', ['unit_owner_list_id' => $unit_owner_list_id]))->toString();

      $rows[] = ['data' => (array) $row];

    }

    $build[] = [
      '#type' => 'link',
      '#title' => t('Add'),
      '#url' => Url::fromRoute('unit_owner.add'),
    ];

    $build['unit_owner_list_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => "The list is empty.  Please add data",
      '#sticky' => \TRUE,
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    $build['#cache']['max-age'] = 0;

    return $build;
  }

}
