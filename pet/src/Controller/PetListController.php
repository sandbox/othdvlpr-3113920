<?php

namespace Drupal\pet\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;

/**
 * Class PetEditForm.
 *
 * @package Drupal\pet\PetListController
 *
 * Substitutions:
 * Tblname. Replace with Pet (init cap).
 * tblname.  Replace with pet.
 * Notes:  Apply replacements to namespace and use statements above.
 *         Class names. Replace underscore and following letter with capital
 *         letter.
 */
class PetListController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
   // $this->messenger()->addMessage(\t('BuildingListController  ckpt1'));

    $db = Database::getConnection('default', 'default');

    $null = NULL;
    $rows = [];
    $header = [
      [
        'data' => \t('Pet_ID'),
        'field' => 'e.pet_id',
        'sort' => 'desc',
      ],
      ['data' => \t('Pet_Owner_Name (ID)'), 'field' => 'e.pet_owner_id'],
      ['data' => \t('Pet_Name'), 'field' => 'e.pet_name'],
      ['data' => \t('Pet_Type'), 'field' => 'e.pet_type'],
      ['data' => \t('Pet_Breed'), 'field' => 'e.pet_breed'],
      ['data' => \t('Pet_Gender_Code'), 'field' => 'e.pet_gender_code'],
      ['data' => \t('Pet_Color'), 'field' => 'e.pet_color'],
      ['data' => \t('Pet_Birth_Date'), 'field' => 'e.pet_birth_date'],
      ['data' => \t('Pet_Height'), 'field' => 'e.pet_height'],
      ['data' => \t('Pet_Length'), 'field' => 'e.pet_length'],
      ['data' => \t('Pet_Weight'), 'field' => 'e.pet_weight'],
      ['data' => \t('Pet_License_Nr'), 'field' => 'e.pet_license_nr'],
      ['data' => \t('Pet_Inoculations'), 'field' => 'e.pet_inoculations'],
      ['data' => \t('Pet_Photo_ID'), 'field' => 'e.pet_photo_id'],
      ['data' => \t('Pet_Comments5'), 'field' => 'e.pet_comments5'], 
      ['data' => t('Delete'), 'field' => $null],
    ];

    $select = $db->select('pet', 'e')
      ->fields('e')
      ->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header)
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit()
      ->execute();
   // $this->messenger()->addMessage(t('petistController  ckpt2'));
    
    foreach ($select as $row) {
      $pet_id = (string) $row->pet_id;

      /* difference btwn $editurl and format in
       * https://api.drupal.org/api/drupal/core!lib!
       * Drupal!Core!Link.php/function/Link%3A%3AfromTextAndUrl/8.2.x .
       */
      
      $row->pet_owner_id = (string) Routines::personName($row->pet_owner_id);
      $pet_photo_id_int = (integer) $row->pet_photo_id; 
      $row->pet_photo_id = Routines::displayManagedFileImage($pet_photo_id_int);
      
      $row->pet_id = Link::fromTextAndUrl(
            $this->t($pet_id),
            Url::fromRoute(
                'pet.edit',
                ['pet_id' => $pet_id, 'pet_photo_id' => $pet_photo_id_int]
            )
              )->toString();   
      
      
      $row->eddel = Link::fromTextAndUrl('Delete', Url::fromRoute('pet.delete',
            ['pet_id' => $pet_id, 'pet_photo_id' => $pet_photo_id_int]))->toString(); 
      $rows[] = ['data' => (array) $row];

    }

    $build[] = [
      '#type' => 'link',
      '#title' => t('Add'),
      '#url' => Url::fromRoute('pet.add'),
    ];

    $build['pet_list_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => "The list is empty.  Please add data",
      '#sticky' => \TRUE,
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];
    
    $build['#cache']['max-age'] = 0;

    return $build;

  }

}
