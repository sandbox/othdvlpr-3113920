<?php

namespace Drupal\pet\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\pet\Form\PetAddForm;
use Drupal\Core\Database\DatabaseExceptionWrapper;
/**
 * Class PetEditForm.
 *
 * @package Drupal\pet\Form\PetEditForm
 *
 * Substitutions:
 * Tblname. Replace with Pet (init cap).
 * tblname.  Replace with pet.
 * Note:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 * @todo. Consider moving BuildingAddForm::table_option routines.
 */
class PetEditForm extends FormBase {

  public function getFormId() {
    return 'pet_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $pet_id = '', $pet_photo_id = '') {  

    $db = Database::getConnection('default', 'default');
    
    $select = $db->select('pet', 'e')
      ->fields('e')
      ->condition('e.pet_id', $pet_id, '=')
      ->execute();
    $row = $select->fetchAssoc();
       
    $form['pet_id'] = [
      '#type' => 'number',
      // '#options' => $options,
      '#disabled' => TRUE,
      '#title' => t('Choose entry to update'),
      '#value' => $row['pet_id'],
      '#description' => t("pet_id"),
      '#ajax' => [
        'wrapper' => 'updateform',
        'callback' => 'pet_form_update_callback',
      ],
    ];

    $form['pet_owner_id'] = [
      '#type' => 'select',
      '#disabled' => FALSE,
      '#title' => t('Updated pet_owner_id'),
      '#options' => PetAddForm::petOwner(),
      '#default_value' => $row['pet_owner_id'],
      '#description' => t("pet_owner_id"),
    ];

    $form['pet_name'] = [
      '#type' => 'textfield',
      '#title' => t('Updated pet_name'),
      '#size' => 20,
      '#default_value' => $row['pet_name'],
      '#description' => t("pet_name"),
    ];

    $form['pet_type'] = [
      '#type' => 'textfield',
      '#title' => t('Updated pet_type'),
      '#size' => 20,
      '#default_value' => $row['pet_type'],
      '#description' => t("pet_type"),
    ];

    $form['pet_breed'] = [
      '#type' => 'textfield',
      '#title' => t('Updated pet_breed'),
      '#size' => 20,
      '#default_value' => $row['pet_breed'],
      '#description' => t("pet_breed"),
    ];

    $form['pet_gender_code'] = [
      '#type' => 'textfield',
      '#title' => t('Updated pet_gender_code'),
      '#size' => 20,
      '#default_value' => $row['pet_gender_code'],
      '#description' => t("The animal's gender. Values: U - Unknown, M - Male,
          F - Female, MN - Neutered Male, FS - Spayed Female, TG - Trans Gender."),
    ];

    $form['pet_color'] = [
      '#type' => 'textfield',
      '#title' => t('Updated pet_color'),
      '#size' => 20,
      '#default_value' => $row['pet_color'],
      '#description' => t("pet_color"),
    ];

    $form['pet_birth_date'] = [
      '#type' => 'date',
      '#title' => t('Updated pet_birth_date'),
      '#date_format' => 'Y-m-d',
      '#required'  => FALSE,
      '#default_value' => $row['pet_birth_date'],
      '#description' => t("pet_birth_date"),
    ];

    $form['pet_height'] = [
      '#type' => 'textfield',
      '#title' => t('Updated pet_height'),
      '#size' => 20,
      '#default_value' => $row['pet_height'],
      '#description' => t("pet_height"),
    ];

    $form['pet_length'] = [
      '#type' => 'textfield',
      '#title' => t('Updated pet_length'),
      '#size' => 20,
      '#default_value' => $row['pet_length'],
      '#description' => t("pet_length"),
    ];

    $form['pet_weight'] = [
      '#type' => 'textfield',
      '#title' => t('Updated pet_weight'),
      '#size' => 20,
      '#default_value' => $row['pet_weight'],
      '#description' => t("pet_weight"),
    ];

    $form['pet_license_nr'] = [
      '#type' => 'textfield',
      '#title' => t('Updated pet_license_nr'),
      '#size' => 20,
      '#default_value' => $row['pet_license_nr'],
      '#description' => t("pet_license_nr"),
    ];

    $form['pet_inoculations'] = [
      '#type' => 'textfield',
      '#title' => t('Updated pet_inoculations'),
      '#size' => 20,
      '#default_value' => $row['pet_inoculations'],
      '#description' => t("pet_inoculations"),
    ];
    
    $idstate = \Drupal::state();
    $idstate->set('default_pet_photo_id',$row['pet_photo_id']);
    
    $idstate->get('default_pet_photo_id');
    
    $form['pet_photo_id'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Updated pet_photo_id'),
      '#default_value' =>  [$row['pet_photo_id']],
      '#description' => $this->t("pet_photo_id"),
      '#upload_location' => 'public://images',
      '#multiple' => FALSE,
      '#TREE' => TRUE,
      '#upload_validators' => [
        'FileIsImage' => [],
        'FileExtension' => ['extensions' => 'png gif jpg jpeg'],
        'FileImageDimensions' => ['maxDimensions' => 50 * 50],
      ],
    ];

    $form['pet_comments'] = [
    '#type'  => 'textfield',
    '#title' => \t('Updated Pet Comments'),
    '#size'  => 60,
    '#maxsize' => 255,
    '#description' => t("Updated Pet description "
            . "and/or office comments concerning it."),
    '#default_value' => $row['pet_comments'],
    '#prefix' => '<tr><td>',
    '#suffix' => '</td></tr>',
  ];
        
    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => t('Submit'),
    ];

    $form['actions']['cancel'] = [
      '#type'  => 'submit',
      '#value' => t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
    $db = Database::getConnection();
    $row = [];
    
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
    
    if ((\gettype($value) === 'array')) {
        if (empty(\current($value))) {
          // NULL;.
          $value = 0;
        }
        else {
          $value =  \current($value); // \current($value);
        }
      }
      
    if ($key === 'pet_birth_date') {
        $value = empty($value) && !is_numeric($value) ? NULL : trim(strip_tags($value));
      }
  
    $row[$key] = $value;
    }
    /*
    \Drupal::messenger()->addMessage(\t('$roww[pet_photo_id at ckpt= ')
            . $row['pet_photo_id']);
    */
    $tid = $db->startTransaction();
    try {
      $nbr_updated = $db->update('pet')->fields($row)
        ->condition('pet_id', $row['pet_id'], '=')
        ->execute();
      
      $messenger = $this->messenger();
      if ($nbr_updated != 1) {
       $this->messenger->addError(
           $nbr_updated . \t(' rows updated, 1 expected. Nothing done. Pet_id=')
                . $row['pet_id']);
               $tid->rollBack();
     }
      else {
      
        if (!empty($row['pet_photo_id'])) {
      
      $idstate = \Drupal::state();
      $idgetval = $idstate->get('default_pet_photo_id');
      $idstate->delete('default_pet_photo_id');
      
      /*
      \Drupal::messenger()->addMessage(\t('default_pet_photo_id=') . $val);
         
      \Drupal::messenger()->addMessage(\t('form[pet_photo_id][#default_value][0] $val =')
            . $val);
      // \sleep(15);
      \Drupal::messenger()->addMessage(\t('new_pet_photo_id=')
             . $row['pet_photo_id']);
       */ 
      Routines::editManagedFiles($row['pet_photo_id'], $idgetval, 'pet');
        }
      $message = \t('Update of pet/owner [') . $row['pet_name'] . \t('/') . 
              Routines::personName($row['pet_owner_id']) . \t('] successful');
      $messenger->addMessage($message);
        }
    }
    catch (DatabaseExceptionWrapper $e) {

     $tid->rollBack();
     // $errmessage = $e->getMessage();
      $messenger = $this->messenger();
      $messenger->addMessage($this->t('Edit Failed. Nothing done. Error message
        follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('pet.list');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('pet.list');
  }

}
