<?php

namespace Drupal\pet\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\pet\Form\PetAddForm;
use Drupal\Component\Render\PlainTextOutput;
use Drupal\Core\Database\DatabaseExceptionWrapper;
/**
 * Class PetEditForm.
 *
 * @package Drupal\pet\Form\PetAddForm
 *
 * Substitutions:
 * Tblname. Replace with Pet (init cap).
 * tblname. Replace with pet.
 * col02.   Replace with column name (i.e. pet_id).
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 */
class PetAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pet_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $pet_id = '') {

    $form['add'] = [
      '#type'  => 'fieldset',
      '#title' => \t('Add a pet entry'),
      '#prefix' => '<div id="formadd" style="background-color:Cornsilk"',
      '#suffix' => '</div>',
    ];
    $petOptions = PetAddForm::petOwner();
    $form['add']['pet_owner_id'] = [
    '#type'  => 'select',
    '#options' => $petOptions, // Routines::tableOptions('resident', 1, 7, TRUE),
    '#title' => t('Pet Owner ID'),
    '#size'  => 5,
    '#description' => t("The resident that owns, or is responsible for, the pet"),
  ];
      
  $form['add']['pet_name'] = [
    '#type'  => 'textfield',
    '#title' => t('Pet Name'),
    '#size'  => 20,
    '#maxsize' => 255,
    '#description' => t("The pet's name"),
  ];
  $form['add']['pet_type'] = [
    '#type'  => 'textfield',
    '#title' => t('Pet Type'),
    '#size'  => 15,
    '#maxsize' => 255,
    '#description' => t("The type of animal. Values include: DOG, CAT, BIRD,
       FERRET, SNAKE, FISH, etc. "),
  ];
  $form['add']['pet_breed'] = [
    '#type'  => 'textfield',
    '#title' => t('Pet Breed'),
    '#size'  => 20,
    '#maxsize' => 255,
    '#description' => t("The breed of animal, depending on pet_type."),
  ];
  $form['add']['pet_gender_code'] = [
    '#type'  => 'select',
    '#title' => t('Pet Gender Code'),
    '#size'  => 6,
    '#options' => array(
      'U ' => t('U - Unknown'),
      'M ' => t('M - Male'), 
      'F ' => t('F - Female'),
      'MN' => t('MS - Neutered Male'), 
      'FS' => t('FS - Spayed Female'),
      'TG' => t('TG - Trans Gender'),
      ),
    '#empty_option' => "Not Used",
    '#description' => t("The animal's gender. Values: U - Unknown, M - Male,
      F - Female, MN - Neutered Male, FS - Spayed Female, TG - Trans Gender."),
  ];
  $form['add']['pet_color'] = [
    '#type'  => 'textfield',
    '#title' => t('Pet Color'),
    '#size'  => 20,
    '#maxsize' => 255,
    '#description' => t("Pet coloring pattern."),
  ];
  $form['add']['pet_birth_date'] = [
    '#type'  => 'date',
    '#title' => t('Pet Birth Date'),
    '#required'  => \FALSE,
    '#description' => t("Pet exact or approximate date of birth, if known."),
  ];
  $form['add']['pet_height'] = [
    '#type'  => 'textfield',
    '#title' => t('Pet Height'),
    '#size'  => 10,
    '#maxsize' => 255,
    '#description' => t("Pet height in inches, centimeters, etc."),
  ]; 
  $form['add']['pet_length'] = [
    '#type'  => 'textfield',
    '#title' => t('Pet Length'),
    '#size'  => 10,
    '#maxsize' => 255,
    '#description' => t("Pet length in inches, centimeters, etc."),
  ];
  $form['add']['pet_weight'] = [
    '#type'  => 'textfield',
    '#title' => t('Pet Weight'),
    '#size'  => 10,
    '#maxsize' => 255,
    '#description' => t("Pet weight in pounds, kilograms, etc."),
  ];
  $form['add']['pet_license_nr'] = [
    '#type'  => 'textfield',
    '#title' => t('Pet License Number'),
    '#size'  => 25,
    '#maxsize' => 255,
    '#description' => t("Pet License Number"),
  ];
  $form['add']['pet_inoculations'] = [
    '#type'  => 'textfield',
    '#title' => t('Pet Innoculations'),
    '#size'  => 25,
    '#maxsize' => 255,
    '#description' => t("One or more innoculations given to the pet"),
  ];

  $form['add']['pet_photo_id'] = [
    '#type'  => 'managed_file',
    '#title' => t('Pet Photo ID'),
    '#description' => t("The unique numeric identifier of the stored photograph."),
    '#upload_location' => 'public://images',
    '#default_value' => NULL, // variable_get('unit_floor_plan', ''),
    '#upload_validators' => [
      'FileIsImage' => [],
      'FileExtension' => ['extensions' => 'png gif jpg jpeg'],
      'FileImageDimensions' => ['maxDimensions' => 50 * 50],
      ],
  ];

  $form['add']['pet_comments'] = [
    '#type'  => 'textfield',
    '#title' => \t('Pet Comments'),
    '#size'  => 60,
    '#maxsize' => 255,
    '#description' => t("Pet description and/or office comments concerning it."),
  ];

    $form['add']['submit'] = [
      '#type'  => 'submit',
      '#value' => \t('Add'),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
      ];

    $form['add']['cancel'] = [
      '#type'  => 'submit',
      '#value' => \t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
      '#prefix' => '<td>',
      '#suffix' => '</td></tr></table>',
    ];

    return $form;
  }

  /**
   * Validates a form for pet (optional).
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  
}

  /**
   * Adds pet form data.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');
    
    $row = [];
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
     
      if (\gettype($value) === 'array') {
        if (empty(\current($value))) {
          $value =  NULL;
        }
        else {
          $value = (integer) \current($value);
        }
      }

      /* Optional managed_file addition (col19) 
      $value = \gettype($value) === 'array' ? 0 : $value;
      */

      //  $value = empty($value) ? \NULL : $value;
      
      if ($key === 'pet_birth_date') {
        $value = empty($value) && !is_numeric($value) ? NULL : trim(strip_tags($value));

      }
      else {
      }

      $row[$key] = $value;
      
      
      
    }

    $tid = $db->startTransaction();
    try {
      $db->insert('pet')->fields($row)->execute();

      $message = \t('Insert of pet/owner [') . $row['pet_name'] . \t('/') . 
              Routines::personName($row['pet_owner_id']) . \t('] successful');
      $this->messenger()->addMessage($message);

      if (!empty($row['pet_photo_id'])) {
        Routines::addManagedFile($row['pet_photo_id'], 'pet');
      }

    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $this->messenger()->addMessage(\t('Add Failed. Nothing done. Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');

    }

    $form_state->setRedirect('pet.list');
  }

  /**
   * Cancels pet form addition.
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('pet.list');
  }
  public static function petOwner() {
    $resident_options = (array) Routines::tableOptions('resident', 1, 2, TRUE);
    $resident_options = PetAddForm::getIdsArray($resident_options,0); 
      
      \sort($resident_options,SORT_NUMERIC);
      $personids = Routines::tableOptions('person',1,7,\NULL,$form_state);
      $residentids = \array_intersect_key($personids,\array_flip($resident_options));
  return $residentids;
  }

public static function getIdsArray(array $options, $valindx)  {
  foreach ($options as $optkey => $optval) {
    $optval1 = PlainTextOutput::renderFromHtml($optval);
    $optval1 =  \str_replace(': ','',$optval1);
    $options[0] = $optval1;
    $tempstr = (string) $options[$valindx];
    $temparr = explode(' ',$tempstr);
$optids[] = $temparr[$valindx];
  }
 return $optids;
}  
  
  
}
