<?php

namespace Drupal\pet\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\Core\Url;

/**
 * Class DefaultForm.
 *
 * @package Drupal\pet\Form\PetDeleteForm
 *
 *  * Substitutions:
 * Tblname. Replace with Pet (init cap).
 * tblname.  Replace with pet.
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 */
class PetDeleteForm extends ConfirmFormBase {

  /*
  public $pet_id;.
  public $greeting_text;.
  protected $pathAlias;.
  $this->pathAlias;.
   */

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pet_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('pet.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuery() {
    return;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state, $pet_id = '', $pet_photo_id = '') {

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Do you wish to delete these pet/pet-photo IDs below? '),
      '#value' =>  $this->t("$pet_id" . '/' .  "$pet_photo_id"),
      '#size' => 60,
      '#maxlength' => 128,
    ];
    
    return parent::buildForm($form, $form_state, $pet_id = '', $pet_photo_id = '');

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');

    $pet_id = (integer) \Drupal::request()->attributes->get('pet_id');

    $pet_photo_id = (integer) \Drupal::request()->query->get('pet_photo_id');

    $tid = $db->startTransaction();
    try {
      $nbr_deleted = $db->delete('pet')
        ->condition('pet_id', $pet_id, '=')
        // ->condition('pet_id', 'xyz', '=')
        ->execute();

      if ($nbr_deleted != 1) {
       $this->messenger->addError(
           $nbr_deleted .     \t(' rows deleted, 1 expected. Nothing done. Pet_id=')
                . $pet_id);
               $tid->rollBack();
     }
      else {
      
        if (!empty($pet_photo_id)) {
          Routines::deleteManagedFile($pet_photo_id, 'pet');
        }

        $this->messenger()->addMessage(\t('Pet deleted.'));
      }
    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      
      $this->messenger->addMessage($this->t('Delete Failed. Nothing done. Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>   $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('pet.list');
  }

}
