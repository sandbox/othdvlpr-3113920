<?php

namespace Drupal\person\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\DatabaseExceptionWrapper;
/**
 * Person form.
 */
class PersonAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'person_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['add'] = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Add a person entry'),
      '#prefix' => '<div id="formadd" style="background-color:Cornsilk"',
      '#suffix' => '</div>',
    ];
    $form['add']['person_last_name'] = [
      '#type'  => 'textfield',
      '#title' => t('Last Name'),
      '#size'  => 15,
      '#prefix' => '<table border=0 cellspacing=3 celladding=3><tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['person_first_name'] = [
      '#type'  => 'textfield',
      '#title' => t('First Name'),
      '#size'  => 20,
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    ];
    $form['add']['person_middle_initial'] = [
      '#type'  => 'textfield',
      '#title' => t('MI'),
      '#size'  => 1,
      '#description' => t("Middle initial"),
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['person_street_address'] = [
      '#type'  => 'textfield',
      '#title' => t('Street address'),
      '#size'  => 40,
      '#description' => t("Separate multiple lines with commas"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['person_address_city'] = [
      '#type'  => 'textfield',
      '#title' => t('City'),
      '#size'  => 25,
      '#description' => t("Person address city"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['person_address_state'] = [
      '#type'  => 'textfield',
      '#title' => t('State or Province'),
      '#size'  => 20,
      '#description' => t("State, Province or other Major Contry Division"),
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    ];
    $form['add']['person_address_country'] = [
      '#type'  => 'textfield',
      '#title' => t('Country'),
      '#size'  => 15,
      '#description' => t("Person address country"),
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    ];
    $form['add']['person_postal_code'] = [
      '#type'  => 'textfield',
      '#title' => t('Postal/ZIP code'),
      '#size'  => 11,
      '#description' => t("Person postal code"),
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['person_phone_day'] = [
      '#type'  => 'tel',
      '#title' => t('Day Phone'),
      '#size'  => 15,
      '#description' => t("Person phone day"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['person_phone_evening'] = [
      '#type'  => 'tel',
      '#title' => t('Evening Phone'),
      '#size'  => 15,
      '#description' => t("Person phone evening"),
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    ];
    $form['add']['person_phone_cell'] = [
      '#type'  => 'tel',
      '#title' => t('Cell Phone'),
      '#size'  => 15,
      '#description' => t("Person Phone Cell"),
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    ];
    $form['add']['person_pager'] = [
      '#type'  => 'tel',
      '#title' => t('Pager Number'),
    // '#size'  => 15,
      '#description' => t("Person Pager"),
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['person_email_address'] = [
      '#type'  => 'email',
      '#title' => t('Email'),
      '#size'  => 30,
      '#description' => t("Person Email Address (format x@y.com)"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['person_login_id'] = [
      '#type'  => 'textfield',
      '#title' => t('Login ID'),
      '#size'  => 20,
      '#description' => t("Person Login ID"),
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['person_note'] = [
      '#type'  => 'textarea',
      '#title' => t('Office Note'),
      '#cols'  => 60,
      '#description' => t("Person Note"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td></tr>',
    ];
    
    $form['add']['actions'] = ['#type' => 'actions'];

    $form['add']['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Add'),
      '#prefix' => '<tr><td>',
    ];
    $form['add']['actions']['cancel'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Cancel'),
      '#limit_validation_errors' => array(),
      '#submit' => ['::cancelForm'],
        // '#prefix' => '<td>',
      '#suffix' => '</td></tr></table>',
    ];
    return $form;

  }

  /**
   * {@inheritdoc}
   *
   * @note: validation runs during cancdlForm!  This appears 2b a known issue.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (empty(\trim($form_state->getValue('person_last_name'))) and empty(\trim($form_state->getValue('person_first_name')))) {
      $form_state->setErrorByName('person_last_name',
        $this->t('Please enter the person\'s first or last name.'));

    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $db = Database::getConnection('default', 'default');
    
    $row = [];
    
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      
      $row[$key] = $value;
      // nbr_updated test not done
    }

    
    $tid = $db->startTransaction();
    try {
      $db->insert('person')->fields($row)->execute();
      $this->messenger()->addMessage('Person insert successful.');
    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
   
      $this->messenger()->addMessage(\t('Insert Failed. Nothing done. Error message
        follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
      
    }
    $form_state->setRedirect('person.list');
  }

  /**
   * Cancels Person Add form addition.
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('person.list');
  }

}
