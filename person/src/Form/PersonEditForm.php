<?php

namespace Drupal\person\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class PersonEditForm.
 *
 * @package Drupal\person_module\Form
 */
class PersonEditForm extends FormBase {

    /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'person_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $person_id = '') {

    $db = Database::getConnection('default', 'default');

    $select = $db->select('person', 'e')
      ->fields('e')
      ->condition('e.person_id', $person_id, '=')
      ->execute();
    $row = $select->fetchAssoc();

    $form['person_id'] = [
      '#type' => 'number',
      '#disabled' => TRUE,
      '#title' => t('Choose entry to update'),
      '#value' => $person_id,
    ];

    $form['person_last_name'] = [
      '#type' => 'textfield',
      '#title' => t('Updated last name'),
      '#size' => 15,
      '#default_value' => $row['person_last_name'],
    ];

    $form['person_first_name'] = [
      '#type' => 'textfield',
      '#title' => t('Updated first name'),
      '#size' => 20,
      '#default_value' => $row['person_first_name'],
    ];

    $form['person_middle_initial'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Updated MI'),
      '#size' => 1,
      '#default_value' => trim($row['person_middle_initial']),
      '#description' => $this->t("over-curser description"),
    ];

    $form['person_street_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Updated address'),
      '#size' => 40,
      '#default_value' => $row['person_street_address'],
      '#description' => $this->t("over-curser description"),
    ];

    $form['person_address_city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Updated City'),
      '#size' => 25,
      '#default_value' => $row['person_address_city'],
      '#description' => $this->t("over-curser description"),
    ];

    $form['person_address_state'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Updated State'),
      '#size' => 20,
      '#default_value' => $row['person_address_state'],
      '#description' => $this->t("over-curser description"),
    ];

    $form['person_address_country'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Updated Country'),
      '#size' => 15,
      '#default_value' => $row['person_address_country'],
      '#description' => $this->t("over-curser description"),
    ];

    $form['person_postal_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Updated postal_code'),
      '#size' => 11,
      '#default_value' => $row['person_postal_code'],
      '#description' => $this->t("over-curser description"),
    ];

    $form['person_phone_day'] = [
      '#type' => 'tel',
      '#title' => $this->t('Updated Daytime phone'),
      '#size' => 15,
      '#default_value' => $row['person_phone_day'],
      '#description' => $this->t("over-curser description"),
    ];

    $form['person_phone_evening'] = [
      '#type' => 'tel',
      '#title' => $this->t('Updated Evening Phone'),
      '#size' => 15,
      '#default_value' => $row['person_phone_evening'],
      '#description' => $this->t("over-curser description"),
    ];

    $form['person_phone_cell'] = [
      '#type' => 'tel',
      '#title' => $this->t('Updated cell phone'),
      '#size' => 15,
      '#default_value' => $row['person_phone_cell'],
      '#description' => $this->t("over-curser description"),
    ];

    $form['person_pager'] = [
      '#type' => 'tel',
      '#title' => $this->t('Updated pager'),
      '#size' => 15,
      '#default_value' => $row['person_pager'],
      '#description' => $this->t("over-curser description"),
    ];

    $form['person_email_address'] = [
      '#type' => 'email',
      '#title' => $this->t('Updated email'),
      '#size' => 30,
      '#default_value' => $row['person_email_address'],
      '#description' => $this->t("Email address (format: x@y.com)."),
    ];

    $form['person_login_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Updated login ID'),
      '#size' => 20,
      '#default_value' => $row['person_login_id'],
      '#description' => $this->t("over-curser description"),
    ];

    $form['person_note'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Updated person note'),
      '#cols' => 60,
      '#default_value' => $row['person_note'],
      '#description' => $this->t("over-curser description"),
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => t('Submit'),
    // '#prefix' => '<tr><td>',
    // '#suffix' => '</td></tr></table>',
    ];
    $form['actions']['cancel'] = [
      '#type'  => 'submit',
      '#value' => t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
    // '#prefix' => '<tr><td>',
    // '#suffix' => '</td></tr></table>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (empty(\trim($form_state->getValue('person_last_name'))) and empty(\trim($form_state->getValue('person_first_name')))) {
      $form_state->setErrorByName('person_last_name',
        $this->t('Please enter the person\'s first or last name.'));

    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');

    $row = [];
    
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      $row[$key] = $value;

    }

    $tid = $db->startTransaction();
    try {
      $nbr_edited = $db->update('person')->fields($row)
        ->condition('person_id', $row['person_id'], '=')
        ->execute();
      
      if ($nbr_edited != 1) {
        $tid->rollBack();
        $this->messenger()->addError(
            $nbr_edited . $this->t(' rows updated, 1 expected. Nothing done. Tblname ID = ')
                 . $row['tblname_id'] ); 
      }
      else {
      $this->messenger()->addMessage(\t('Person update successful'));
      }
    }
    catch (DatabaseExceptionWrapper $e) {
      $this->messenger()->addMessage($this->t('Person update failed. Nothing done. 
        Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(), // $e->getTraceAsString()
      ]), 'error');
    }

    $form_state->setRedirect('person.list');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('person.list');
  }

}
