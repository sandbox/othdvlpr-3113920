<?php

namespace Drupal\person\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\DatabaseExceptionWrapper;
Use Drupal\Core\Url;
/**
 * Class DefaultForm.
 *
 * @package Drupal\person\Form
 */
class PersonDeleteForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'person_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('person.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $person_id = '') {

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Do you wish to delete the Person ID below? '),
      '#value' =>  $this->t("$person_id"),
      '#size' => 60,
      '#maxlength' => 128,
    ];
    
    return parent::buildForm($form, $form_state, $person_id = '');

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');

    $person_id = (integer) \Drupal::request()->attributes->get('person_id');

    //  $person_id = (integer) -1;  //  $this->person_id;
    $tid = $db->startTransaction();
    try {
      $nbr_deleted = $db->delete('person')
        ->condition('person_id', $person_id, '=')
        // ->condition('nocol', 'xyz' , '=')
        ->execute();

      if ($nbr_deleted != 1) {
        $tid->rollBack();
       $this->messenger()->addError(
           $nbr_deleted .     $this->t(' rows deleted, 1 expected. Nothing done.'));
     }
     
      else {
      $this->messenger()->addMessage($this->t('Person deleted.'));
     }
    }
    
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      
      $this->messenger()->addMessage($this->t('Delete Failed. Nothing done. Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('person.list');
    }

  }
