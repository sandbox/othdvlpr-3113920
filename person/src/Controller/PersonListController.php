<?php

namespace Drupal\person\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;

/**
 * Class EditForm.
 *
 * @package Drupal\person\PersonListController
 *
 * Substitutions:
 * Tblname. Replace with Person (init cap).
 * tblname.  Replace with person.
 * Notes:  Apply replacements to namespace and use statements above.
 *         Class names. Replace underscore and following letter with capital
 *         letter.
 */
class PersonListController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {

      $db = Database::getConnection('default', 'default');

        $null = NULL;
    $rows = [];
    $ownerdesc = 'O';
    $residentdesc = 'R';
    $supplierdesc = 'S'; 
    $spaces20 = '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'
            . '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp';
    
$select = $db->select('person', 't');

$select->addField('t', 'person_id');
$select->addField('t', 'person_last_name');
$select->addField('t', 'person_first_name');
$select->addField('t', 'person_middle_initial');
$select->addField('t', 'person_street_address');
$select->addField('t', 'person_address_city');
$select->addField('t', 'person_address_state');
$select->addField('t', 'person_address_country');
$select->addField('t', 'person_postal_code');
$select->addField('t', 'person_phone_day');
$select->addField('t', 'person_phone_evening');
$select->addField('t', 'person_phone_cell');
$select->addField('t', 'person_pager');
$select->addField('t', 'person_email_address');
$select->addField('t', 'person_login_id');
$select->addField('t', 'person_note');

$select->addJoin('LEFT OUTER','unit_owner','o','t.person_id = o.owner_person_id'); //unit_owner_list_id is the key
  $ownerfieldcontents = "CASE WHEN t.person_id = o.owner_person_id Then 'O' ELSE ' ' END";
  $select->addExpression($ownerfieldcontents,$ownerdesc);

$select->addJoin('LEFT OUTER','resident','y','t.person_id = y.resident_id',);
  $residentfieldcontents = "CASE WHEN t.person_id = y.resident_id Then 'R' ELSE ' ' END";
  $select->addExpression($residentfieldcontents,$residentdesc);
 
$select->addJoin('LEFT OUTER','organization_person','z','t.person_id = z.op_person_id');
  $supplierfieldcontents = "CASE WHEN t.person_id = z.op_person_id Then 'S' ELSE ' ' END";
  $select->addExpression($supplierfieldcontents,$supplierdesc);

$select->distinct();

       $this->messenger()->addMessage(t('personlistcontroller  ckpt2'));

$header = [
      [
        'data' => \t('person_id'),
        'field' => 't.person_id',
        'sort' => 'asc',
      ],
      ['data' => \t('     Last_Name      '), 'field' => 't.person_last_name'],
      ['data' => \t('    First_Name     '), 'field' => 't.person_first_name'],
      ['data' => \t('MI'), 'field' => 'e.person_middle_initial'],
      ['data' => \t('O '), 'field' => $ownerdesc],
      ['data' => \t('R '), 'field' => $residentdesc],
      ['data' => \t('S '), 'field' => $supplierdesc],
      ['data' => \t('Street_Address'), 'field' => 't.person_street_address'],
      ['data' => \t('        City        '), 'field' => 't.person_address_city'],
      ['data' => \t('   State/Province   '), 'field' => 't.person_address_state'],
      ['data' => \t('       Country      '), 'field' => 't.person_address_country'],
      ['data' => \t('Postal Code'), 'field' => 't.person_postal_code'],
      ['data' => \t('Phone Day'), 'field' => 't.person_phone_day'],
      ['data' => \t('Phone Evening'), 'field' => 't.person_phone_evening'],
      ['data' => \t('Phone Cell'), 'field' => 't.person_phone_cell'],
      ['data' => \t('Pager'), 'field' => 't.person_pager'],
      ['data' => \t('Email Address'), 'field' => 't.person_email_address'],
      ['data' => \t('LoginID'), 'field' => 't.person_login_id'],
      ['data' => \t('Notes/Remarks' . $spaces20 . $spaces20),
          'field' => 't.person_note'],
      ['data' => \t('Delete'), 'field' => $null],
    ];

      $entries = $select
        ->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header)
        ->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit()    
        ->execute();

$rows = [];  // array();


    foreach ($entries as $rowobj) {

      $person_id = (string) $rowobj->person_id;
 
      $rowobj->person_id = Link::fromTextAndUrl($this->t($person_id),
              Url::fromRoute('person.edit',
                ['person_id' => $person_id]))->toString();    

    $row[0] = $rowobj->person_id;
    $row[1] = $rowobj->person_last_name;
    $row[2] = $rowobj->person_first_name;
    $row[3] = $rowobj->person_middle_initial;
    $row[4] = $rowobj->$ownerdesc;
    $row[5] = $rowobj->$residentdesc;
    $row[6] = $rowobj->$supplierdesc;
    $row[7] = $rowobj->person_street_address;
    $row[8] = $rowobj->person_address_city;
    $row[9] = $rowobj->person_address_state;
    $row[10] = $rowobj->person_address_country;
    $row[11] = $rowobj->person_postal_code;
    $row[12] = $rowobj->person_phone_day;
    $row[13] = $rowobj->person_phone_evening;
    $row[14] = $rowobj->person_phone_cell;
    $row[15] = $rowobj->person_pager;
    $row[16] = $rowobj->person_email_address;
    $row[17] = $rowobj->person_login_id;
    $row[18] = $rowobj->person_note;
    $row[19] = Link::fromTextAndUrl('Delete',
         Url::fromRoute('person.delete',
                 ['person_id' => $person_id]))->toString();  //                  ['resident_id' => $resident_id]))->toString(); // 
 
      $rows[] = ['data' => (array) $row];   
      
    }

$build[] = [
      '#type' => 'link',
      '#title' => t('Add'),
      '#url' => Url::fromRoute('person.add'),
    ];

    $build['person_list_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => "The list is empty.  Please add data",
      '#sticky' => \TRUE,
      '#cache' => [
        'max-age' => 0,
      ],

    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;
  }

}
