<?php

namespace Drupal\unit\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
/**
 * Class UnitEditForm.
 *
 * @package Drupal\unit\UnitListController
 *
 * Substitutions:
 * Tblname. Replace with Unit (init cap).
 * tblname.  Replace with unit.
 * Note:  Apply replacements to namespace and use statements above.
 */
class UnitListController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {

    $db = Database::getConnection('default', 'default');

    $null = NULL;
    $rows = [];
    $header = [
      [
        'data' => t('unit_id'),
        'field' => 'e.unit_id',
        'sort' => 'asc',
      ],
      ['data' => \t('unit_code'), 'field' => 'e.unit_code'],
      ['data' => \t('unit_type_cd'), 'field' => 'e.unit_type_cd'],
      ['data' => \t('unit_building_id'), 'field' => 'e.unit_building_id'],
      ['data' => \t('unit_street_number'), 'field' => 'e.unit_street_number'],
      ['data' => \t('unit_street_name'), 'field' => 'e.unit_street_name'],
      ['data' => \t('unit_door_id'), 'field' => 'e.unit_door_id'],
      ['data' => \t('unit_interest'), 'field' => 'e.unit_interest'],
      ['data' => \t('unit_purchase_date'), 'field' => 'e.unit_purchase_date'],
      ['data' => \t('unit_lease_expiration_review_date'), 'field' => 'e.unit_lease_expiration_review_date'],
      ['data' => \t('unit_manager_id'), 'field' => 'e.unit_manager_id'],
      ['data' => \t('unit_management_org_id'), 'field' => 'e.unit_management_org_id'],
      ['data' => \t('Delete'), 'field' => $null],
    ];

    $select = $db->select('unit', 'e')
      ->fields('e')
      ->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header)
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit()
        // C ->range(0, 10).
      ->execute();

    foreach ($select as $row) {

      $unit_id = (string) $row->unit_id;
      
      $row->unit_manager_id = (string) Routines::personName($row->unit_manager_id);
      $row->unit_management_org_id = (string) Routines::organizationName($row->unit_management_org_id);
      
      $formlink = Link::fromTextAndUrl(
            $this->t($unit_id),
            Url::fromRoute('unit.edit', ['unit_id' => $unit_id])
        )->toString();

      $row->eddel = Link::fromTextAndUrl('Delete', Url::fromRoute('unit.delete', ['unit_id' => $unit_id]))->toString();

      $row->unit_id = $formlink;

      $rows[] = ['data' => (array) $row];

    }

    $build[] = [
      '#type' => 'link',
      '#title' => t('Add'),
      '#url' => Url::fromRoute('unit.add'),
    ];

    $build['unit_list_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => "The list is empty.  Please add data",
      '#sticky' => \TRUE,

      '#cache' => [
        'max-age' => 0,
      ],

    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;

  }

}
