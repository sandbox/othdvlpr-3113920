<?php

namespace Drupal\unit\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\DatabaseExceptionWrapper;
Use Drupal\Core\Url;

/**
 * Class DefaultForm.
 *
 * @package Drupal\unit\Form
 *
 * Substitutions:
 * Tblname. Replace with Unit (init cap).
 * tblname.  Replace with unit.
 * Note:  Apply replacements to namespace and use statements above.
 */
class UnitDeleteForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unit_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('unit.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $unit_id = '') {

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Do you wish to delete the Unit ID below? '),
      '#value' =>  $this->t("$unit_id"),
      '#size' => 60,
      '#maxlength' => 128,
    ];
    
    return parent::buildForm($form, $form_state, $unit_id = '');

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');

    $unit_id = (integer) \Drupal::request()->attributes->get('unit_id');

    // $unit_id = 0;
    $tid = $db->startTransaction();
    try {
      $nbr_deleted = $db->delete('unit')
        ->condition('unit_id', $unit_id, '=')
        ->execute();
      
      if ($nbr_deleted != 1) {
        $tid->rollBack();
       $this->messenger()->addError(
           $nbr_deleted . $this->t(' rows deleted, 1 expected. Nothing done.'));
     }
      else {
      $this->messenger()->addMessage($this->t('Unit deleted.'));
     }
    }
    
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      
      $this->messenger()->addMessage($this->t('Delete Failed. Nothing done. Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('unit.list');
  }

}
