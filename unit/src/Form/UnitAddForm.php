<?php

namespace Drupal\unit\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class UnitEditForm.
 *
 * @package Drupal\unit\Form\UnitAddForm
 *
 * Substitutions:
 * Tblname. Replace with Unit (init cap).
 * tblname.  Replace with unit.
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 */
/**
 * Class UnitAddForm implements FormInterface {.
 */
class UnitAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unit_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $unit_id = '') {

    $form['add'] = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Add a unit entry'),
      // '#prefix' => '<div id="formadd" style="background-color:Cornsilk"',
      '#prefix' => '<table>',
      // '#suffix' => '</div>',
    ];

    $form['add']['unit_code'] = [
      '#type'  => 'textfield',
      '#title' => $this->t('Unit Code'),
      '#size'  => 6,
      '#description' => $this->t("A unique code assigned by the community to a unit.  
        Such a code could be numeric, alphabetic, alphanumeric, and could 
        contain special characters.  Codes could be serially-assigned numbers,
        or reflect  building, floor, apartment number, or any combination
        thereof.  (Examples: 100, 10A, 10-a, A2-100.)"),
      // '#prefix' => '<table border=0 cellspacing=3 celladding=3><tr><td>',
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['unit_type_cd'] = [
      '#type'  => 'select',
      '#title' => $this->t('unit_type_cd'),
      '#options' => Routines::tableOptions('unit_type', 1, 2),
      '#empty_value' => NULL,
    // '0'
      '#default_value' => NULL,
      '#empty_option' => $this->t('None'),
      '#description' => $this->t("The type of unit, specified in the unit_type table,
      to which the unit belongs"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['unit_building_id'] = [
      '#type'  => 'select',
      '#title' => $this->t("Unit Building Identifier"),
      '#options'  => Routines::tableOptions('building', 1, 3, \TRUE),
      '#empty_option' => $this->t('None'),
      '#empty_value' => NULL,
    // '0'
      '#default_value' => NULL,
      '#description' => $this->t('A unique building or building complex identifier to "
      which the unit belongs'),
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
    ];

    $form['add']['unit_street_number'] = [
      '#type'  => 'textfield',
      '#title' => $this->t('unit_street_number'),
      '#size'  => 10,
      '#description' => $this->t("The unit's street number.  Note: those units attached 
      to a building should inherit its street number if unit_street_number and 
      unit_street_name are left blank."),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['unit_street_name'] = [
      '#type'  => 'textfield',
      '#title' => $this->t("Unit Street Name"),
      '#size'  => 20,
      '#description' => $this->t("The unit's street name.   Note: those units attached to a
      building should inherit its street number if unit_street_number and 
      unit_street_name are left blank."),
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    ];
    // Start here.
    $form['add']['unit_door_id'] = [
      '#type'  => 'textfield',
      '#title' => $this->t('unit_door_id'),
      '#size'  => 8,
      '#description' => $this->t('An identifier placed upon the door of an individual 
        unit. this identifier could be the unit code and/or an apartment number
        designator.'),
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['unit_interest'] = [
      '#type'  => 'textfield',
      '#title' => $this->t('unit_interest'),
      '#size'  => 20,
      '#default_value' => 0.0,
      '#step' => .00000000000000000001,
      '#description' => $this->t("A decimal number indicating the degree of interest 
        of an individual unit in relation to the community. This number can 
        take the form of a fraction, a percentage or some other pre-defined 
        value. 
        <br /> For a fraction, the sum of these interests should sum to one for 
        all units; for a percentage, the sum should be 100.
        Numbers of this type are often placed in the governing documents 
        of the community."),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['unit_purchase_date'] = [
      '#type'  => 'date',
      '#title' => $this->t('unit_purchase_date'),
      '#description' => $this->t("The most recent date on which the unit was purchased."),
      '#default_value' => NULL,
      '#prefix' => '<td>',
      '#suffix' => '</td></table>',
    ];
    $form['add']['unit_lease_expiration_review_date'] = [
      '#type'  => 'date',
      '#description' => $this->t('The date on which the current lease is due to expire
        or be reviewed.  This date defines a leased or rented unit. 
        It should reflect a future date (e.g., month or year).'),
      '#title' => $this->t("unit_lease_expiration_review_date"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['leasefields'] = [
      '#type' => 'container',
      '#title' => $this->t('Leased-unit management'),
      '#states' => [
        'visible' => [
          ':input[name="unit_lease_expiration_review_date"]' => ['filled' => TRUE],
        ],
      ],
    ];
    $form['add']['leasefields']['unit_manager_id'] = [
      '#type'  => 'select',
      '#title' => $this->t('Unit Manager Identifier'),
      '#options'  => Routines::tableOptions('person', 1, 4),
      '#empty_value' => 0,
      '#empty_option' => $this->t('None'),
      '#default_value' => 0,
      '#description' => $this->t("A person retained by the owner to manage the unit. If this person is a member of an 
            organization (e.g., a Real-Estate company) he can be related to his organization there"),
      '#prefix' => '<p><tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['leasefields']['unit_management_org_id'] = [
      '#type'  => 'select',
      '#title' => $this->t('Unit Management Organization Identifier'),
      '#options'  => Routines::tableOptions('organization', 1, 2),
      '#empty_value' => 0,
      '#empty_option' => $this->t('None'),
      '#default' => 0,
      '#description' => $this->t("An organization retained by the owner to manage a unit,
      but which does not specify a specific person as the manager
      for that unit."),
      '#prefix' => '<td>',
      '#suffix' => '</td></tr></p>',
    ];

    $form['add']['submit'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Add'),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];

    $form['add']['cancel'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
    ];

    return $form;
  }

  /**
   * Validates a form for unit (optional).
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (Routines::uniqueColumnValueTest('unit', 'unit_code',
      $form_state->getValue('unit_code')) != 0) {
      $form_state->setErrorByName('unit_code', t('Please enter a unique value 
      for unit_code'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');

    $row = [];
    
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {

      if ($key === 'unit_code') {
        if ((empty($value)  || ctype_space($value))) {
          $value = NULL;
        }
      }
      if ($key === 'unit_lease_expiration_review_date') {
        $value = empty($value) && !is_numeric($value) ?
            \NULL : trim(strip_tags($value));
      }

      if ($key === 'unit_manager_id' || $key === 'unit_management_org_id') {
        if (empty($value)) {
          $value = NULL;
        }
      }
      $row[$key] = $value;
    }

    $tid = $db->startTransaction();
    try {
      $db->insert('unit')->fields($row)->execute(); // insert('tblnam1')
      
      
      $message = $this->t('Insert of Unit [') . $row['unit_code'] . $this->t('] successful');
      $this->messenger()->addMessage($message);
    }
    
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      
      $this->messenger()->addMessage($this->t('Insert Failed. Nothing done. Error message
        follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('unit.list');
  }

  /**
   * Cancels unit form addition.
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('unit.list');
  }

}
