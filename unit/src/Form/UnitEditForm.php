<?php

namespace Drupal\unit\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\common\Routines;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class UnitEditForm.
 *
 * @package Drupal\unit\Form\UnitEditForm
 *
 * Substitutions:
 * Tblname. Replace with Unit (init cap).
 * tblname.  Replace with unit.
 * Note:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * @todo. Consider moving BuildingAddForm::table_option routines.
 */
class UnitEditForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unit_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $unit_id = '') {

    $db = Database::getConnection('default', 'default');
    $select = $db->select('unit', 'e')
      ->fields('e')
      ->condition('e.unit_id', $unit_id, '=')
      ->execute();
    $row = $select->fetchAssoc();

    $rowcoarr = Routines::complexOptions('unit', 1, 5, \TRUE);
    $form['unit_id'] = [
      '#type' => 'number',
      '#disabled' => TRUE,
      '#options' => $unit_id,
      '#title' => t('Choose entry to update'),
      '#default_value' => $row['unit_id'],
      '#description' => $this->t('unit_id'),
      '#prefix' => '<div><tr><td>',
      '#ajax' => [
        'wrapper' => 'updateform',
        'callback' => 'unit_form_update_callback',
      ],
    ];
    $form['unit_code'] = [
      '#type' => 'textfield',
      '#title' => t('Updated Unit Code'),
      '#size' => 6,
      '#default_value' => $row['unit_code'],
      '#description' => t(
          "A unique code assigned by the community to a unit.  Such a code could be numeric, alphabetic,
          alphanumeric, and could contain special characters.  Codes could be serially-assigned numbers,
          or reflect  building, floor,  apartment number, or any combination thereof.  
          Examples: 100, 10A, 10-a, A2-100."
      ),
    ];
    $form['unit_type_cd'] = [
      '#type' => 'select',
      '#title' => t('Updated unit_type_cd'),
      '#options' => Routines::tableOptions('unit_type', 1, 2),
      '#default_value' => $row['unit_type_cd'],
      '#empty_option' => t('None'),
      '#empty_value' => NULL,
      '#description' => t(
          "The type of unit, specified in the unit_type table,
         to which the unit belongs.e_cd"
      ),
    ];
    $form['unit_building_id'] = [
      '#type' => 'select',
      '#title' => t('Updated Building Identifier'),
      '#options' => Routines::tableOptions('building', 1, NULL, TRUE),
      '#empty_option' => t('None'),
      '#empty_value' => NULL,
      '#default_value' => $row['unit_building_id'],
      '#description' => t("A unique building or building complex identifier to which the unit belongs"),
    ];
    $form['unit_street_number'] = [
      '#type' => 'textfield',
      '#title' => t('Updated unit_street_number'),
      '#size' => 10,
      '#default_value' => $row['unit_street_number'],
      '#description' => t("unit_street_number"),
    ];

    $form['unit_street_name'] = [
      '#type' => 'textfield',
      '#title' => t('Updated unit_street_name'),
      '#size' => 20,
      '#default_value' => $row['unit_street_name'],
      '#description' => t("unit_street_name"),
    ];

    $form['unit_door_id'] = [
      '#type' => 'textfield',
      '#title' => t('Updated unit_door_id'),
      '#size' => 8,
      '#default_value' => $row['unit_door_id'],
      '#description' => t("unit_door_id"),
    ];
    $form['unit_interest'] = [
      '#type' => 'number',
      '#title' => t('Updated unit_interest'),
      '#size' => 20,
      '#step' => .00000000000000000001,
      // '#max' => 100.00000000000000000000,
      '#empty_value' => 0.0,
      '#default_value' => $row['unit_interest'],
      '#description' => t("unit_interest"),
    ];
    $form['unit_purchase_date'] = [
      '#type' => 'date',
      '#title' => t('Updated unit_purchase_date'),
      // '#size' => normal,
      // '#date_format' => 'Y-m-d',
      // '#date_year_range' => '-40:+1',
      // '#empty_value' => NULL,
      // '#input'  => TRUE,
      '#default_value' => $row['unit_purchase_date'],
      '#description' => t("unit_purchase_date"),
    ];

    $form['unit_lease_expiration_review_date'] = [
      '#type' => 'date',
      '#title' => t('Updated unit_lease_expiration_review_date'),
      '#default_value' => $row['unit_lease_expiration_review_date'],
      '#empty_value' => NULL,
      '#empty_option' => t('None'),
      '#description' => t(
          "The date on which the current lease is due to expire
        or be reviewed.  This date defines a leased or rented unit. 
        It should reflect a future date (e.g., month or year)."
      ),
    ];

    $form['leasefields'] = [
      '#type' => 'fieldset',
      '#title' => \t('Leased-unit management'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="unit_lease_expiration_review_date"]' => ['empty' => FALSE],
        ],
      ],
      '#prefix' => '<p>',
      '#suffix' => '</td></p>',
    ];
    $form['leasefields']['unit_manager_id'] = [
      '#type' => 'select',
      '#title' => t('Updated unit_manager_id'),
      '#options'  => Routines::tableOptions('person', 1, 4),
      '#default_value' => $row['unit_manager_id'],
      '#empty_option' => t('None'),
      '#description' => t("unit_manager_id"),
    ];

    $form['leasefields']['unit_management_org_id'] = [
      '#type' => 'select',
      '#title' => t('Updated unit_management_org_id'),
      '#options'  => Routines::tableOptions('organization', 1, 2),
      '#default_value' => $row['unit_management_org_id'],
      '#empty_option' => t('None'),
      '#empty_value' => 0,
      '#description' => t("unit_management_org_id"),
      '#suffix' => '</p></div>',
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => t('Submit'),
      // C '#prefix' => '<tr><td>',.
      // C '#suffix' => '</td></tr></table>',.
    ];

    $form['actions']['cancel'] = [
      '#type'  => 'submit',
      '#value' => t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
      // C '#prefix' => '<tr><td>',.
      // C '#suffix' => '</td></tr></table>',.
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

      if (Routines::uniqueColumnValueTest('unit', 'unit_code',
      $form_state->getValue('unit_code')) > 0
        && $form['unit_code']['#default_value'] !== $form_state->getValue('unit_code')) 
      {
      $form_state->setErrorByName('unit_code',
          \t('Please enter a unique value for unit_code'));

    }

  }

  /**
   * {@inheritdoc}
   *
   * @todo make unit_interest 0 if empty}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection();

    $row['unit_id'] = $form_state->getValue('unit_id');
    $row['unit_code'] = $form_state->getValue('unit_code');
    $row['unit_type_cd'] = $form_state->getValue('unit_type_cd');
    $row['unit_building_id'] = $form_state->getValue('unit_building_id');
    $row['unit_street_number'] = $form_state->getValue('unit_street_number');
    $row['unit_street_name'] = $form_state->getValue('unit_street_name');
    $row['unit_door_id'] = $form_state->getValue('unit_door_id');
    $row['unit_interest'] = $form_state->getValue('unit_interest');

    $row['unit_code'] =
      empty($row['unit_code'])
      || \ctype_space($row['unit_code']) ? \NULL
      : $row['unit_code'];

    $row['unit_purchase_date'] =
        empty($form_state->getValue('unit_purchase_date'))
        && !is_numeric($form_state->getValue('unit_purchase_date')) ? NULL
        : trim(strip_tags($form_state->getValue('unit_purchase_date')));

    $row['unit_lease_expiration_review_date'] =
        empty($form_state->getValue('unit_lease_expiration_review_date'))
        && !is_numeric($form_state->getValue('unit_lease_expiration_review_date')) ? NULL
        : trim(strip_tags($form_state->getValue('unit_lease_expiration_review_date')));

    $row['unit_manager_id'] = empty($form_state->getValue('unit_manager_id'))
        ? NULL : $form_state->getValue('unit_manager_id');

    $row['unit_management_org_id'] = empty($form_state->getValue('unit_management_org_id'))
        ? NULL : $form_state->getValue('unit_management_org_id');

    // $row['unit_id'] = 0;
    $tid = $db->startTransaction();
    try {
      $nbr_edited = $db->update('unit')->fields($row)
        ->condition('unit_id', $row['unit_id'], '=') 
        ->execute();
      if ($nbr_edited != 1) {
        $tid->rollBack();
        $this->messenger()->addError(
            $nbr_edited . $this->t(' rows updated, 1 expected. Nothing done. Unit ID = ')
                 . $row['unit_id'] ); 
      }
      
      else {
      $this->messenger()->addMessage($this->t('Unit update Successful'));
      }
    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $this->messenger()->addMessage($this->t('Unit update failed. Nothing done. 
        Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(), // $e->getTraceAsString()
      ]), 'error');
    }

    $form_state->setRedirect('unit.list');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('unit.list');
  }

}
