<?php

namespace Drupal\resident\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Database\Database;

/**
 * Class ResidentListController.  Displays resident table rows.
 *
 * @package Drupal\resident\ResidentListController
 *
 * Substitutions:
 * Tblname. Replace with Resident (init cap).
 * tblname.  Replace with resident.
 * Notes:  Apply replacements to namespace and use statements above.
 *         Class names. Replace underscore and following letter with capital
 *         letter.
 */
class ResidentListController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    // \Drupal::messenger()->addMessage(t('BuildingListController  ckpt1'));
    $db = Database::getConnection('default', 'default');

    $null = NULL;
    $rows = [];
    $personexpression = 'unit_owner_name';
    $residentexpression = 'resident_type';

    $select = $db->select('resident', 't');

    $select->addField('t', 'resident_id');
    // $select->addField('t', 'col02');
    $select->addField('t', 'resident_unit_id');
    $select->addField('u', 'unit_code');
    $select->addField('t', 'move_in_date');
    $select->addField('t', 'registration_date');
    $select->addField('t', 'resident_owner_family_mbr_ind');

    $select->addJoin('INNER', 'person', 'p', 't.resident_id = p.person_id');
    $personname = "concat_ws(',  ', p.person_last_name, p.person_first_name, 
      concat(' ',replace(p.person_middle_initial,'',NULL),'.'))";

    $select->addExpression($personname, $personexpression);

    $select->addJoin('INNER', 'unit', 'u', 't.resident_unit_id = u.unit_id');

    $select->addJoin('LEFT OUTER', 'unit_owner', 'uo', 'uo.owner_person_id = t.resident_id');

    $resident_type = "CASE 
      WHEN t.resident_id = uo.owner_person_id THEN
        CASE WHEN t.resident_unit_id = uo.unit_owned_id THEN 'RESIDENT OWNER' 
        ELSE 'TENANT OWNER'
        END
      WHEN t.resident_owner_family_mbr_ind = TRUE THEN 'OWNER FAMILY MEMBER'
      ELSE 'TENANT' 
      END";

    $select->addExpression($resident_type, $residentexpression);

    $select->distinct();

    $select->range(0, 50);

    // \Drupal::messenger()->addMessage(t('residentistController  ckpt2'));
    $header = [
      [
        'data' => t('resident_id'),
        'field' => 't.resident_id',
        'sort' => 'asc',
      ],
      ['data' => t('resident name'), 'field' => $personexpression],
      ['data' => t('resident unit ID'), 'field' => 'e.resident_unit_id'],
      ['data' => t('resident unit code'), 'field' => 'u.unit_code'],
      ['data' => t('move-in date'), 'field' => 'e.move_in_date'],
      ['data' => t('registration date'), 'field' => 'e.registration_date'],
      [
        'data' => t('resident-owner family-member'),
        'field' => 'e.resident_owner_family_mbr_ind',
      ],
      ['data' => t('resident type'), 'field' => $residentexpression],
        ['data' => t('Delete'), 'field' => $null],
    ];

    $entries = $select
    // ->orderByHeader($header)
      ->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header)
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit()
      ->execute();

    $rows = [];

    foreach ($entries as $rowobj) {

      $resident_id = (string) $rowobj->resident_id;
      $rowobj->resident_id = Link::fromTextAndUrl($this->t($resident_id),
              Url::fromRoute('resident.edit',
                ['resident_id' => $resident_id]))->toString();

      $row[0] = $rowobj->resident_id;
      $row[1] = $rowobj->$personexpression;
      $row[2] = $rowobj->resident_unit_id;
      $row[3] = $rowobj->unit_code;
      $row[4] = $rowobj->move_in_date;
      $row[5] = $rowobj->registration_date;
      $row[6] = ($rowobj->resident_owner_family_mbr_ind) ? 'Y' : 'N';
      $row[7] = $rowobj->$residentexpression;

      $row[8] = Link::fromTextAndUrl('Delete',
         Url::fromRoute('resident.delete',
      // ['resident_id' => $resident_id]))->toString(); //
                 ['resident_id' => $resident_id]))->toString();

      $rows[] = ['data' => (array) $row];
    }

    $build[] = [
      '#type' => 'link',
      '#title' => t('Add'),
      '#url' => Url::fromRoute('resident.add'),
    ];

    $build['resident_list_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => "The list is empty.  Please add data",
      '#sticky' => \TRUE,
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;

  }

}
