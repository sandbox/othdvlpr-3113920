<?php

namespace Drupal\resident\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;
/**
 * Class ResidentEditForm.
 *
 * @package Drupal\resident\Form\ResidentEditForm
 *
 * Substitutions:
 * Tblname. Replace with Resident (init cap).
 * tblname.  Replace with resident.
 * Note:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 * @todo . Consider moving BuildingAddForm::table_option routines.
 */
class ResidentEditForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'resident_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $resident_id = '') {
   
    $db = Database::getConnection('default', 'default');
    $select = $db->select('resident', 'e')
      ->fields('e')
      ->condition('e.resident_id', $resident_id, '=')
      ->execute();
    $row = $select->fetchAssoc();

    $trueint = (int) 1;
    $falseint = (int) 0;

    $form['resident_id'] = [
      '#type' => 'select',
      '#disabled' => \TRUE,
      '#title' => t('Choose entry to update'),
      '#options' => Routines::tableOptions('person', 1, 4, \NULL, $form_state),
      '#default_value' => $row['resident_id'],
      '#description' => t("resident_id"),
      '#ajax' => [
        'wrapper' => 'updateform',
        'callback' => 'resident_form_update_callback',
      ],
    ];

    $form['resident_unit_id'] = [
      '#type' => 'select',
      '#options' => Routines::tableOptions('unit', 1, 7, TRUE),
      '#title' => t('Updated resident_unit_id'),
      '#default_value' => $row['resident_unit_id'],
      '#description' => t("resident_unit_id"),
    ];

    $form['move_in_date'] = [
      '#type' => 'date',
      '#title' => t('Move-in Date'),
      // '#date_format' => 'Y-m-d',
      // '#date_year_range' => '-40:+1',
      '#required'  => TRUE,
      '#description' => t("The resident's move-in date"),
      '#default_value' => $row['move_in_date'],
    ];

    $form['registration_date'] = [
      '#type' => 'date',
      '#title' => t('Resident Registration Date'),
      '#required'  => FALSE,
      '#description' => t("The date the resident registered with the community."),
      '#default_value' => $row['registration_date'],
    ];

    $form['resident_owner_family_mbr_ind'] = [
      '#type' => 'select',
      '#title' => t('Resident Owner Family Member Indicator'),
      '#options' => [
        $trueint => t('Y'),
        $falseint => t('N'),
      ],
      '#description' => t("Resident is a non-owner family member residing in owner's unit."),
      '#default_value' => $row['resident_owner_family_mbr_ind'],
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => t('Submit'),
    ];

    $form['actions']['cancel'] = [
      '#type'  => 'submit',
      '#value' => t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // No validation code necessary.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection();

    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      $row[$key] = $value;
    }

    $row['registration_date'] =
      empty($form_state->getValue('registration_date'))
      && !is_numeric($form_state->getValue('registration_date')) ? NULL
      : trim(strip_tags($form_state->getValue('registration_date')));

    // $row['resident_id'] = 0;
    $tid = $db->startTransaction();
    try {
      $nbr_edited = $db->update('resident')->fields($row)
        ->condition('resident_id', $row['resident_id'], '=')
        ->execute();
      
      if ($nbr_edited != 1) {
        $tid->rollBack();
        $this->messenger()->addError(
            $nbr_edited . $this->t(' rows updated, 1 expected. Nothing done. Resident ID = ')
                 . $row['resident_id'] ); 
      }
      else {
      $messenger = $this->messenger();
      $messenger->addMessage($this->t('Resident Update Successful'));
      
      }
    }
    
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $this->messenger()->addMessage($this->t('Resident update failed. Nothing done. 
        Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('resident.list');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('resident.list');
  }

}
