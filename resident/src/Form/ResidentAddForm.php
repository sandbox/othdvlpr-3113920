<?php

namespace Drupal\resident\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class ResidentAddForm. Adds a row to the resident table.
 *
 * @package Drupal\resident\Form\ResidentAddForm
 *
 * Substitutions:
 * Tblname. Replace with Resident (init cap).
 * tblname. Replace with resident.
 * col02.   Replace with column name (i.e. resident_id).
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 */
class ResidentAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'resident_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $resident_id = '') {

    $trueint = (int) 1;
    $falseint = (int) 0;

    $form['add'] = [
      '#type'  => 'fieldset',
      '#title' => \t('Add a resident entry'),
    // '#prefix' => '<div id="formadd" style="background-color:Cornsilk"',
    // '#suffix' => '</div>',
    ];
    $form['add']['resident_id'] = [
      '#type'  => 'select',
      '#options' => Routines::tableOptions('person', 1, 4, NULL, $form_state),
      '#title' => \t('resident Identifier'),
      '#description' => \t("Primary Key: Unique resident ID, foreign key to person"),
    ];
    /*
    $form['add']['resident_name'] = [
    '#type'  => 'select',
    '#options' => Routines::tableOptions('person',1,4,NULL,$form_state),
    '#title' => \t('resident Identifier'),
    '#description' => \t("Primary Key: Unique resident ID, foreign key to person"),
    // '#prefix' => '<table border=0 cellspacing=3 celladding=3><tr><td>',
    // '#suffix' => '</td>',
    ];
     */
    $form['add']['resident_unit_id'] = [
      '#type'  => 'select',
      '#options' => Routines::tableOptions('unit', 1, 7, TRUE),
      '#title' => \t('resident_unit_id'),
      '#description' => \t("Resident Unit Identifier, foreign key to unit."),
    // '#prefix' => '<td>',
    // '#suffix' => '</td></tr>',
    ];
    $form['add']['move_in_date'] = [
      '#type'  => 'date',
      '#title' => \t('Move-in Date'),
      '#date_format' => 'Y-m-d',
      '#date_year_range' => '-40:+1',
      '#required'  => TRUE,
      '#default_value'  => NULL,
      '#description' => \t("The resident's move-in date"),
    // '#prefix' => '<tr><td>',
    // '#suffix' => '</td>',
    ];
    $form['add']['registration_date'] = [
      '#type'  => 'date',
      '#title' => \t('Resident Registration Date'),
      '#date_format' => 'Y-m-d',
      '#date_year_range' => '-40:+1',
      '#required'  => FALSE,
      '#default_value'  => NULL,
      '#description' => \t("The date the resident registered with the community."),
    // '#prefix' => '<td>',
    // '#suffix' => '</td></tr>',
    ];
    $form['add']['resident_owner_family_mbr_ind'] = [
      '#type'  => 'select',
      '#title' => \t('Resident Owner Family Member Indicator'),
      '#size'  => 'tiny',
      '#options' => [
        $trueint => \t('Y'),
        $falseint => \t('N'),
      ],
      '#default_value'  => $falseint,
      '#description' => \t("Resident is a non-owner family member residing in owner's unit."),
      // '#prefix' => '<tr><td>',
      // '#suffix' => '</td></tr>',
    ];
    $form['add']['submit'] = [
      '#type'  => 'submit',
      '#value' => \t('Add'),
      // C '#prefix' => '<tr><td>',.
      // C '#suffix' => '</td></tr></table>',.
    ];

    $form['add']['cancel'] = [
      '#type'  => 'submit',
      '#value' => \t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
      // C '#prefix' => '<tr><td>',.
      // C '#suffix' => '</td></tr></table>',.
    ];

    return $form;
  }

  /**
   * Validates a form for resident (optional).
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (Routines::uniqueColumnValueTest('resident', 'resident_id',
      $form_state->getValue('resident_id')) != 0) {
      $form_state->setErrorByName('resident_id', t('Please enter a unique value 
      for resident_id'));
    }

  }

  /**
   * Adds resident form data.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');

    $row = [];
    
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {

      if ($key == 'resident_name') {
        continue;
      }

      $row[$key] = $value;
    }

    $row['registration_date'] =
      empty($form_state->getValue('registration_date'))
      && !is_numeric($form_state->getValue('registration_date')) ? NULL
      : trim(strip_tags($form_state->getValue('registration_date')));

    $tid = $db->startTransaction();
    try {
      $db->insert('resident')->fields($row)->execute();

      $message = \t('Insert of resident_id [') . $row['resident_id'] . \t('] successful');
      $this->messenger()->addMessage($message);

    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      
      $this->messenger()->addMessage(\t('Insert Failed. Nothing done. Error message
        follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
    }

    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      $this->messenger()->addMessage($key . \t('=') . $value);
    }

    $form_state->setRedirect('resident.list');
  }

  /**
   * Cancels resident form addition.
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    // $form['actions']['wizard_prev']['#limit_validation_errors'] = [];
    $form_state->setRedirect('resident.list');
  }

}
