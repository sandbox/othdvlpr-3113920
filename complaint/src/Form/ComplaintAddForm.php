<?php

namespace Drupal\complaint\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class ComplaintEditForm.
 *
 * @package Drupal\complaint\Form\ComplaintAddForm
 *
 * Substitutions:
 * Tblname. Replace with Complaint (init cap).
 * tblname. Replace with complaint.
 * col02.   Replace with column name (i.e. complaint_id).
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 */
class ComplaintAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'complaint_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $complaint_id = '') {
    
    $trueint = (int) 1;
    $falseint = (int) 0;
    
    $form['add'] = [
      '#type'  => 'fieldset',
      '#title' => \t('Add a complaint entry'),
      '#prefix' => '<div id="formadd" style="background-color:Cornsilk"',
      '#suffix' => '</div>',
    ];
    
    $form['add']['complaint_recording_person_id'] = [
      '#type'  => 'select',
      '#options' => Routines::tableOptions('person',1,4,NULL,$form_state),
      '#description' => \t("Complaint Recording Person ID"),
      '#empty_option' => 'Office',
      '#empty_value' => '00',
      '#prefix' => '<table<<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['complaining_person_id'] = [
      '#type'  => 'select',
      '#options' => Routines::tableOptions('person',1,4,NULL,$form_state),
      '#description' => \t("Complaining Person ID"),
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['complaint_type'] = [
      '#type'  => 'select',
      '#title' => \t('Complaint Type'),
      '#size'  => 3,
      '#options' => [
          'CONDUCT' => \t('CONDUCT   '),
          'SERVICE' => \t('SERVICE   '),
          'OTHER' => \t('OTHER     '),
      ],
      '#default_value'  => t('OTHER'),
      '#description' => \t("Complaint type. Values: CONDUCT, SERVICE, OTHER"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $today = \date("Y-m-d");
    $form['add']['complaint_receipt_date'] = [
      '#type'  => 'date',
      '#title' => \t('Complaint Receipt Date'),
      '#description' => \t("The date complaint was received"),
      '#default_value' => $today,
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    ];
    $form['add']['complaint_source'] = [
      '#type'  => 'select',
      '#title' => \t('complaint_source'),
      '#size'  => 8,
      '#description' => \t("Complaint source. Values:  Phone, Email,Website, 
              . Letter, Walk-In, BoD_Meeting, Committee_meeting, Other"),
      '#options' => [
          'WEBSITE' => t('WEBSITE'),
          'EMAIL' => t('EMAIL'),
          'PHONE' => t('PHONE'),
          'LETTER' => t('LETTER'),
          'WALK-IN' => t('WALK-IN'),
          'BOD MEETING' => t('BOD MEETING'),
          'COMMITTEE MEETING' => t('COMMITTEE MEETING'),
          'OTHER' => t('OTHER'),
        ],
      '#default_value'  => t('WEBSITE'),
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['complaint_status'] = [
      '#type'  => 'select',
      '#title' => \t('Complaint Status. Values: OPEN, CLOSED, REOPENED, '
              . 'REJECTED'),
      '#size'  => 4,
      '#options' => [
            'OPEN' => t('OPEN'),
            'CLOSED' => t('CLOSED'),
            'REOPENED' => t('REOPENED'),
            'REJECTED' => t('REJECTED'),
        ],
      '#default_value' => t('OPEN'),
      '#description' => \t("Complaint status. Values: OPEN, CLOSED, REOPENED, "
              . "REJECTED"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['complaint_dispositiion'] = [
      '#type'  => 'textarea',
      '#title' => \t('Complaint_dispositiion'),
      '#cols'  => 40,
      '#rows' => 6,
      '#resizable' => FALSE,
      '#description' => \t("Complaint_dispositiion"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['complaint_text'] = [
      '#type'  => 'textarea',
      '#title' => \t('complaint_text'),
      '#cols'  => 40,
      '#rows' => 6,
      '#resizable' => FALSE,
      '#description' => \t("Complaint_Text"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td></tr>',
    ];
    
    $form['add']['submit'] = [
      '#type'  => 'submit',
      '#value' => \t('Add'),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',];

    $form['add']['cancel'] = [
      '#type'  => 'submit',
      '#value' => \t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
      '#prefix' => '<td>',
      '#suffix' => '</td></tr></table>',
    ];

    return $form;
  }

  /**
   * Validates a form for complaint (optional).
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  
  if (empty($form_state->getValue('complaint_receipt_date'))) {
  $form_state->setErrorByName('complaint_receipt_date', \t('Please enter'
  . ' a complaint receipt date.'));
  }
    
}

  /**
   * Adds complaint form data.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');

    foreach ($form_state->cleanValues()->getValues() as $key => $value) {

      $value = empty($value) ? \NULL : $value;
      
      $row[$key] = $value;
    }
    
    $tid = $db->startTransaction();
    try {
      $db->insert('complaint')->fields($row)->execute();

      $message = \t('Insert of complaint successful');
      $this->messenger()->addMessage($message);

    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
        
      $this->messenger()->addMessage(\t('Add Failed. Nothing done. Error message
        follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>   $e->getTraceAsString(),
      ]), 'error');

    }

   /* 
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      \Drupal::messenger()->addMessage($key . \t('=') . $value);
    }
    */
    
    $form_state->setRedirect('complaint.list');
  }

  /**
   * Cancels complaint form addition.
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('complaint.list');
  }

}
