<?php

namespace Drupal\complaint\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\DatabaseExceptionWrapper;
Use Drupal\Core\Url;

/**
 * Class DefaultForm.
 *
 * @package Drupal\complaint\Form\ComplaintDeleteForm
 *
 *  * Substitutions:
 * Tblname. Replace with Complaint (init cap).
 * tblname.  Replace with complaint.
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 */
class ComplaintDeleteForm extends ConfirmFormBase {

    /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'complaint_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {   
    return;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('complaint.list');
  }
  
   /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $complaint_id = '') {
   
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Do you wish to delete this Complaint ID below? '),
      '#value' =>  $this->t("$complaint_id"),
      '#size' => 60,
      '#maxlength' => 128,
    ];
    
    return parent::buildForm($form, $form_state, $complaint_id = '');
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    /*
    $get_values = (string) \implode(',', $form_state->getValues());
    $this->messenger()->addMessage('submitForm $get_values=' . $get_values);
    */
      
    $db = Database::getConnection('default', 'default');

    $complaint_id = (integer) \Drupal::request()->attributes->get('complaint_id');
    
    $tid = $db->startTransaction();
    try {
      $nbr_deleted = $db->delete('complaint')
        ->condition('complaint_id', $complaint_id, '=')
        // ->condition('complaint_receipt_date', 'xyz' , '=')
        ->execute();

       if ($nbr_deleted != 1) {
         $tid->rollBack();
         $this->messenger()->addError(
           $nbr_deleted  .  \t(' rows deleted, 1 expected. Nothing done.'));
     }
      else {
      $this->messenger()->addMessage(\t('Complaint deleted.'));
     }
    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $this->messenger()->addMessage($this->t('Delete Failed. Nothing done. Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>   $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('complaint.list');
  }

}
