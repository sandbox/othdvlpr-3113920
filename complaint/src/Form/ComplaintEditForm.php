<?php

namespace Drupal\complaint\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class ComplaintEditForm.
 *
 * @package Drupal\complaint\Form\ComplaintEditForm
 *
 * Substitutions:
 * Tblname. Replace with Complaint (init cap).
 * tblname.  Replace with complaint.
 * Note:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 * @todo. Consider moving BuildingAddForm::table_option routines.
 */
class ComplaintEditForm extends FormBase {

  /**
   * {@inheritdoc}
   */

    /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'complaint_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $complaint_id = '') {
   
    $db = Database::getConnection('default', 'default');
    $select = $db->select('complaint', 'e')
      ->fields('e')
      ->condition('e.complaint_id', $complaint_id, '=')
      ->execute();
    $row = $select->fetchAssoc();
    
    $form['complaint_id'] = [
      '#type' => 'number',
      '#disabled' => TRUE,
      '#title' => t('Choose entry to update'),
      '#value' => $row['complaint_id'],
      '#description' => t("complaint_id"),
      '#ajax' => [
        'wrapper' => 'updateform',
        'callback' => 'complaint_form_update_callback',
      ],
    ];

    $form['complaint_recording_person_id'] = [
      '#type' => 'select',
      '#title' => t('Updated complaint_recording_person_id'),
      '#options' => Routines::tableOptions('person',1,4,NULL,$form_state),
      '#empty_option' => 'Office',
      '#empty_value' => '00',
      '#default_value' => $row['complaint_recording_person_id'],
      '#description' => t("complaint_recorder"),
    ];

    $form['complaining_person_id'] = [
      '#type' => 'select',
      '#title' => t('Updated complainant'),
      '#options' => Routines::tableOptions('person',1,4,NULL,$form_state),
      '#default_value' => $row['complaining_person_id'],
      '#description' => \t("Complainant"),
    ];

    $form['complaint_type'] = [
      '#type' => 'select',
      '#title' => t('Updated complaint_type'),
      '#options' => [
          'CONDUCT' => \t('CONDUCT   '),
          'SERVICE' => \t('SERVICE   '),
          'OTHER' => \t('OTHER     '),
            ],
      '#default_value' => $row['complaint_type'],
      '#description' => \t("Complaint type. Values: CONDUCT, SERVICE, OTHER"),
    ];

    $form['complaint_receipt_date'] = [
      '#type' => 'date',
      '#title' => t('Updated complaint_receipt_date'),
      '#default_value' => $row['complaint_receipt_date'],
      '#description' => t("The date complaint was received"),
    ];

    $form['complaint_source'] = [
      '#type' => 'select',
      '#title' => \t('Updated complaint_source'),
      '#options' => [
          'WEBSITE' => t('WEBSITE'),
          'EMAIL' => t('EMAIL'),
          'PHONE' => t('PHONE'),
          'LETTER' => t('LETTER'),
          'WALK-IN' => t('WALK-IN'),
          'BOD MEETING' => t('BOD MEETING'),
          'COMMITTEE MEETING' => t('COMMITTEE MEETING'),
          'OTHER' => t('OTHER'),
        ],
      '#default_value' => $row['complaint_source'],
      '#description' => \t('Complaint Source. Values:  WEBSITE, EMAIL, PHONE,
              LETTER, WALK-IN, BOD MEETING, COMMITTEE MEETING, OTHER.'),
    ];

    $form['complaint_status'] = [
      '#type' => 'select',
      '#title' => \t('Complaint Status'),
      '#size' => 4,
      '#options' => [
            'SUBMITTED' => t('SUBMITTED'),
            'OPEN' => t('OPEN'),
            'CLOSED' => t('CLOSED'),
            'REOPENED' => t('REOPENED'),
            'REJECTED' => t('REJECTED'),
        ],
      '#default_value' => $row['complaint_status'],
      '#description' => \t("Complaint status. Values: SUBMITTED, OPEN, CLOSED,"
              . " REOPENED, " ."REJECTED"),         
    ];

    $form['complaint_dispositiion'] = [
      '#type' => 'textarea',
      '#title' => t('Updated Complaint Dispositiion'),
      '#cols'  => 40,
      '#rows' => 6,
      '#resizable' => FALSE,
      '#default_value' => $row['complaint_dispositiion'],
      '#description' => t("Complaint Dispositiion"),
    ];

    $form['complaint_text'] = [
      '#type' => 'textarea',
      '#title' => t('Updated Complaint Text'),
      '#cols'  => 40,
      '#rows' => 6,
      '#resizable' => FALSE,
      '#default_value' => $row['complaint_text'],
      '#description' => t("Complaint Text"),
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => t('Submit'),
    ];

    $form['actions']['cancel'] = [
      '#type'  => 'submit',
      '#value' => t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  if (empty($form_state->getValue('complaint_receipt_date'))) {
    $form_state->setErrorByName('complaint_receipt_date', \t('Please enter'
    . ' a complaint receipt date.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
    $db = Database::getConnection();

    $row['complaint_id'] = $form_state->getValue('complaint_id');
    $row['complaint_recording_person_id'] = 
        $form_state->getValue('complaint_recording_person_id');
    $row['complaining_person_id'] = $form_state->getValue('complaining_person_id');
    $row['complaint_type'] = $form_state->getValue('complaint_type');
    $row['complaint_receipt_date'] = $form_state->getValue('complaint_receipt_date');
    $row['complaint_source'] = $form_state->getValue('complaint_source');
    $row['complaint_status'] = $form_state->getValue('complaint_status');
    $row['complaint_dispositiion'] = $form_state->getValue('complaint_dispositiion');
    $row['complaint_text'] = $form_state->getValue('complaint_text');
    
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      $row[$key] = $value;
    }

    $tid = $db->startTransaction();
    try {
      $nbr_edited = $db->update('complaint')->fields($row)
        ->condition('complaint_id', $row['complaint_id'], '=')  
        ->execute();
      
      if ($nbr_edited != 1) {
        $tid->rollBack();
        $this->messenger()->addError(
            $nbr_edited . \t(' rows updated, 1 expected. Nothing done. Tblname ID = ')
                 . $row['tblname_id'] );
      }
      else {
      $this->messenger()->addMessage(\t('Complaint Update Successful'));
      }
    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $this->messenger()->addMessage($this->t('Complaint update Failed. Nothing done. 
        Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>   $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('complaint.list');
    
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('complaint.list');
  }
  
}
