<?php

namespace Drupal\complaint\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;


/**
 * Class ComplaintEditForm.
 *
 * @package Drupal\complaint\ComplaintListController
 *
 * Substitutions:
 * Tblname. Replace with Complaint (init cap).
 * tblname.  Replace with complaint.
 * Notes:  Apply replacements to namespace and use statements above.
 *         Class names. Replace underscore and following letter with capital
 *         letter.
 */
class ComplaintListController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    // $this->messenger()->addMessage(\t('BuildingListController  ckpt1'));
    
    $db = Database::getConnection('default', 'default');

    $null = NULL;
    $rows = [];
    $header = [
      [
        'data' => t('Complaint ID'),
        'field' => 'e.complaint_id',
        'sort' => 'desc',
      ],
      ['data' => t('Complaint Recorder'), 
          'field' => 'e.complaint_recording_person_id'],
       ['data' => t('Complaining Person_ID'), 'field' => 'e.complaining_person_id'],
      ['data' => t('Complaint Type'), 'field' => 'e.complaint_type'],
      ['data' => t('Complaint Receipt_Date'), 'field' => 'e.complaint_receipt_date'],
      ['data' => t('Complaint Source'), 'field' => 'e.complaint_source'],
      ['data' => t('Complaint Status'), 'field' => 'e.complaint_status'],
      ['data' => t('Complaint_Disposition'), 'field' => 'e.complaint_dispositiion'],
      ['data' => t('Complaint_Text'), 'field' => 'e.complaint_text'],
      ['data' => t('Delete'), 'field' => 'delete'],
    ];
       
   $select = $db->select('complaint', 'e')
      ->fields('e')
      ->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header)
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit()

      ->execute();
   //  $this->messenger()->addMessage(t('complaintistController  ckpt2'));

    foreach ($select as $row) {

      $complaint_id = (string) $row->complaint_id;
      $row->complaining_person_id = (string) Routines::personName($row->complaining_person_id);
      
      if ($row->complaint_recording_person_id == '00') {  
        $row->complaint_recording_person_id =  (string) 'Office';
       } 
        else {
      $row->complaint_recording_person_id =  
       (string) Routines::personName($row->complaint_recording_person_id); 
       }     
    
      $row->complaint_id = Link::fromTextAndUrl($this->t($complaint_id), Url::fromRoute('complaint.edit', ['complaint_id' => $complaint_id]))->toString();
      $row->del = Link::fromTextAndUrl('Delete', Url::fromRoute('complaint.delete', ['complaint_id' => $complaint_id]))->toString();
      
      $rows[] = ['data' => (array) $row];

    }

    $build[] = [
      '#type' => 'link',
      '#title' => t('Add'),
      '#url' => Url::fromRoute('complaint.add'),
    ];

    $build['complaint_list_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => "The list is empty.  Please add data",
      '#sticky' => \TRUE,
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];
    
    $build['#cache']['max-age'] = 0;

    return $build;

  }

}
