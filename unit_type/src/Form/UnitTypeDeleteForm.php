<?php

namespace Drupal\unit_type\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\Core\Url;
// Use Drupal\tblname\HelloStorage;.
// Use Drupal\Core\Url;.
// C \EasyRdf\ParsedUri.
/**
 * Class DefaultForm.
 *
 * @package Drupal\unit_type\Form
 */
class UnitTypeDeleteForm extends ConfirmFormBase {

  /*
  public $tblname_id;.
  public $greeting_text;.
  protected $pathAlias;.
  $this->pathAlias;.
   */

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unit_type_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('unit_type.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }
  
  /**
   * {@inheritdoc}
   */
  public function getQuery() {
    return;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state, $unit_type_id = '', $unit_floor_plan = '') {

    $unit_type_id = (integer) \Drupal::request()->attributes->get('unit_type_id');
    $unit_floor_plan = (integer) \Drupal::request()->query->get('unit_floor_plan');
    // Textfield.
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Do you wish to delete this unit_type/floor_plan IDs below? '),
      '#value' =>  $this->t("$unit_type_id" . '/' .  "$unit_floor_plan"),
      '#size' => 60,
      '#maxlength' => 128,
    ];
    
    return parent::buildForm($form, $form_state, $unit_type_id = '', $unit_floor_plan = '');

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // parent::validateForm($form, $form_state);.
    /*
    No validation.
     */
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $get_values = (string) \implode(',', $form_state->getValues());
    // \Drupal::messenger()->addMessage(\t('get_values=') . $get_values);

    $unit_floor_plan = (integer) \Drupal::request()->query->get('unit_floor_plan');

    $db = Database::getConnection('default', 'default');

    // C $tblname_id = (integer) $this->tblname_id;.
    $unit_type_id = (integer) \Drupal::request()->attributes->get('unit_type_id');
    $unit_floor_plan = (integer) \Drupal::request()->query->get('unit_floor_plan');
    
    $tid = $db->startTransaction();
    try {
      $nbr_deleted = $db->delete('unit_type')
        ->condition('unit_type_id', $unit_type_id, '=')
        ->execute();
      if ($nbr_deleted != 1) {
        $this->messenger->addError(
            $nbr_deleted . \t(' rows deleted, 1 expected. Nothing done.'
              . 'unit_type_id = ') . $unit_type_id );
        $tid->rollBack();
     }
      else {
      
      if (!empty($unit_floor_plan)) {
        Routines::deleteManagedFile($unit_floor_plan, 'unit_type');
        }
      $this->messenger()->addMessage($this->t('Unit Type successfully deleted.'));
      }
    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $this->messenger()->addMessage($this->t('Delete Failed. Nothing done. 
        Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>   $e->getTraceAsString(), // $e->getTraceAsString()
      ]), 'error');
    }

    $form_state->setRedirect('unit_type.list');
  }

}
