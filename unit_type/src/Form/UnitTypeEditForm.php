<?php

namespace Drupal\unit_type\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\Core\MessengerInterface;

/**
 * Class Unit_TypeEditForm.
 *
 * @package Drupal\unit_type\Form\Unit_TypeEditForm
 *
 * Substitutions:
 * Unit_Type. Replace with Unit_Type (init cap).
 * tblname.  Replace with unit_type.
 * Note:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 */
class UnitTypeEditForm extends FormBase {

  /**
   * Factory for original unit_floor_plan.
   *
   * @var typestringfactorystore
   *  Store factory
   */
  private $tempStoreFactory;
  /**
   * Construct temporary storage for original unit_floor_plan.
   *
   * @var typestringstore
   *  Store name
   */
  protected $store;

  /**
   * Storage for messages.
   *
   * @var typestringmessenger
   *  Storage for messages
   */
  protected $messenger;

  /**
   * Construct temporary store object.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $tempStoreFactory
   *   Temporary store factory.
   */
  public function __construct(
        PrivateTempStoreFactory $tempStoreFactory
    ) {
    $this->tempStoreFactory = $tempStoreFactory;
    $collection = 'collection_name2';
    $this->store = $this->tempStoreFactory->get($collection);
  }

  /**
   * Create private tempstore for unit_floor_plan value.
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('tempstore.private')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unit_type_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form,
  FormStateInterface $form_state) {

    $unit_type_id = \Drupal::request()->attributes->get('unit_type_id');
    $unit_floor_plan = \Drupal::request()->attributes->get('unit_floor_plan');

    $db = Database::getConnection('default', 'default');
    $select = $db->select('unit_type', 'e')
      ->fields('e')
      ->condition('e.unit_type_id', $unit_type_id, '=')
      ->execute();
    $row = $select->fetchAssoc();

    $form['unit_type_id'] = [
      '#type' => 'number',
      '#disabled' => TRUE,
      '#value' => $unit_type_id,
      '#title' => t('Choose entry to update'),
      // '#default_value' => $this->row['unit_type_id'],
      '#description' => $this->t('unit_type_id'),
      '#ajax' => [
        'wrapper' => 'updateform',
        'callback' => 'unit_type_form_update_callback',
      ],
    ];

    $form['unit_style'] = [
      '#type' => 'textfield',
      '#title' => t('Updated unit_type_unit_style'),
      '#size' => 60,
      '#default_value' => $row['unit_style'],
      '#description' => $this->t('unit_type_unit_style'),
    ];

    $form['unit_occupancy_limit'] = [
      '#type' => 'textfield',
      '#title' => t('Updated unit_type_unit_occupancy_limit'),
      '#size' => 3,
      '#default_value' => $row['unit_occupancy_limit'],
      '#description' => t("unit_type_unit_occupancy_limit"),
    ];

    $this->store->set('unit_floor_plan_orig', $row['unit_floor_plan']);

    $form['unit_floor_plan'] = [
      '#type' => 'managed_file',
      '#title' => \t('Updated unit_floor_plan'),
      '#default_value' => [$row['unit_floor_plan']],
      '#description' => t("unit_floor_plan"),
      '#upload_location' => 'public://images',
      '#multiple' => FALSE,
      '#tree' => TRUE,
      '#upload_validators' => [
        'FileIsImage' => [],
        'FileExtension' => ['extensions' => 'png gif jpg jpeg'],
        'FileImageDimensions' => ['maxDimensions' => 50 * 50],
      ],
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => t('Submit'),
    // C '#prefix' => '<tr><td>',.
    // C '#suffix' => '</td></tr></table>',.
    ];

    $form['actions']['cancel'] = [
      '#type'  => 'submit',
      '#value' => t('Cancel'),
      '#submit' => ['::cancelForm'],
    ];
    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection();
    $unit_floor_plan_orig = $this->store->get('unif_floor_plan_orig');

    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      if (\gettype($value) === 'array') {
        if (empty(\current($value))) {
          $value = \NULL;
        }
        else {
          $value = \current($value);
        }
      }
      
      $value = (empty($value) && ($key == 'unit_occupancy_limit')) ? 0 : $value;
      $row[$key] = $value;
    }

    $message = $row['unit_floor_plan'];

    $tid = $db->startTransaction();
    try {
      $nbr_edited = $db->update('unit_type')->fields($row)
        ->condition('unit_type_id', $row['unit_type_id'], '=')
        // ->condition('unit_type_id', $row['nonrow'], '=')
        ->execute();
     
      if ($nbr_edited != 1) {
        $this->messenger->addError(
            $nbr_edited . \t(' rows updated, 1 expected. Nothing done.helloid = ')
                 . $row['unit_type_id'] ); // $row['helloid']
        $tid->rollBack();
      }
      else {
      $unit_floor_plan_orig = $this->store->get('unit_floor_plan_orig');
      Routines::editManagedFiles($row['unit_floor_plan'],$unit_floor_plan_orig, 'unit_type');

      $this->messenger()->addMessage($this->t('Update Successful'));
      }
    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $this->messenger()->addMessage($this->t('Update Failed. Nothing done. 
        Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>   $e->getTraceAsString(), // $e->getTraceAsString()
      ]), 'error');
    }

    $this->store->delete('unit_floor_plan_orig');

    $form_state->setRedirect('unit_type.list');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('unit_type.list');
  }

}
