<?php

namespace Drupal\unit_type\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class Unit_TypeAddForm.
 *
 * @package Drupal\unit_type\Form
 */
class UnitTypeAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unit_type_add_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['unit_style'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Unit Style'),
      '#description' => $this->t('Describe a unit type.'),
      '#maxlength' => 255,
      '#size' => 60,
    ];
    $form['unit_occupancy_limit'] = [
      '#type' => 'number',
      '#maxlength' => 4,
      '#default_value' => 1,
      '#title' => $this->t('Unit Occupancy Limit'),
      '#description' => $this->t('Enter a positive integer'),
    ];
    $form['unit_floor_plan'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Unit Floor Plan'),
      '#description' => $this->t('Unit picture or floor plan'),
      '#upload_location' => 'public://images',
    // variable_get('unit_floor_plan', ''),.
      '#default_value' => NULL,
      '#upload_validators' => [
        'FileIsImage' => [],
        'FileExtension' => ['extensions' => 'png gif jpg jpeg'],
        'FileImageDimensions' => ['maxDimensions' => 50 * 50],
      ],
    ];
    /*
    $form['image_thumb'] = [
    '#type' => 'item',
    '#title' => $this->t('Image Thumb'),
    '#description' => $this->t('Image Link'),
    ];
     */
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    $form['add']['cancel'] = [
      '#type'  => 'submit',
      '#value' => t('Cancel'),
      '#submit' => ['::cancelForm'],
      // C '#prefix' => '<tr><td>',.
      // C '#suffix' => '</td></tr></table>',.
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');

    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      if (\gettype($value) === 'array') {
       // $this->messenger()->addMessage(\t('\gettype($value)= ') . \gettype($value));
        if (empty(\current($value))) {
          $value = NULL;
        }
        else {
          $value = (integer) \current($value);
        }
      }
      
      $value = (empty($value) && ($key == 'unit_occupancy_limit')) ? 0 : $value;
      
      $row[$key] = $value;

    }
    $tid = $db->startTransaction();
    try {
      $db->insert('unit_type')->fields($row)->execute();
      \Drupal::messenger()->addMessage('Insert successful.');
      if (!empty($row['unit_floor_plan'])) {
        Routines::addManagedFile($row['unit_floor_plan'], 'unit_type');
      }

    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $messenger = $this->messenger();
      $messenger->addMessage($this->t('Insert Failed. Nothing done. Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>   $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('unit_type.list');
  }

  /**
   * Cancels unit_type form addition.
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('unit_type.list');
  }

}
