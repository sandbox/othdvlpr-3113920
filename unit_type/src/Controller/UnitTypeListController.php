<?php

namespace Drupal\unit_type\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;

/**
 * Class Unit_TypeEditForm.
 *
 * @package Drupal\unit_type\Unit_typeLIstController
 *
 * Substitutions:
 * Tblname. Replace with Unit_Type (init cap).
 * tblname.  Replace with unit_type.
 * Note:  Apply replacements to namespace and use statements above.
 */
class UnitTypeListController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    // \Drupal::messenger()->addMessage(\t('UnitTypeListController  ckpt1'));

    $db = Database::getConnection('default', 'default');
    $null = NULL;
    $rows = [];
    $header = [
      [
        'data' => t('unit type ID'),
        'field' => 'e.unit_type_id',
        'sort' => 'asc',
      ],
      ['data' => \t('unit_type unit_style'), 'field' => 'e.unit_type_unit_style'],
      ['data' => \t('unit_type unit occupancy limit'), 'field' => 'e.unit_type_unit_occupancy_limit'],
      ['data' => \t('unit_type unit_floor_plan'), 'field' => 'e.unit_type_unit_floor_plan'],

      ['data' => t('Ed/Del'), 'field' => $null],
    ];

    $select = $db->select('unit_type', 'e')
      ->fields('e')
      ->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header)
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit()
      ->execute();
    // \Drupal::messenger()->addMessage(\t('UnitTypeListController  ckpt2'));

    foreach ($select as $row) {
      $unit_type_id = (string) $row->unit_type_id;
      $unit_floor_plan_int = (integer) $row->unit_floor_plan;
      $row->unit_floor_plan =
        Routines::displayManagedFileImage($row->unit_floor_plan);

      $formlink = Link::fromTextAndUrl($this->t($unit_type_id),
              Url::fromRoute('unit_type.edit',
                      ['unit_type_id' => $unit_type_id]))->toString();

      $row->eddel = Link::fromTextAndUrl('Delete', Url::fromRoute('unit_type.delete',
          ['unit_type_id' => $unit_type_id, 'unit_floor_plan' => $unit_floor_plan_int])
          )->toString();

      $row->unit_type_id = $formlink;

      $rows[] = ['data' => (array) $row];

    }

    $build[] = [
      '#type' => 'link',
      '#title' => t('Add'),
      '#url' => Url::fromRoute('unit_type.add'),
    ];

    $build['unit_type_list_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => "The list is empty.  Please add data",
      '#sticky' => \TRUE,
    ];

    // C $build[] = ['#type' => 'pager'];.
    $build['pager'] = [
      '#type' => 'pager',
    ];

    $build['#cache']['max-age'] = 0;

    return $build;
  }

}
