<?php

namespace Drupal\organization_person\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;

/**
 * Class OrganizationPersonListController. Displays organization_person data.
 *
 * @package Drupal\organization_person\OrganizationPersonListController
 *
 * Substitutions:
 * Tblname. Replace with Tblname (init cap).
 * tblname.  Replace with organization_person.
 * Notes:  Apply replacements to namespace and use statements above.
 *         Class names. Replace underscore and following letter with capital
 *         letter.
 */
class OrganizationPersonListController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {

    $db = Database::getConnection('default', 'default');

    $null = NULL;

    $header = [
      ['data' => \t('edit'), 'field' => $null],
      [
        'data' => \t('organization Name (id)'),
        'field' => 'e.op_organization_id',
        'sort' => 'asc',
      ],
      ['data' => \t('person_name (id)'), 'field' => 'e.op_person_id'],
      [
        'data' => \t('organization_person_role'),
        'field' => 'e.organization_person_role',
      ],
      ['data' => \t('Delete'), 'field' => $null],
    ];

    $select = $db->select('organization_person', 'e')
      ->fields('e')
      ->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header)
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit()
      ->execute();

    $row = [];
    $rows = [];
    foreach ($select as $rowobj) {

      $op_organization_id = (string) $rowobj->op_organization_id;
      $op_person_id = (string) $rowobj->op_person_id;

      $rowobj->editrow = Link::fromTextAndUrl('edit', Url::fromRoute('organization_person.edit',
             [
               'op_organization_id' => $op_organization_id,
               'op_person_id' => $op_person_id,
             ]))->toString();
      $rowobj->delrow = Link::fromTextAndUrl('Delete',
                Url::fromRoute('organization_person.delete',
                  [
                    'op_organization_id' => $op_organization_id,
                    'op_person_id' => $op_person_id,
                  ]))->toString();

      $row[0] = $rowobj->editrow;
      $row[1] = (string) Routines::organizationName($rowobj->op_organization_id);
      $row[2] = (string) Routines::personName($rowobj->op_person_id);
      $row[3] = $rowobj->organization_person_role;
      $row[4] = $rowobj->delrow;

      $rows[] = ['data' => (array) $row];

    }

    $build[] = [
      '#type' => 'link',
      '#title' => t('Add'),
      '#url' => Url::fromRoute('organization_person.add'),
    ];

    $build['organization_person_list_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => "The list is empty.  Please add data",
      '#sticky' => \TRUE,
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    $build['#cache']['max-age'] = 0;

    return $build;

  }

 }
