<?php

namespace Drupal\organization_person\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class OrganizationPersonEditForm.
 *
 * @package Drupal\organization_person\Form\OrganizationPersonAddForm
 *
 * Substitutions:
 * Tblname. Replace with OrganizationPerson (init cap).
 * tblname. Replace with organization_person.
 * col02.   Replace with column name (i.e. organization_person_id).
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 */
class OrganizationPersonAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'organization_person_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form,
  FormStateInterface $form_state,
            $op_organizatioin_id = '',
  $op_person_id = '') {

    $form['add'] = [
      '#type'  => 'fieldset',
      '#title' => \t('Add a organization_person entry'),
      '#prefix' => '<div id="formadd" style="background-color:Cornsilk"',
      '#suffix' => '</div>',
    ];
    $form['add']['op_organization_id'] = [
      '#type'  => 'select',
      '#title' => t('organization_name'),
      '#options' => Routines::tableOptions('organization', 1, 5, \TRUE),
      '#required' => TRUE,
      // '#empty_value' => 0,
      // '#default_value' => 0,
      '#empty_option' => t('Please enter an organization'),
      '#description' => t("An organization to which the person belongss,"),
      '#prefix' => '<table><tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['op_person_id'] = [
      '#type'  => 'select',
      '#title' => t('person_name'),
      '#options' => Routines::tableOptions('person', 1, 5, \TRUE),
      '#required' => TRUE,
      // '#empty_value' => 0,
      // '#default_value' => 0,
      '#empty_option' => t('Please enter a person in the above organization'),
      '#description' => t("A person belonging to the selected,"),
      '#prefix' => '<table><tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['organization_person_role'] = [
      '#type'  => 'textfield',
      '#title' => \t('Organization-Person Role'),
      '#size'  => 20,
      '#description' => \t("The person's role in the above organization"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td></tr></table>',
    ];

    $form['add']['submit'] = [
      '#type'  => 'submit',
      '#value' => \t('Add'),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];

    $form['add']['cancel'] = [
      '#type'  => 'submit',
      '#value' => \t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
      '#prefix' => '<td>',
      '#suffix' => '</td></tr></table>',
    ];

    return $form;
  }

  /**
   * Validates a form for organization_person (optional).
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    /**
     * Validates a form for uniqueness.
     */
    if (Routines::dualUniqueColumnsValuesTest('organization_person', 'op_organization_id',
      $form_state->getValue('op_organization_id'), 'op_person_id',
      $form_state->getValue('op_person_id')) != 0) {
      $form_state->setErrorByName('unique organization/person', t('Please enter 
        a unique organization-person entry.'));
    }

  }

  /**
   * Adds organization_person form data.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');
    
    $row = [];
    
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
    
      $row[$key] = $value;
    }

    $tid = $db->startTransaction();
    try {
      $db->insert('organization_person')->fields($row)->execute();

      $message = \t('Insert of Organization-Person [')
              . $row['op_organization_id']
              . \t(' ') . $row['op_person_id']
              . \t('] successful');
      
      
      $this->messenger()->addMessage($message);

    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();

      $this->messenger()->addMessage(\t('Insert Failed. Nothing done. Error message
        follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
    }
    
    $form_state->setRedirect('organization_person.list');
  }

  /**
   * Cancels organization_person form addition.
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('organization_person.list');
  }

}
