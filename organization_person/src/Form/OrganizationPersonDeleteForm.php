<?php

namespace Drupal\organization_person\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\DatabaseExceptionWrapper;
Use Drupal\Core\Url;

/**
 * Class DefaultForm.
 *
 * @package Drupal\organization_person\Form\OrganizationPersonDeleteForm
 *
 *  * Substitutions:
 * Tblname. Replace with OrganizationPerson (init cap).
 * tblname.  Replace with organization_person.
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 */
class OrganizationPersonDeleteForm extends ConfirmFormBase {

  /*
  public $tblname_id;.
  public $greeting_text;.
  protected $pathAlias;.
  $this->pathAlias;.
   */

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'organization_person_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('organization_person.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

      $op_organization_id = (integer) \Drupal::request()->attributes->get('op_organization_id');
      $op_person_id = (integer) \Drupal::request()->attributes->get('op_person_id');
      
  $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Do you wish to delete this Organization/Person ID pair below? '),
      '#value' =>  $this->t($op_organization_id . "/" . $op_person_id),
      '#size' => 60,
      '#maxlength' => 128,
    ];
    
    return parent::buildForm($form, $form_state, $op_organization_id = '', $op_person_id = '');

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');

    $op_organization_id = (integer) \Drupal::request()->attributes->get('op_organization_id');
    $op_person_id = (integer) \Drupal::request()->attributes->get('op_person_id');
    
    $tid = $db->startTransaction();
    
    try {
      $nbr_deleted = $db->delete('organization_person')
        ->condition('op_organization_id', $op_organization_id, '=')
        ->condition('op_person_id', $op_person_id, '=')
        ->execute();
      if ($nbr_deleted != 1) {
        $tid->rollBack();
       \Drupal::messenger()->addError(
           $nbr_deleted .     $this->t(' rows deleted, 1 expected. Nothing done.'));
     }
      else {
      $this->messenger()->addMessage($this->t('Organization/Person deleted.'));
      }
    }
    
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      
      $this->messenger()->addMessage($this->t('Delete Failed. Nothing done. Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('organization_person.list');
    }

  }
