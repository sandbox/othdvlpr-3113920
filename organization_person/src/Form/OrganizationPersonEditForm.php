<?php

namespace Drupal\organization_person\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class OrganizationPersonEditForm.  Edits an organization/person pair.
 *
 * @package Drupal\organization_person\Form\OrganizationPersonEditForm
 *
 * Substitutions:
 * Tblname. Replace with OrganizationPerson (init cap).
 * tblname.  Replace with organization_person.
 * Note:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 * @todo . Consider moving BuildingAddForm::table_option routines.
 */
class OrganizationPersonEditForm extends FormBase {

  /**
   * {@inheritdoc}
   */

  /*
  protected function getEditableConfigNames() {
  return [
  'entitying_module.entity',
  ];
  }
   */

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'organization_person_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state,
          $op_organization_id = '',
          $op_person_id = '') {
  //  $op_organization_id = \Drupal::request()->attributes->get('op_organization_id');
  //  $op_person_id = \Drupal::request()->attributes->get('op_person_id');

    $db = Database::getConnection('default', 'default');

    $row_org_array = [];
    $row_prsn_array = [];
    $row_org_array = Routines::tableOptions('organization', 1, 5, \TRUE);
    $row_prsn_array = Routines::tableOptions('person', 1, 5, \TRUE);

    $select = $db->select('organization_person', 'e')
      ->fields('e')
      ->condition('e.op_organization_id', $op_organization_id, '=')
      ->condition('e.op_person_id', $op_person_id, '=')
      ->execute();
    $row = $select->fetchAssoc();

    $form['op_organization_id'] = [
      '#type'  => 'select',
      '#title' => t('organization_name'),
      '#options' => $row_org_array,
      '#required' => TRUE,
      '#size' => 10,
      '#default_value' => $op_organization_id,
      '#description' => t("An updated organization to which the person belongss,"),
      '#prefix' => '<table><tr><td>',
      '#suffix' => '</td>',
    ];
    $form['op_person_id'] = [
      '#type'  => 'select',
      '#title' => t('person_name'),
      '#options' => $row_prsn_array,
      '#required' => TRUE,
      '#size' => 10,

      '#default_value' => $op_person_id,
      '#description' => t("An updated person belonging to the selected,"),
      '#prefix' => '<table><tr><td>',
      '#suffix' => '</td>',
    ];
    $form['organization_person_role'] = [
      '#type'  => 'textfield',
      '#title' => \t('Organization-Person Role'),
      '#size'  => 20,
      '#default_value' => $row['organization_person_role'],
      '#description' => \t("The person's role in the above organization"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td></tr></table>',
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => t('Submit'),
    ];

    $form['actions']['cancel'] = [
      '#type'  => 'submit',
      '#value' => t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
    ];
    // C return parent::buildForm($form, $form_state);.
    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

      /*
     * Validates a form for uniqueness.
     */
    if (Routines::triUniqueColumnsValuesTest('organization_person', 'op_organization_id',
      $form_state->getValue('op_organization_id'), 'op_person_id',
      $form_state->getValue('op_person_id'), 'organization_person_role',
      $form_state->getValue('organization_person_role'))!= 0 ) {
      $form_state->setErrorByName('unique organization/person/role ',
                t('Please enter a unique organization-person-role entry.'));
    }
      
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection();
    
    $row = [];
    
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      $row[$key] = $value;

    }

    $tid = $db->startTransaction();
    try {
      $nbr_edited = $db->update('organization_person')->fields($row)
        ->condition('op_organization_id', $form['op_organization_id']['#default_value'], '=')
        ->condition('op_person_id', $form['op_person_id']['#default_value'], '=')
        ->execute();
      
      if ($nbr_edited != 1) {
        $tid->rollBack();
        $this->messenger()->addError(
            $nbr_edited . $this->t(' rows updated, 1 expected. Nothing done.'
                . ' Organization ID/Person ID = ')
                . $row['op_organization_id']
          . '/' . $row['op_person_id']); 
      }
      else {

      $this->messenger()->addMessage($this->t('Organization-Person Update Successful'));
      }
    }
      
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $this->messenger()->addMessage($this->t('Organization-Person update failed. 
        Nothing done. Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('organization_person.list');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('organization_person.list');
  }

}
