<?php

namespace Drupal\building\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class DefaultForm.
 *
 * @package Drupal\building\Form
 */
class BuildingDeleteForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'building_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
        return;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return  Url::fromRoute('building.list');
  }
  
  /*
   * {@inheritdoc}
   */
  public function getConfirmText() {
   return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $building_id = '') {
   
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Do you wish to delete these building ID below? '),
      '#value' =>  $this->t("$building_id"),
      '#size' => 60,
      '#maxlength' => 128,
    ];
    
    return parent::buildForm($form, $form_state, $building_id = '');

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');

    $building_id = (integer) \Drupal::request()->attributes->get('building_id');

    $tid = $db->startTransaction();
    // $building_id = 0;
    try {
      $nbr_deleted = $db->delete('building')
        ->condition('building_id', $building_id, '=')
        ->execute();
      
      if ($nbr_deleted != 1) {
        $tid->rollBack();
        \Drupal::messenger()->addError(
           $nbr_deleted  .   \t(' rows deleted, 1 expected. Nothing done.'));
       }
      else {
      \Drupal::messenger()->addMessage(t('Building delete successful'));
      }
    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      \Drupal::messenger()->addMessage($this->t('Delete Failed. Nothing done. Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>   $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('building.list');
  }

}
