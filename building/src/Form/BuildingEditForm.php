<?php

namespace Drupal\building\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class BuildingEditForm.
 *
 * @package Drupal\building_module\Form
 */
class BuildingEditForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'building_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $building_id = '') {

    $db = Database::getConnection('default', 'default');

    $select = $db->select('building', 'e')
      ->fields('e')
      ->condition('e.building_id', $building_id, '=')
      ->execute();
    $row = $select->fetchAssoc();

    $form['building_id'] = [
      '#type' => 'number',
      // '#markup' => check_plain($this->building_id),.
      '#disabled' => TRUE,
      '#title' => $this->t('Building ID'),
      '#description' => $this->t('The unique Building Identifier'),
      '#value' => $building_id,
      // '#default_value' => $config->getValue('building_id'),
     // '#default_value' => $form_state->getValue('building_id'),.
    ];
    $form['building_type_cd'] = [
      '#type' => 'select',
      '#title' => $this->t('Building Type Code'),
      '#description' => $this->t('Type of Building. Values: R - Residential (default), building contains residential units; C  - Complex, A building with multiple street addresses, such as an apartment-type building with ground-floor shops.  A - Asset, a building without units such as a clubhouse or utiltiy building.'),
      '#options' =>
        [
          'R' => $this->t('R'),
          'C' => $this->t('C'),
          'A' => $this->t('A'),
        ],
      '#size' => 3,
      '#default_value' => $row['building_type_cd'],
    ];

    $rowcoarr = Routines::complexOptions('building', 1, 5, \TRUE);

    $form['parent_complex_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Parent Complex ID'),
      '#description' => $this->t('Building Parent Complex ID, if any.
        Example: a high-rise complex with ground-floor shops having different
        street addresses; the upper floor residential units have the same
        street address but different apartment/door numbers.
        Zero indicates no parent complex.'),
      '#default_value' => $row['parent_complex_id'],
    // No select option if null or absent.
      '#empty_value' => 0,
      '#options' => $rowcoarr,
    ];
    $form['building_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Building Name'),
      '#description' => $this->t('A building name, if it exists.'),
      '#default_value' => $row['building_name'],
    ];
    $form['building_street_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Building Street Address'),
      '#description' => $this->t('Building Street Address.'),
      '#default_value' => $row['building_street_address'],
    ];
    $form['building_postal_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Building Postal Code'),
      '#description' => $this->t('Building Postal Code (optional).'),
      '#default_value' => $row['building_postal_code'],
    ];
    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => t('Submit'),
    // '#prefix' => '<tr><td>',
    // '#suffix' => '</td></tr></table>',.
    ];
    $form['actions']['cancel'] = [
      '#type'  => 'submit',
      '#value' => t('Cancel'),
      '#submit' => ['::cancelForm'],
    // '#prefix' => '<tr><td>',
    // '#suffix' => '</td></tr></table>',.
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if ($form_state->getValue('building_id') < 1) {
      $form_state->setErrorByName('building_id', $this->t('The building ID
       is invalid. Please enter a valid building ID.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');

    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      $row[$key] = $value;
      // \Drupal::messenger()->addMessage($key . '=>' . $row[$key]);.
    }

    $tid = $db->startTransaction();
    try {
      $nbr_edited = $db->update('building')->fields($row)
        ->condition('building_id', $row['building_id'], '=')
        ->execute();
      
      if ($nbr_edited != 1) {
        $tid->rollBack();
        $this->messenger()->addError(
            $nbr_edited . \t(' rows updated, 1 expected. Nothing done. Building ID = ')
                 . $row['building_id'] );
      }
      else {
      $this->messenger()->addMessage(t('Building update Successful'));
      }
    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $this->messenger()->addMessage($this->t('Building update Failed. Nothing done. 
        Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>   $e->getTraceAsString(), // $e->getTraceAsString()
      ]), 'error');
    }

    /* */
    /*
    foreach ($form_state->getValues() as $key => $value) {
    \Drupal::messenger()->addMessage($key . '=>' . $row[$key]);
    }

    // $rowarr = array();
    // $rowarr = (array) $rowobj;
    foreach ($row as $key => $value) {
    \Drupal::messenger()->addMessage($key . '=>' . $row[$key]);
    }
     */
    $form_state->setRedirect('building.list');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('building.list');
  }

}
