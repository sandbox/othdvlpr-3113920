<?php

namespace Drupal\building\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Building form.
 */
class BuildingAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'building_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['building_type_cd'] = [
      '#type'  => 'select',
      '#title' => $this->t('Building Type Code'),
      '#size'  => 1,
      '#options' => [
        'C' => t('C - Building Complex'),
        'A' => t('A - Asset'),
        'R' => t('R - Residential'),
      ],
      '#empty_option' => t('Residential'),
      '#default_value' => t('R'),
      '#description' => t("Type of building. Values: <br />
        &nbsp; &nbsp; blank - (default) building containing residential units. <br />
        &nbsp; &nbsp; C - Building complex. A building with multiple street addresses. <br />
        &nbsp; &nbsp; A - Asset.  A building without units, such as a clubhouse or utility building."),
      '#prefix' => '<table border=0 cellspacing=3 celladding=3><tr><td>',
      '#suffix' => '</td></tr>',
    ];

    $form['parent_complex_id'] = [
      '#type'  => 'select',
      '#title' => $this->t('parent_complex_id'),
    // $options_parent_complex_id_add,.
      '#options' => Routines::complexOptions('building', 1, 5, \TRUE),
    // NULL,.
      '#empty_value' => 0,
    // NULL,.
      '#default_value' => 0,
      '#empty_option' => t('None'),
    // '#size'  => 1,.
      '#description' => t("Building Parent Complex ID, if any. Example: a high-rise 
      complex with ground floor shops, each with their own street address; the upper
      floor residential units have the same street address, but different 
      apartment/door numbers. Zero indicates a building without a parent complex."),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td></tr>',
    ];
    $form['building_name'] = [
      '#type'  => 'textfield',
      '#title' => $this->t('building_name'),
      '#size'  => 20,
      '#description' => t("Building Name, if it exists"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td></tr>',
    ];

    $form['building_street_address'] = [
      '#type'  => 'textfield',
      '#title' => $this->t('building_street_address'),
      '#size'  => 60,
      '#description' => t("Building Street Address"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td></tr>',
    ];
    
    $form['building_postal_code'] = [
      '#type'  => 'textfield',
      '#title' => $this->t('building_postal_code'),
      '#size'  => 11,
      '#description' => t("Building Postal Code (optional)"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td></tr>',
    ];
    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Add'),
      '#prefix' => '<tr><td>',
    ];
    $form['actions']['cancel'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Cancel'),
      '#submit' => ['::cancelForm'],
        // '#prefix' => '<td>',.
      '#suffix' => '</td></tr></table>',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $db = Database::getConnection('default', 'default');
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {

      $row[$key] = $value;
      // drupal_set_message($key . '=>' . $row[$key]);.
    }

    $tid = $db->startTransaction();
    try {
      $db->insert('building')->fields($row)->execute();
      
      $this->messenger()->addMessage(\t('Building insert successful.'));
    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      
      $this->messenger()->addMessage(\t('Add Failed. Nothing done. Error message
        follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>   $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('building.list');

  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('building.list');
  }

}
