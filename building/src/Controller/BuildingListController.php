<?php

namespace Drupal\building\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Database\Database;

/**
 * Class assetEditForm.
 */
class BuildingListController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {

    // Global $db;
    // $db = \Drupal::database();
    $db = Database::getConnection('default', 'default');

    $null = NULL;
    $header = [

    ['data' => t('building_id'), 'field' => 'e.building_id', 'sort' => 'asc'],
    ['data' => t('building_type'), 'field' => 'e.building_type_cd'],
    // == '0' ? '' : 'e.parent_complex_id'),.
    ['data' => t('parent_complex_id'), 'field' => 'e.parent_complex_id'],
    ['data' => t('building_name'), 'field' => 'e.building_name'],
    ['data' => t('building_street_address'), 'field' => 'e.building_street_address'],
    ['data' => t('building_postal_code'), 'field' => 'e.building_postal_code'],
    // 'field' => 'e.building_id'),.
    ['data' => t('Ed/Del'), 'field' => $null],
    ];

    $select = $db->select('building', 'e')
      ->fields('e')
      ->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header)
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit()
      // ->range(0, 10)
      ->execute();

    /**
     * Translates building_type_cd to a description/
     * @param string $dkey
     *   The building_type_code.
     * @param string $decode
     *   The encoded code description.
     * @param bool $flip
     *   Inverts the decode array.
     */
    function decoder($dkey, $decode, $flip = TRUE) {

      if ($flip) {
        $decode = array_flip($decode);
      }

      $decode = array_change_key_case($decode, CASE_UPPER);
      $dkey = strtoupper($dkey);

      $decoded = array_search($dkey, $decode);

      return $decoded;

    }

    $decode = [
      '' => '',
      'BUILDING COMPLEX' => 'C',
      'ASSET' => 'A',
    ];

    $rowarr = [];

    foreach ($select as $row) {

      $building_id = (string) $row->building_id;

      $formlink = Link::fromTextAndUrl(\t($building_id), Url::fromRoute('building.edit', ['building_id' => $building_id]))->toString();

      $row->eddel = Link::fromTextAndUrl('Delete', Url::fromRoute('building.delete', ['building_id' => $building_id]))->toString();

      $row->building_id = $formlink;

      $row->building_type_cd = decoder($row->building_type_cd, $decode, FALSE);
      $rows[] = ['data' => (array) $row];

    }

    $build[] = [
      '#type' => 'link',
      '#title' => t('Add'),
      '#url' => Url::fromRoute('building.add'),
    ];

    $build['building_list_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => "The list is empty.  Please add data",
      '#sticky' => \TRUE,
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;
  }

}
