<?php

namespace Drupal\organization\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Database\Database;

/**
 * Class OrganizationEditForm.
 *
 * @package Drupal\organization\OrganizationListController
 *
 * Substitutions:
 * Tblname. Replace with Organization (init cap).
 * tblname.  Replace with organization.
 * Notes:  Apply replacements to namespace and use statements above.
 *         Class names. Replace underscore and following letter with capital
 *         letter.
 */
class OrganizationListController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {

    $db = Database::getConnection('default', 'default');

    $null = NULL;
    $rows = [];
    $header = [
      [
        'data' => t('org_id'),
        'field' => 'e.organization_id',
        'sort' => 'asc',
      ],
      ['data' => \t('organization_name'), 'field' => 'e.organization_name'],
      ['data' => t('organization_type'), 'field' => 'e.organization_type'],
      [
        'data' => t('organization_address_1'),
        'field' => 'e.organization_address_1',
      ],
      [
        'data' => t('organization_address_2'),
        'field' => 'e.organization_address_2',
      ],
      [
        'data' => t('organization_address_3'),
        'field' => 'e.organization_address_3',
      ],
      ['data' => t('organization_state'), 'field' => 'e.organization_state'],
      ['data' => t('org_postal_code'), 'field' => 'e.organization_postal_code'],
      ['data' => t('organization_country'), 'field' => 'e.organization_country'],
      ['data' => t('orgnztn_phone_nr'), 'field' => 'e.organization_phone_nr'],
      ['data' => t('org_website'), 'field' => 'e.organization_website'],
      ['data' => t('organization_note'), 'field' => 'e.organization_note'],
      ['data' => t('Delete'), 'field' => $null],
    ];

    $select = $db->select('organization', 'e')
      ->fields('e')
      ->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header)
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit()
      ->execute();

    foreach ($select as $row) {

      if (!empty($row->organization_website)) {
        $url = Url::fromUri($row->organization_website);
        $row->organization_website = Link::fromTextAndUrl(t("website"), $url);
      }

      $organization_id = (string) $row->organization_id;

      $row->organization_id = Link::fromTextAndUrl($this->t($organization_id), Url::fromRoute('organization.edit', ['organization_id' => $organization_id]))->toString();

      $row->eddel = Link::fromTextAndUrl('Delete', Url::fromRoute('organization.delete', ['organization_id' => $organization_id]))->toString();

      $rows[] = ['data' => (array) $row];

    }

    $build[] = [
      '#type' => 'link',
      '#title' => t('Add'),
      '#url' => Url::fromRoute('organization.add'),
    ];

    $build['organization_list_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => "The list is empty.  Please add data",
      '#sticky' => \TRUE,
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    $build['#cache']['max-age'] = 0;

    return $build;

  }

}
