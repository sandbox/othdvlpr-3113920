<?php

namespace Drupal\organization\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\DatabaseExceptionWrapper;
Use Drupal\Core\Url;

/**
 * Class DefaultForm.
 *
 * @package Drupal\organization\Form\OrganizationDeleteForm
 *
 *  * Substitutions:
 * Tblname. Replace with Organization (init cap).
 * tblname.  Replace with organization.
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 */
class OrganizationDeleteForm extends ConfirmFormBase {

  /*
  public $organization_id;.
  public $greeting_text;.
  protected $pathAlias;.
  $this->pathAlias;.
   */

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'organization_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return;
  }
  
  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('organization.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $organization_id = '') {

  $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Do you wish to delete this organization ID below? '),
      '#value' =>  $this->t("$organization_id"),
      '#size' => 60,
      '#maxlength' => 128,
    ];
    
    return parent::buildForm($form, $form_state, $organization_id = '');

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');

    $organization_id = (integer) \Drupal::request()->attributes->get('organization_id');
    // $organization_id = 999;
    
    $tid = $db->startTransaction();
    try {
      $nbr_deleted = $db->delete('organization')
        ->condition('organization_id', $organization_id, '=')
        // ->condition('nocol', 'xyz' , '=')
        ->execute();
      
      if ($nbr_deleted != 1) {
        $tid->rollBack();
       $this->messenger()->addError(
           $nbr_deleted .     \t(' rows deleted, 1 expected. Nothing done.'));
     }
      else {
      $this->messenger()->addMessage(\t('Organization deleted.'));
     }
    }
    
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      
      $this->messenger()->addMessage($this->t('Delete Failed. Nothing done. Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('organization.list');
  }

}
