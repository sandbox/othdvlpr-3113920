<?php

namespace Drupal\organization\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class OrganizationEditForm.
 *
 * @package Drupal\organization\Form\OrganizationAddForm
 *
 * Substitutions:
 * Tblname. Replace with Organization (init cap).
 * tblname. Replace with organization.
 * col02.   Replace with column name (i.e. tblname_id).
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 */
class OrganizationAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'organization_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $organization_id = '') {

    $form['add'] = [
      '#type'  => 'fieldset',
      '#title' => \t('Add a organization entry'),
      '#prefix' => '<div id="formadd" style="background-color:Cornsilk"',
      '#suffix' => '</div>',
    ];
    $form['add']['organization_name'] = [
      '#type'  => 'textfield',
      '#title' => \t('organization_name'),
      '#size'  => 25,
      '#description' => \t("organization name"),
      '#prefix' => '<table border=0 cellspacing=3 celladding=3><tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['organization_type'] = [
      '#type'  => 'select',
      '#title' => \t('organization_type'),
    // '#size'  => 15,
      '#description' => \t("Describes the type of organization. Values include:  CONTRACTOR,GOVT, PROFESSIONAL, VENDOR"),
      '#options' => [
        'CONTRACTOR' => \t('CONTRACTOR'),
        'GOVERNMENT' => \t('GOVERNMENT'),
        'PROFESSIONAL' => \t('PROFESSIONAL'),
        'COMMUNITY' => \t('COMMUNITY'),
        'BANKING/LENDING' => \t('BANKING/LENDING'),
        'VENDOR' => \t('VENDOR'),
        'OTHER' => \t('OTHER'),
      ],
      '#default_value' => \t('OTHER'),
      '#multiple' => FALSE,
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['organization_address_1'] = [
      '#type'  => 'textfield',
      '#title' => \t('organization_address_1'),
      '#size'  => 35,
      '#description' => \t("First Address Line"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['organization_address_2'] = [
      '#type'  => 'textfield',
      '#title' => \t('organization_address_2'),
      '#size'  => 35,
      '#description' => \t("Optional 2nd Address line"),
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    ];
    $form['add']['organization_address_3'] = [
      '#type'  => 'textfield',
      '#title' => \t('organization_address_3'),
      '#size'  => 35,
      '#description' => \t("Third address line"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['organization_state'] = [
      '#type'  => 'textfield',
      '#title' => \t('organization_state'),
      '#size'  => 20,
      '#description' => \t("State, Province or other Major Contry Division"),
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['organization_postal_code'] = [
      '#type'  => 'textfield',
      '#title' => \t('organization_postal_code'),
      '#size'  => 11,
      '#description' => \t("Postal Code"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['organization_country'] = [
      '#type'  => 'textfield',
      '#title' => \t('organization_country'),
      '#size'  => 15,
      '#description' => \t("Organization country"),
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['organization_phone_nr'] = [
      '#type'  => 'textfield',
      '#title' => \t('organization_phone_nr'),
      '#size'  => 15,
      '#description' => \t("Organization phone number"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['organization_website'] = [
      '#type'  => 'textfield',
      '#title' => \t('organization_website'),
      '#size'  => 60,
      '#description' => \t("Organization website"),
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['organization_note'] = [
      '#type'  => 'textarea',
      '#title' => \t('Organization note'),
      '#cols'  => 60,
      '#description' => \t("Organization note"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['submit'] = [
      '#type'  => 'submit',
      '#value' => \t('Add'),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];

    $form['add']['cancel'] = [
      '#type'  => 'submit',
      '#value' => \t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
      '#prefix' => '<td>',
      '#suffix' => '</td></tr></table>',
    ];

    return $form;
  }

  /**
   * Validates a form for organization (optional).
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (Routines::prependStringTest($form_state->getValue('organization_website'),
      ['HTTP://', 'HTTPS://', 'FTP://']) === \FALSE) {
      $form_state->setErrorByName('organization_website', t('Please prepend one 
     of the following to the website address:  http://, https://, 
     ftp://'));
    }
  }

  /**
   * Adds organization form data.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');

    $row = [];
    
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      $row[$key] = $value;
    }

    $tid = $db->startTransaction();
    try {
      $db->insert('organization')->fields($row)->execute(); // insert('tblnam1')
      
      
      $message = \t('Insert of Organization [') . $row['organization_name'] . \t('] successful');
      $this->messenger()->addMessage($message);
    }
    
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      
      $this->messenger()->addMessage(\t('Insert Failed. Nothing done. Error message
        follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
      
    }

    $form_state->setRedirect('organization.list');
  }

  /**
   * Cancels organization form addition.
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('organization.list');
  }

}
