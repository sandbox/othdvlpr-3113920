<?php

namespace Drupal\organization\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class OrganizationEditForm. Edits an organization entry.
 *
 * @package Drupal\organization\Form\OrganizationEditForm
 *
 * Substitutions:
 * Tblname. Replace with Organization (init cap).
 * tblname.  Replace with organization.
 * Note:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 * @todo . Consider moving BuildingAddForm::table_option routines.
 */
class OrganizationEditForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'organization_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $organization_id = '') {
   // $this->organization_id = \Drupal::request()->attributes->get('organization_id');
   // $organization_id = (integer) $this->organization_id;

    $db = Database::getConnection('default', 'default');
    $select = $db->select('organization', 'e')
      ->fields('e')
      ->condition('e.organization_id', $organization_id, '=')
      ->execute();
    $row = $select->fetchAssoc();

    $form['organization_id'] = [
      '#type' => 'number',
      '#disabled' => TRUE,
      '#title' => \t('Choose entry to update'),
      '#default_value' => $organization_id,
      '#ajax' => [
        'wrapper' => 'updateform',
        'callback' => 'organization_form_update_callback',
      ],
    ];

    $form['organization_name'] = [
      '#type' => 'textfield',
      '#title' => \t('Updated organization_name'),
      '#size' => 25,
      '#default_value' => $row['organization_name'],
    ];

    $form['organization_type'] = [
      '#type' => 'select',
      '#title' => \t('Updated organization_type'),
      // '#size' => 20,
      '#default_value' => $row['organization_type'],
      '#description' => \t("Describes the type of organization. Values include:  CONTRACTOR,GOVT, PROFESSIONAL, VENDOR"),
      '#options' => [
        'CONTRACTOR' => \t('CONTRACTOR'),
        'GOVERNMENT' => \t('GOVERNMENT'),
        'PROFESSIONAL' => \t('PROFESSIONAL'),
        'COMMUNITY' => \t('COMMUNITY'),
        'BANKING/LENDING' => \t('BANKING/LENDING'),
        'VENDOR' => \t('VENDOR'),
        'OTHER' => \t('OTHER'),
        ],
      '#multiple' => FALSE,
    ];

    $form['organization_address_1'] = [
      '#type' => 'textfield',
      '#title' => \t('Updated organization_address_1'),
      '#size' => 35,
      '#default_value' => $row['organization_address_1'],
      '#description' => \t("First Address Line"),
    ];

    $form['organization_address_2'] = [
      '#type' => 'textfield',
      '#title' => \t('Updated organization_address_2'),
      '#size' => 35,
      '#default_value' => $row['organization_address_2'],
      '#description' => \t("Optional 2nd Address line"),
    ];

    $form['organization_address_3'] = [
      '#type' => 'textfield',
      '#title' => \t('Updated organization_address_3'),
      '#size' => 35,
      '#default_value' => $row['organization_address_3'],
      '#description' => \t("Thrid line address"),
    ];

    $form['organization_state'] = [
      '#type' => 'textfield',
      '#title' => \t('Updated organization_state'),
      '#size' => 20,
      '#default_value' => $row['organization_state'],
      '#description' => \t("State, Province or other Major Contry Division"),
    ];

    $form['organization_postal_code'] = [
      '#type' => 'textfield',
      '#title' => \t('Updated organization_postal_code'),
      '#size' => 11,
      '#default_value' => $row['organization_postal_code'],
      '#description' => \t("Postal Code"),
    ];

    $form['organization_country'] = [
      '#type' => 'textfield',
      '#title' => \t('Updated organization_country'),
      '#size' => 15,
      '#default_value' => $row['organization_country'],
      '#description' => \t("Organization country"),
    ];

    $form['organization_phone_nr'] = [
      '#type' => 'textfield',
      '#title' => \t('Updated organization_phone_nr'),
      '#size' => 15,
      '#default_value' => $row['organization_phone_nr'],
      '#description' => \t("Organization phone number"),
    ];

    $form['organization_website'] = [
      '#type' => 'textfield',
      '#title' => \t('Updated organization_website'),
      '#size' => 60,
      '#default_value' => $row['organization_website'],
      '#description' => \t("Organization website"),
    ];

    $form['organization_note'] = [
      '#type'  => 'textarea',
      '#title' => \t('Updated organization note'),
      '#cols'  => 60,
      '#default_value' => $row['organization_note'],
      '#description' => \t("Organization note"),
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => \t('Submit'),
    // C '#prefix' => '<tr><td>',.
    // C '#suffix' => '</td></tr></table>',.
    ];

    $form['actions']['cancel'] = [
      '#type'  => 'submit',
      '#value' => \t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
    // C '#prefix' => '<tr><td>',.
    // C '#suffix' => '</td></tr></table>',.
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (Routines::prependStringTest($form_state->getValue('organization_website'),
      ['HTTP://', 'HTTPS://', 'FTP://']) === \FALSE) {
      $form_state->setErrorByName('organization_website', t('Please prepend one 
     of the following to the website address:  http://, https://, 
     ftp://'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection();

    $row['organization_id'] = $form_state->getValue('organization_id');
    $row['organization_name'] = $form_state->getValue('organization_name');
    $row['organization_type'] = $form_state->getValue('organization_type');
    $row['organization_address_1'] = $form_state->getValue('organization_address_1');
    $row['organization_address_2'] = $form_state->getValue('organization_address_2');
    $row['organization_address_3'] = $form_state->getValue('organization_address_3');
    $row['organization_state'] = $form_state->getValue('organization_state');
    $row['organization_postal_code'] = $form_state->getValue('organization_postal_code');
    $row['organization_country'] = $form_state->getValue('organization_country');
    $row['organization_phone_nr'] = $form_state->getValue('organization_phone_nr');
    $row['organization_website'] = $form_state->getValue('organization_website');
    $row['organization_note'] = $form_state->getValue('organization_note');

    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      $row[$key] = $value;
    }

    $tid = $db->startTransaction();
    try {
      $nbr_edited = $db->update('organization')->fields($row)
        ->condition('organization_id', $row['organization_id'], '=') // nocol
        ->execute();
      if ($nbr_edited != 1) {
        $tid->rollBack();
        $this->messenger()->addError(
            $nbr_edited . $this->t(' rows updated, 1 expected. Nothing done. Organization ID = ')
                 . $row['organization_id'] ); 
      }
      
      else {
      $this->messenger()->addMessage($this->t('Organization update Successful'));
      }
    }
    
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $this->messenger()->addMessage($this->t('Organization update failed. Nothing done. 
        Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(), // $e->getTraceAsString()
      ]), 'error');
    }

    $form_state->setRedirect('organization.list');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('organization.list');
  }

}
