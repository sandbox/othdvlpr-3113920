<?php

namespace Drupal\concern\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\DatabaseExceptionWrapper;
/**
 * Class ConcernEditForm.
 *
 * @package Drupal\concern\ConcernListController
 *
 * Substitutions:
 * Tblname. Replace with Concern (init cap).
 * tblname.  Replace with concern.
 * Notes:  Apply replacements to namespace and use statements above.
 *         Class names. Replace underscore and following letter with capital
 *         letter.
 */
class ConcernListController extends ControllerBase {
  
  protected $account;
  
  final public function __construct(AccountInterface $account) {
    $this->account = $account;
  }
  
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('current_user')
    );
  }  
  
  /**
   * {@inheritdoc}
   */
  public function content() {
    
    $db = Database::getConnection('default', 'default');
    // $concern_id = c.complaint_id;
    $null = NULL;
    $rows = [];
    $row = [];
    $header = [
      [
        'data' => t('Concern ID'),
        'field' => 'c.complaint_id',
        'sort' => 'desc',
      ],
      ['data' => t('Complaint Recorder'), 'field' => 'c.complaint_recording_person_id'],
      ['data' => t('Complainant'), 'field' => 'c.complaining_person_id'],
      ['data' => t('Concern Type'), 'field' => 'c.complaint_type'],
      ['data' => t('Concern Receipt_Date'), 'field' => 'c.complaint_receipt_date'],
      ['data' => t('Concern Source'), 'field' => 'c.complaint_source'],
      ['data' => t('Concern Status'), 'field' => 'c.complaint_status'],
      ['data' => t('Concern_Disposition'), 'field' => 'c.complaint_dispositiion'],
      ['data' => t('&nbsp&nbsp&nbspConcern_Text&nbsp&nbsp&nbsp'), 'field' => 'c.complaint_text'],
        ['data' => t('Delete')],
    ];
    
    $name = $this->account->getDisplayName();
    global $loginid;
    global $complainant_id; 
    $loginid = $name;
    
    $tid = $db->startTransaction();
    try {
      global $complainant_id;
      $query = $db->select('person', 'p')
      ->condition('p.person_login_id', $loginid);
      $query->addField('p','person_id');
      $query->distinct();
      $result = $query->execute();
      $complainant_id = $result->fetchField();
    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $this->messenger()->addMessage(\t('Complainant ID not found.
              . Users please contact site administrator for privileges.
              . Administrators, please use complaint, not concern, module.
        Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
      
      return $this->redirect('<front>');
    }
    
    $tid = $db->startTransaction();
    try {
    $select = $db->select('person', 'p')
      ->condition('p.person_login_id', $loginid);
      // ->isNotNull('p.person_id');
    $select->innerjoin('complaint','c', 'p.person_id = c.complaining_person_id');
    $select->fields('c',['complaint_id','complaint_recording_person_id', 'complaining_person_id'
        ,'complaint_type', 'complaint_receipt_date', 'complaint_source'
        , 'complaint_status', 'complaint_dispositiion', 'complaint_text']);
     $result = $select
       ->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header)
       ->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit()
       ->execute();
  }
  
  catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $errmessage = $e->getMessage();
      $messenger = $this->messenger();
      $messenger->addError(
            \t(
                'Concern list failed. Message =') . $errmessage
                  . \t('query=')
                  . $e->query_string);
    }
   
    foreach ($result as $rowobj) {
      
     $concern_id = (string) $rowobj->complaint_id;
      $complaint_id = (string) $rowobj->complaint_id;
      $complainant_id = (string) $rowobj->complaining_person_id;
      
     $rowobj->complaining_person_id = (string) Routines::personName($rowobj->complaining_person_id);
      
      if ($rowobj->complaint_recording_person_id == '00') {  
        $rowobj->complaint_recording_person_id =  (string) 'Office';
       } 
        else {
      $rowobj->complaint_recording_person_id =  
       (string) Routines::personName($rowobj->complaint_recording_person_id); 
       }     
    
      if ($rowobj->complaint_status === 'SUBMITTED') {
      $rowobj->complaint_id = Link::fromTextAndUrl($this->t($complaint_id),
              Url::fromRoute('concern.edit', ['concern_id' => $complaint_id]))->toString();
      
      $rowobj->del = Link::fromTextAndUrl('Delete', Url::fromRoute('concern.delete', ['concern_id' => $complaint_id]))->toString();
      }
        else {
          $rowobj->del = NULL;
        }
        
      $row[0] = $rowobj->complaint_id;
      $row[1] = $rowobj->complaint_recording_person_id;
      $row[2] = $rowobj->complaining_person_id;
      $row[3] = $rowobj->complaint_type;
      $row[4] = $rowobj->complaint_receipt_date;        
      $row[5] = $rowobj->complaint_source;       
      $row[6] = $rowobj->complaint_status;        
      $row[7] = $rowobj->complaint_dispositiion;        
      $row[8] = $rowobj->complaint_text;        
      $row[9] = $rowobj->del;       
              
      $rows[] = ['data' => (array) $row];
    }

    if (empty($complainant_id)) {
      $this->messenger()->addMessage($this->t('Complainant ID not on file.'
              . 'Users please contact site administrator for privileges.'
              . ' Administrators, please use complaint, not concern, module.'),'error');
    }
    
    else {
    $build[] = [
      '#type' => 'link',
      '#title' => t('Add'),
      '#url' => Url::fromRoute('concern.add', ['complainant_id' => $complainant_id]),
      ];
    $build['complaint_list_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => "The list is empty.  Please add data",
      '#sticky' => \TRUE,
      '#cache' => [
        'max-age' => 0,
      ],
    ];
    }
    $build['pager'] = [
      '#type' => 'pager',
    ];

    $build['#cache']['max-age'] = 0;

    return $build;

  }

}
