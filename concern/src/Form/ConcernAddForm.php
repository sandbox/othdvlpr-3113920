<?php

namespace Drupal\concern\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class ConcernEditForm.
 *
 * @package Drupal\concern\Form\ConcernAddForm
 *
 * Substitutions:
 * Tblname. Replace with Concern (init cap).
 * tblname. Replace with concern.
 * col02.   Replace with column name (i.e. concern_id).
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 */
class ConcernAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'concern_add';
  }

  /**
   * {@inheritdoc}
   */
  // , $complainant_id = '',  $loginid = ''
  public function buildForm(array $form,
          FormStateInterface $form_state, $complainant_id = '') {
    
    $dr = \Drupal::request();
    $complaining_person_id = $dr->attributes->get('complainant_id');
    $trueint = (int) 1;
    $falseint = (int) 0;
    $null = NULL;
    
    $form['add'] = [
      '#type'  => 'fieldset',
      '#title' => \t('Add a concern entry'),
      '#prefix' => '<div id="formadd" style="background-color:Cornsilk"',
      '#suffix' => '</div>',
    ];
    
    $form['add']['complaint_recording_person_id'] = [
      '#type'  => 'hidden',
      '#disabled' => TRUE,
      // '#options' => Routines::tableOptions('person',1,4,NULL,$form_state),
      '#description' => \t("Concern Recording Person ID"),
      '#empty_option' => 0,
      '#empty_value' => 0,
      '#default_value' => 0,
      '#prefix' => '<table<<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['complaining_person_id'] = [
      '#type'  => 'value',
      '#value' => $complaining_person_id,
      '#description' => \t("Complaining Person ID"),
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['complaint_type'] = [
      '#type'  => 'select',
      '#title' => \t('Concern Type'),
      '#size'  => 3,
      '#options' => [
          'CONDUCT' => \t('CONDUCT   '),
          'SERVICE' => \t('SERVICE   '),
          'OTHER' => \t('OTHER     '),
      ],
      '#default_value'  => t('OTHER'),
      '#description' => \t("Concern type. Values: CONDUCT, SERVICE, OTHER"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $today = \date("Y-m-d");
    $form['add']['complaint_receipt_date'] = [
      '#type'  => 'date',
      '#disabled' => TRUE,
      '#title' => \t('Concern Receipt Date'),
      '#description' => \t("The date concern was received"),
      '#value' => $today,
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    ];
    $form['add']['complaint_source'] = [
      '#type'  => 'value',
      '#title' => \t('concern_source'),
      '#value'  => 'WEBSITE',
      '#description' => \t("Concern source. Value: Website"), 
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['complaint_status'] = [
      '#type'  => 'value',
      '#title' => \t('Concern Status.'),
      '#description' => \t("Concern status. Values: SUBMITTED, OPEN, CLOSED,"
              . " REOPENED, ". "REJECTED"),
        '#value' => 'SUBMITTED', 
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['complaint_dispositiion'] = [
      '#type'  => 'hidden',
      '#disabled' => TRUE,
      '#title' => \t('Concern_dispositiion'),
      '#description' => \t("Concern_dispositiion"),
      '#value' => $null,
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['complaint_text'] = [
      '#type'  => 'textarea',
      '#title' => \t('concern_text'),
      '#cols'  => 40,
      '#rows' => 6,
      '#description' => \t("Concern_Text"),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td></tr>',
    ];
    
    $form['add']['submit'] = [
      '#type'  => 'submit',
      '#value' => \t('Add'),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',];

    $form['add']['cancel'] = [
      '#type'  => 'submit',
      '#value' => \t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
      '#prefix' => '<td>',
      '#suffix' => '</td></tr></table>',
    ];

    return $form;
  }

  /**
   * Validates a form for concern (optional).
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

}

  /**
   * Adds concern form data.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');
    
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {

      $value = empty($value) ? \NULL : $value;
      // $value = 'xyz';
      $row[$key] = $value;
    }

    $tid = $db->startTransaction();
    try {
      $db->insert('complaint')->fields($row)->execute();
        
      $message = \t('Insert of concern successful');
      $this->messenger()->addMessage($message);

    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      
      $this->messenger()->addMessage(\t('Insert Failed. Nothing done.
        Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>   $e->getTraceAsString(),
      ]), 'error');

      /* Optional managed_file upload (col19)
            if (!empty($row['col19'])) {
        Routines::addManagedFile($row['col19'], 'tblname');
      }
      */

    }

    /*
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      $this->messenger()->addMessage($key . \t('=') . $value);
    }
    */
    
    $form_state->setRedirect('concern.list');
  }

  /**
   * Cancels concern form addition.
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('concern.list');
  }

}
