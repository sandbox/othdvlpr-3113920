<?php

namespace Drupal\concern\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\DatabaseExceptionWrapper;
Use Drupal\Core\Url;

/**
 * Class DefaultForm.
 *
 * @package Drupal\concern\Form\ConcernDeleteForm
 *
 *  * Substitutions:
 * Tblname. Replace with Concern (init cap).
 * tblname.  Replace with concern.
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 */
class ConcernDeleteForm extends ConfirmFormBase {

  /*
  public $tblname_id;.
  public $greeting_text;.
  protected $pathAlias;.
  $this->pathAlias;.
   */

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'concern_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('concern.list');
  }
  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }
 
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $concern_id = '') {

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Do you wish to delete this Concern ID below? '),
      '#value' =>  $this->t("$concern_id"),
      '#size' => 60,
      '#maxlength' => 128,
    ];
    
    return parent::buildForm($form, $form_state, $complaint_id = '');

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

     $db = Database::getConnection('default', 'default');

    $complaint_id = (integer) \Drupal::request()->attributes->get('concern_id');
    
    $tid = $db->startTransaction();
    try {
     $nbr_deleted = $db->delete('complaint')
        ->condition('complaint_id', $complaint_id, '=')
        // ->condition('complaint_receipt_date', 'xyz' , '=')
        ->execute();

      /*
      if (!empty($managed_file_id)) {
        Routines::deleteManagedFile($managed_file_idn, 'tblname');
      }
      */
     if ($nbr_deleted != 1) {
       $this->messenger()->addError(
           $nbr_deleted .     \t(' rows deleted, 1 expected. Transaction rolled back.'));
               $tid->rollBack();              
     }
      else {
      $this->messenger()->addMessage(\t('Concern deleted.'));
     }
    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $this->messenger()->addMessage($this->t('Delete Failed. Nothing done. Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('concern.list');
  }

 }
