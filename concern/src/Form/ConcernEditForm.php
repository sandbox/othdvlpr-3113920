<?php

namespace Drupal\concern\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class ConcernEditForm.
 *
 * @package Drupal\concern\Form\ConcernEditForm
 *
 * Substitutions:
 * Tblname. Replace with Concern (init cap).
 * tblname.  Replace with concern.
 * Note:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 * @todo. Consider moving BuildingAddForm::table_option routines.
 */
class ConcernEditForm extends FormBase {

  /**
   * {@inheritdoc}
   */

  /*
  protected function getEditableConfigNames() {
  return [
  'entitying_module.entity',
  ];
  }
   */

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'concern_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $concern_id = '') {
        
    $db = Database::getConnection('default', 'default');
    $select = $db->select('complaint', 'e')
      ->fields('e')
      ->condition('e.complaint_id', $concern_id, '=')
      ->execute();
    $row = $select->fetchAssoc();
        
    $null = NULL;
    $form['complaint_id'] = [
      '#type' => 'number',
      // '#options' => $options,
      '#disabled' => TRUE,
      '#title' => t('Choose entry to update'),
      '#value' => $concern_id,
      '#description' => t("concern_id"),
      '#ajax' => [
        'wrapper' => 'updateform',
        'callback' => 'complaint_form_update_callback',
      ],
    ];

    $form['complaint_recording_person_id'] = [
      '#type'  => 'hidden',
      '#title' => t('Updated concern_recording_person_id'),
      '#empty_option' => 0,
      '#empty_value' => 0,
      '#default_value' => $row['complaint_recording_person_id'],
      '#description' => t("complaint_recorder"),
    ];

    $form['complaining_person_id'] = [
      '#type'  => 'number',
      '#disabled' => TRUE,
      '#title' => t('Updated complainant'),
      '#default_value' => $row['complaining_person_id'],
      '#description' => \t("Complainant"),
    ];

    $form['complaint_type'] = [
      '#type' => 'select',
      '#title' => t('Updated concern_type'),
      '#options' => [
          'CONDUCT' => \t('CONDUCT   '),
          'SERVICE' => \t('SERVICE   '),
          'OTHER' => \t('OTHER     '),
        ],
      '#default_value' => $row['complaint_type'],
      '#description' => \t("Concern type. Values: CONDUCT, SERVICE, OTHER"), 
    ];

    $form['complaint_receipt_date'] = [
      '#type' => 'date',
      '#attributes' =>['readonly' => 'readonly'],
      '#title' => t('Updated concern_receipt_date'),
      '#default_value' => $row['complaint_receipt_date'],
      '#description' => t("The date concern was received"),
    ];

    $form['complaint_source'] = [
      '#type' => 'textfield',
      '#size' => 7,
      '#title' => \t('Updated concern_source'),
      '#disabled' => TRUE,
      '#default_value' => $row['complaint_source'],
      '#description' => \t('Concern Source. Values:  WEBSITE, EMAIL, PHONE,
              LETTER, WALK-IN, BOD MEETING, COMMITTEE MEETING, OTHER.'),
    ];

    $form['complaint_status'] = [
      '#type' => 'textfield',
      '#size' => 9,
      '#disabled' => TRUE,
      '#title' => \t('Concern Status'),
      '#value' => 'SUBMITTED',
      '#description' => \t("Concern status. Values: SUBMITTED, OPEN, CLOSED,"
              . " REOPENED, " ."REJECTED"),         
    ];

    $form['complaint_dispositiion'] = [
      '#type' => 'textarea',
      '#title' => t('Updated Complaint Dispositiion'),
      '#cols'  => 40,
      '#rows' => 6,
      '#disabled' => TRUE,
      '#resizable' => FALSE,
      '#default_value' => $row['complaint_dispositiion'],
      '#description' => t("Concern Dispositiion"),
    ];

    $form['complaint_text'] = [
      '#type' => 'textarea',
      '#title' => t('Updated Concern Text'),
      '#cols'  => 40,
      '#rows' => 6,
      '#resizable' => FALSE,
      '#default_value' => $row['complaint_text'],
      '#description' => t("Concern Text"),
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => t('Submit'),
    ];

    $form['actions']['cancel'] = [
      '#type'  => 'submit',
      '#value' => t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection();

    $row['complaint_id'] = $form_state->getValue('complaint_id');
 
    $row['complaint_type'] = $form_state->getValue('complaint_type');
   
    $row['complaint_text'] = $form_state->getValue('complaint_text');
    
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      $row[$key] = $value;
    }

    $tid = $db->startTransaction();
    try {
      $nbr_edited =  $db->update('complaint')->fields(
            ['complaint_type' => $form_state->getValue('complaint_type'),
            'complaint_text' => $form_state->getValue('complaint_text')])
                  
        ->condition('complaint_id', $row['complaint_id'], '=')
        // ->condition('nocol', $row['nonrow'] , '=')
        ->execute();
      if ($nbr_edited != 1) {
        $this->messenger()->addError(
            $nbr_edited . \t(' rows updated, 1 expected. Nothing done.Concern ID = ')
                 . $row['complaint_id'] );
        $tid->rollBack();
     }
      else {
      $messenger = $this->messenger();
      $messenger->addMessage(\t('Concern update Successful'));
        }
    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $messenger = $this->messenger();
      $messenger->addMessage($this->t('Edit Failed. Nothing done. Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('concern.list');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('concern.list');
  }

}
