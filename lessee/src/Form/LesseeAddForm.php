<?php

namespace Drupal\lessee\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class LesseeAddForm. Adds to the lessee database.
 *
 * @package Drupal\lessee\Form\LesseeAddForm.
 *
 * Substitutions:
 * Tblname. Replace with Lessee (init cap).
 * tblname. Replace with lessee.
 * col02.   Replace with column name (i.e. lessee_id).
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 */
class LesseeAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lessee_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $lessee_id = '') {

    $trueint = (int) 1;
    $falseint = (int) 0;

    $form['add']['leased_unit_id'] = [
      '#type'  => 'select',
      '#title' => t('Leased Unit ID'),
      '#options'  => Routines::tableOptions('unit', 1, 7, \NULL, $form_state),
      '#description' => t("The Unit leased by the person or organization below"),

    ];
    $form['add']['lessee_type'] = [
      '#type'  => 'select',
      '#title' => t('Unit Lessee Type'),
      '#options' => [
        0 => \t('P'),
        1 => \t('O'),
      ],
      '#description' => t("Lessee Type.  Values:P - Person (default),O - Organization."),
    ];
    $form['add']['lessee_person_id'] = [
      '#type'  => 'select',
      '#title' => t('Lessee Person Identifier'),
      '#options'  => Routines::tableOptions('person', 1, 4, NULL, $form_state),
      '#empty_value' => '0',
      '#default_value' => $falseint,
      '#description' => t("The person leasing the unit"),
      '#states' => [
        'disabled' => [
          ':input[name="lessee_type"]' => ['value' => t("O")],
        ],
        'required' => [
          ':input[name="lessee_type"]' => ['value' => t("P")],
        ],
      ],
    ];
    $form['add']['lessee_organization_id'] = [
      '#type'  => 'select',
      '#title' => t('Lessee Organization Identifier'),
      '#options'  => Routines::tableOptions('organization', -1, -2, \NULL, $form_state),
      '#empty_value' => 0,
      '#default_value' => $falseint,
      '#description' => t("Lessee Identifier, if the it is an organization"),
      '#states' => [
        'disabled' => [
          ':input[name="lessee_type"]' => ['value' => t("P")],
        ],
        'required' => [
          ':input[name="lessee_type"]' => ['value' => t("O")],
        ],
      ],
    ];

    $form['add']['submit'] = [
      '#type'  => 'submit',
      '#value' => \t('Add'),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];

    $form['add']['cancel'] = [
      '#type'  => 'submit',
      '#value' => \t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
      '#prefix' => '<td>',
      '#suffix' => '</td></tr></table>',
    ];

    return $form;
  }

  /**
   * Validates a form for lessee (optional).
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('lessee_person_id'))
      && empty($form_state->getValue('lessee_organization_id'))) {
      $form_state->setErrorByName('lessie_name',
        \t('Please enter a person and/or organization name.'));
    }
  }

  /**
   * Adds lessee form data.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');

    foreach ($form_state->cleanValues()->getValues() as $key => $value) {

      $value = empty($value) ? \NULL : $value;
      // $value = 'xyz';
      $row[$key] = $value;
    }

    $row['lessee_type'] = !empty($row['lessee_organization_id']) ? 'O' : 'P';

    $tid = $db->startTransaction();
    try {
      $db->insert('lessee')->fields($row)->execute();

      $lessee_id = $row['lessee_organization_id'] ?? $row['lessee_personn_id'];
      $message = \t('Insert of person or organization ID [')
        . $lessee_id . \t('] successful');

      $this->messenger()->addMessage($message);

    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      
      $this->messenger()->addMessage(\t('Insert Failed. Nothing done. Error message
        follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>   $e->getTraceAsString(),
      ]), 'error');

    }
    /*
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
    \Drupal::messenger()->addMessage($key . \t('=') . $value);
    }
     */
    $form_state->setRedirect('lessee.list');
  }

  /**
   * Cancels lessee form addition.
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('lessee.list');
  }

}
