<?php

namespace Drupal\lessee\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class LesseeEditForm. Edits existing lessee information.
 *
 * @package Drupal\lessee\Form\LesseeEditForm
 *
 * Substitutions:
 * Tblname. Replace with Lessee (init cap).
 * tblname.  Replace with lessee.
 * Note:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 * @todo . Consider moving BuildingAddForm::table_option routines.
 */
class LesseeEditForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lessee_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $lessee_id = '') {

    $db = Database::getConnection('default', 'default');
    $select = $db->select('lessee', 'e')
      ->fields('e')
      ->condition('e.lessee_id', $lessee_id, '=')
      ->execute();
    $row = $select->fetchAssoc();

    $form['lessee_id'] = [
      '#type' => 'number',
      '#disabled' => \TRUE,
      '#title' => t('Choose entry to update'),
      '#default_value' => $row['lessee_id'],
      '#description' => t("Updated Unit Owner Identifier"),
    ];

    $form['leased_unit_id'] = [
      '#type' => 'select',
      '#title' => t('Updated Unit Leased ID'),
      '#options'  => Routines::tableOptions('unit', 1, 7, \NULL, $form_state),
      '#default_value' => $row['leased_unit_id'],
      '#description' => t("The unit leased by the person or organization below"),
    ];

    $form['lessee_type'] = [
      '#type' => 'select',
      '#title' => t('Updated Lessee Type'),
      '#options' => ['P' => \t('P'), 'O' => \t('O')],
      '#default_value' => $row['lessee_type'],
      '#description' => t("Lessee Type.  Values:P - Person (default),O - Organization."),
    ];

    $form['lessee_person_id'] = [
      '#type' => 'select',
      '#title' => t('Updated Lessee ID'),
      '#options'  => Routines::tableOptions('person', 1, 4, NULL, $form_state),
      '#empty_value' => 0,
      '#empty_option' => t('Select'),
      '#required' => 0,
      '#default_value' => $row['lessee_person_id'],
      // '#after_build' => array('lessee_update_after_build'),
      '#description' => t("The person leasing the unit"),
      /* '#states' => array(
        'disabled' => array(
          ':input[name="lessee_type"]' => array('value' => t("O")),
        ),
        'required' => array(
          ':input[name="lessee_type"]' => array('value' => t("P")),
        ),
      ),
        */
    ];

    $form['lessee_organization_id'] = [
      '#type' => 'select',
      '#title' => t('Updated Lessee Organization Identifier'),
      '#options'  => Routines::tableOptions('organization', -1, -2, NULL, $form_state),
      '#empty_value' => 0,
      '#empty_option' => t('Select'),
      '#required' => 0,
      '#default_value' => $row['lessee_organization_id'],
      // '#after_build' => array('lessee_update_after_build'),
      '#description' => t("Lessee Identifier, if it is an organization"),
        /*
      '#states' => array(
        'disabled' => array(
          ':input[name="lessee_type"]' => array('value' => t("P")),
        ),
        'required' => array(
          ':input[name="lessee_type"]' => array('value' => t("O")),
        ),
      ),
        */
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => t('Submit'),
    // C '#prefix' => '<tr><td>',.
    // C '#suffix' => '</td></tr></table>',.
    ];

    $form['actions']['cancel'] = [
      '#type'  => 'submit',
      '#value' => t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
    // C '#prefix' => '<tr><td>',.
    // C '#suffix' => '</td></tr></table>',.
    ];
    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (empty($form_state->getValue('lessee_person_id'))
      && empty($form_state->getValue('lessee_organization_id'))) {
      $form_state->setErrorByName('lessie_name',
              \t('Please enter a person and/or organization name.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection();

    foreach ($form_state->cleanValues()->getValues() as $key => $value) {

      $value = empty($value) ? \NULL : $value;

      $row[$key] = $value;
      /*
      $message = ($key . '=>' . $row[$key]);

      $messenger = $this->messenger();
      $messenger->addMessage($message);
       */
    }

    $row['lessee_type'] = !empty($row['lessee_organization_id']) ? 'O' : 'P';

    $tid = $db->startTransaction();
    try {
       $nbr_edited = $db->update('lessee')->fields($row)
        ->condition('lessee_id', $row['lessee_id'], '=') //   nocol
        ->execute();
       
      if ($nbr_edited != 1) {
        $tid->rollBack();
        
        $this->messenger()->addError(
            $nbr_edited . \t(' rows updated, 1 expected. Nothing done. Lessee ID = ')
                 . $row['lessee_id'] ); 
      }
      else {
            $this->messenger()->addMessage(\t('Lessee Update Successful'));
      }
    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      
      $this->messenger()->addMessage(\t('Update Failed. Nothing done. Error message
        follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>   $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('lessee.list');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('lessee.list');
  }

}
