<?php

namespace Drupal\lessee\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\DatabaseExceptionWrapper;
Use Drupal\Core\Url;

/**
 * Class DefaultForm.
 *
 * @package Drupal\lessee\Form\LesseeDeleteForm
 *
 *  * Substitutions:
 * Tblname. Replace with Lessee (init cap).
 * tblname.  Replace with lessee.
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 */
class LesseeDeleteForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lessee_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('lessee.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $lessee_id = '') {
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Do you wish to delete this Lessee ID below? '),
      '#value' =>  $this->t("$lessee_id"),
      '#size' => 60,
      '#maxlength' => 128,
    ];
    
    return parent::buildForm($form, $form_state, $Lessee_id = '');

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
    $db = Database::getConnection('default', 'default');
    $lessee_id = (integer) \Drupal::request()->attributes->get('lessee_id');
 
    $tid = $db->startTransaction();
    try {
      $nbr_deleted = $db->delete('lessee')
        ->condition('lessee_id', $lessee_id, '=')
        ->execute();
      
     if ($nbr_deleted != 1) {
        $tid->rollBack();
        $this->messenger()->addError(
           $nbr_deleted .     \t(' rows deleted, 1 expected. Nothing done.'));
     }
      else { 
      
      $this->messenger()->addMessage(\t('Lessee deleted.'));
      }
    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $this->messenger()->addMessage($this->t('Delete Failed. Nothing done. Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('lessee.list');
  }

}
