<?php

namespace Drupal\lessee\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Database\Database;

/**
 * Class LesseeListController. Displays lessee data.
 *
 * @package Drupal\lessee\LesseeListController
 *
 * Substitutions:
 * Tblname. Replace with Lessee (init cap).
 * tblname.  Replace with lessee.
 * Notes:  Apply replacements to namespace and use statements above.
 *         Class names. Replace underscore and following letter with capital
 *         letter.
 */
class LesseeListController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {

    $db = Database::getConnection('default', 'default');

    $null = NULL;
    $rows = [];

    $lesseeexpression = 'lessee_name';

    $select = $db->select('lessee', 't');

    $select->addField('t', 'lessee_id');
    $select->addField('t', 'leased_unit_id');

    $select->addField('t', 'lessee_type');
    $select->addField('t', 'lessee_person_id');
    $select->addField('t', 'lessee_organization_id');

    $select->addJoin('LEFT OUTER', 'person', 'p', 't.lessee_person_id = p.person_id');
    $personname = "concat_ws(',  ', p.person_last_name, p.person_first_name, 
        concat(' ',replace(p.person_middle_initial,'',NULL),'.'))";
    $select->addJoin('LEFT OUTER', 'organization', 'o', 'o.organization_id = t.lessee_organization_id');

    $lesseename = "CASE
            WHEN ISNULL(o.organization_name) THEN
              CONCAT_WS(',  ', p.person_last_name, p.person_first_name, 
              CONCAT(' ',replace(p.person_middle_initial,'',NULL),'.'))
            WHEN ISNULL(p.person_last_name) THEN o.organization_name
            ELSE CONCAT_WS('/', o.organization_name, p.person_last_name)
            END";

    $select->addExpression($lesseename, $lesseeexpression);

    $select->addJoin('INNER', 'unit', 'u', 't.leased_unit_id = u.unit_id');
    $select->addField('u', 'unit_code');

    $header = [
      ['data' => \t('lessee_id'), 'field' => 't.lessee_id', 'sort' => 'DESC'],
      ['data' => \t('leased_unit_id'), 'field' => 't.leased_unit_id'],
      ['data' => \t('lessee name'), 'field' => $lesseeexpression],
      ['data' => \t('leased_unit_code'), 'field' => 'u.unit_code'],
      ['data' => \t('lessee_type'), 'field' => 't.lessee_type'],
      [
        'data' => \t('lessee_person_id'),
        'field' => 't.lessee_person_id',
      ],
      [
        'data' => \t('lessee_organization_id'),
        'field' => 't.lessee_organization_id',
      ],
      ['data' => t('Delete'), 'field' => $null],
    ];
    $entries = $select
      ->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header)
      ->execute();

    $row = [];

    foreach ($entries as $rowobj) {
      $lessee_id = (string) $rowobj->lessee_id;

      $rowobj->lessee_id = Link::fromTextAndUrl($this->t($lessee_id),
        Url::fromRoute('lessee.edit',
        ['lessee_id' => $lessee_id]))->toString();

      $row[0] = $rowobj->lessee_id;
      $row[1] = $rowobj->leased_unit_id;
      $row[2] = $rowobj->$lesseeexpression;
      $row[3] = $rowobj->unit_code;
      $row[4] = $rowobj->lessee_type;
      $row[5] = $rowobj->lessee_person_id;
      $row[6] = $rowobj->lessee_organization_id;

      $row[7] = Link::fromTextAndUrl('Delete',
        Url::fromRoute('lessee.delete',
        ['lessee_id' => $lessee_id]))->toString();

      $rows[] = ['data' => (array) $row];

    }

    $build[] = [
      '#type' => 'link',
      '#title' => t('Add'),
      '#url' => Url::fromRoute('lessee.add'),
    ];

    $build['lessee_list_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => "The list is empty.  Please add data",
      '#sticky' => \TRUE,
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    $build['#cache']['max-age'] = 0;

    return $build;

  }

}
