<?php

namespace Drupal\common;

use Drupal\image\Entity\ImageStyle;
use Drupal\Core\Database\Database;
use Drupal\file\Entity\File;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\File\FileUrlGeneratorInterface;
/**
 * Common routines.
 */
class Routines {
  /**
   * {@inheritdoc}
   */

  protected $messenger;

  /**
   * Displays the rows of a parent table for selection.
   *
   * @param string $tblname
   *   The name of the table to be displayed.
   * @param int $nrkeys
   *   The number of key fields in the table.
   * @param int $nrfields
   *   The number of fields in the table.
   * @param bool $alignfields
   *   If the fields need alignment for readability.
   * @param string $form_state
   *   The state of the form.
   * @param array $rowsubset
   *   The subset of rows to be displayed.
   */
  public static function tableOptions($tblname, $nrkeys = 1, $nrfields = NULL, $alignfields = \FALSE, &$form_state = NULL, array $rowsubset = NULL) {
    $db = Database::getConnection('default', 'default');

    $colnames = ['column_name'];
    $select = $db->select('information_schema.columns', 'isc')
      ->fields('isc', $colnames);

    $group = $select->andConditionGroup()
      ->condition('isc.table_name', $tblname);
    $select->condition($group);
    $result = $select->execute();
    $fieldsobj = $result->fetchCol();

    $fields = (array) $fieldsobj;

    $fcount = (is_null($nrfields)) ? count($fields) : min(count($fields), $nrfields);

    $fkeys = array_slice($fields, 0, $nrkeys);

    $dispfields = array_slice($fields, 0, $fcount);
    
    $query = $db->select($tblname, 'ut')
      ->fields('ut', $dispfields)
      ->execute();

    $arresults = $query->fetchAllAssoc($dispfields[0], \PDO::FETCH_ASSOC);
    // $alignfields = \FALSE;
    if ($alignfields) {
      Routines::tableOptionsAlign($arresults, $fields, $fcount);
    }
    
    /* options row shift diagnostics
    $dfimpld = \implode(',',$arresults);
    \Drupal::messenger()->addMessage($dfimpld);
    */
      
    $options = [];
    $row = [];
    $rowlatest = [];
    // Archived dpms deleted.
    foreach ($arresults as $rowkeys => $rowval) {

      $row = (array) $rowval;

      \array_splice($rowval, $nrkeys, 0, ':'); // $rowval

      $options[$rowkeys] = t(\implode(' ', $rowval));
      
      /* options row shift diagnostics
      \Drupal::messenger()->addMessage($options[$rowkeys]);
       */  
    }

    $optionsorted = ksort($options);
    /*
    $dfimpld = \implode(',',$options);
    \Drupal::messenger()->addMessage($dfimpld);
     */
    return $options;

  }

  /**
   * Aligns the fields in a table for better readability.
   *
   * @param array $arresults
   *   The query results from a table.
   * @param array $fields
   *   A list of all table's field names.
   * @param int $fcount
   *   The number of fields to be aligned.
   *
   * @return array
   *   Aligned fields. Underscores are appended to fields to equate field lengths.
   */
  public static function tableOptionsAlign(array $arresults, array $fields, $fcount) {
    $maxvals = [];
    foreach ($arresults as $row) {

      foreach ($row as $colname => $colvalue) {

        $colvalength = strlen($colvalue ?? '');

        if (!isset($maxvals[$colname])) {
          $maxvals[$colname] = 0;
        }
        if ($colvalength > $maxvals[$colname]) {
          $maxvals[$colname] = $colvalength;
        }

      }
    }

    foreach ($arresults as $rownum => $row) {

      foreach ($row as $colname => $colvalue) {

        $padlength = $maxvals[$colname];

        if ($colname != $fields[$fcount - 1]) {
          $arresults[$rownum][$colname] = str_pad($arresults[$rownum][$colname] ?? '', $padlength, '_');
        }

      }
    }
    return $arresults;
  }

  /**
   * Converts empty column strings or zeros to null values.
   *
   * @param array $row
   *   database columns to be added or updated via DBTNG.
   * @param string $colzero
   *   Database column name with a zero value.
   *
   * @return array
   *   Columns whose empty strings or zero value are replaced by null values.
   */
  public function zeroStringToNullConvert(array $row, $colzero = NULL) {
    foreach ($row as $column => $value) {

      if ($value === '') {
        $value = NULL;
      }
      elseif (isset($colzero)
      && $column === $colzero
      && $value === 0) {

        $value = NULL;
      }

      $row[$column] = $value;
    }
    return $row;

  }

  /**
   * Building complex options for tableOptions.
   */
  public static function complexOptions($tblname, $nrkeys = 1, $nrfields = NULL, $alignfields = \FALSE, &$form_state = NULL, $rowsubset = NULL) {

    $tableoptionsarr = Routines::tableOptions($tblname, $nrkeys, $nrfields, \TRUE);
    $complexoptions = [];

    foreach ($tableoptionsarr as $rowkey => $rowval) {
      $rowexpld = explode(' ', $rowval);
      $colonkeypos = array_search(':', $rowexpld);
      $buildingtypecdpos = $colonkeypos + 1;
      $buildingcomplexidpos = $colonkeypos + 2;
      if ($rowexpld[$buildingtypecdpos] == 'C') {
        $complexoptions[$rowkey] = $rowval;
      }
    }

    foreach ($complexoptions as $key => $value) {
    }

    return $complexoptions;
  }

  /**
   * Replaces organization_name from organization_id.
   *
   * @param int $organization_id
   *   Unique organization identifieer..
   *
   * @return string
   *   The Organization name.
   */
  public static function organizationName($organization_id) {

    $db = Database::getConnection('default', 'default');

    $organization_name = '';
    $select = $db->select('organization', 'e')
      ->fields('e')
      ->condition('e.organization_id', $organization_id, '=')
      ->execute();
    $row = $select->fetchAssoc();

    if (!empty($row['organization_name'])) {
      $organization_name = $row['organization_name'] . ' (' . $row['organization_id'] . ')';
    }
    return $organization_name;
  }
  
  /**
   * Replaces person name from person ID.
   */
  public static function personName($person_id) {

    $db = Database::getConnection('default', 'default');

    $select = $db->select('person', 'e')
      ->fields('e')
      ->condition('e.person_id', $person_id, '=')
      ->execute();
    $row = $select->fetchAssoc();

    $person_full_name = '';
    if (!empty($row['person_last_name'])) {
      $person_full_name = $row['person_last_name'];
    }
    if (!empty($row['person_first_name'])) {
      $person_full_name = $person_full_name . ', ' . $row['person_first_name'];
    }
    if (!empty($row['person_middle_initial'])) {
      $person_full_name = $person_full_name . ' ' . $row['person_middle_initial'] . '.';
    }
    if (!empty($person_full_name)) {
      $person_full_name = $person_full_name . ' (' . $person_id . ')';
    }
    if (!empty($person_full_name)) {
      return $person_full_name;
    } else {
        return NULL;
    }
  }

  /**
   * Adds a managed file ID to a module entity.
<<<<<<< Updated upstream
=======
   *
   * @param int $fid
   *   Managed file id.
   * @param string $entity_id
   *   The managed file module name.
>>>>>>> Stashed changes
   */
  public static function addManagedFile($fid, $entity_id) {
    $fileobj = File::load($fid);

    $fileobj->FILE_EXISTS_RENAME;
    // Wsod.
    $fileobj->setPermanent();
    $fileobj->save();

  }

  /**
   * Deletes a managed file.
   *
   * @param int $fid
   *   Managed file id.
   * @param string $entity_id
   *   The managed file module name.
   */
  public static function deleteManagedFile($fid, $entity_id) {
    $fileobj = File::load($fid);
    if (!empty($fileobj)) {
      // Make this the only delete stmt?
      $fileobj->setTemporary();
      $fileobj->delete();
    }
  }

  /**
   * Edits a managed file.
   *
   * @param int $new_fid
   *   The replacement managed file ID.
   * @param int $existing_fid
   *   The original managed file ID to be replaced or deleted.
   * @param string $entity_id
   *   The module entity name of the module containing the managed file.
   */
  public static function editManagedFiles($new_fid, $existing_fid, $entity_id) {
    if (!empty($new_fid)
        && empty($existing_fid)) {
      Routines::addManagedFile($new_fid, $entity_id);
    }
    elseif (empty($new_fid)
        && !empty($existing_fid)) {
      Routines::deleteManagedFile($existing_fid, $entity_id);
    }
    elseif (!empty($new_fid)
        && !!empty($existing_fid)
        && $new_fid != $existing_fid) {
      Routines::deleteManagedFile($existing_fid, $entity_id);
      Routines::addManagedFile($new_fid, $entity_id);
    }
  }

  /**
   * Create a link from a managed file ID.
   *
   * @param mixed $fid
   *   Managed file ID.
   *
   * @return link
   *   Url of the managed file.
   */
  public static function displayManagedFile($fid) {
    $fid = (integer) $fid;
    if (!empty($fid)) {

      $fileobj = File::load($fid);

      if ($fileobj) {
        $fileobj->setPermanent();
        $fileobj->save();

        $filename = $fileobj->filename->value;

        // Get public path.
        $uripath = $fileobj->getFileUri();

        $urlobj = \Drupal::service('file_url_generator')->generate($uripath);

        return Link::fromTextAndUrl("$filename", $urlobj)->toString();
      }
      else {
        return NULL;
      }
    }
  }

  /**
   * Create image link from a managed file ID.
   *
   * @param mixed $fid
   *   Managed file ID.
   *
   * @return link
   *   Url of the managed file.
   */
  public static function displayManagedFileImage($fid) {
    $fid = (integer) $fid;
    if (!empty($fid)) {
      $fileobj = File::load($fid);
        if (!\is_null($fileobj)) {
          $filename = $fileobj->filename->value;
          $url = ImageStyle::load('medium')->buildUrl($fileobj->getFileUri());
          $urlobj = Url::fromUri($url);

      return Link::fromTextAndUrl("$filename", $urlobj);
        }
    }
  }

  /**
   * Prepends http://, https://, or ftp:// to an image link.
   *
   * @param mixed $ip_string
   *   Existing Url type.
   * @param mixed $prepend_array
   *   Url.
   *
   * @return bool|int
   *   Returns TRUE if the link is prepended and false if it is not.
   */
  public static function prependStringTest($ip_string, $prepend_array) {
    if (empty($ip_string)) {
      return 0;
    }
    foreach ($prepend_array as $prepend_element) {
      if (\mb_strpos(\mb_strtoupper($ip_string), \mb_strtoupper($prepend_element)) === 0) {
        return 0;
      }
    }
    return \FALSE;
  }

  /**
   * Determines the [non]unique status of 2 column values of a table.
   *
   * @param mixed $table_name
   *   Table whose column is to be examined.
   * @param mixed $column1_name
   *   First of 2 oolumn names whose uniqueness/non-uniqueness is to be examined.
   * @param mixed $column1_value
   *   First of 2 values to test for uniqueness.
   *
   * @param mixed $column2_name
   *   Second column name whose uniqueness/non-uniqueness is to be examined.
   * @param mixed $column2_value
   *   Second of 2 values to test for uniqueness.
   * @return int
   *   Returns number of existing values of $column_name, 0 if unique.
   */
  
  public static function dualUniqueColumnsValuesTest($table_name,
  $column1_name,
  $column1_value,
  $column2_name,
  $column2_value) {

    if (!(empty(($column1_value) || \ctype_space($column1_value)
     && ($column2_value) || \ctype_space($column2_value))
    )) {

      $db = Database::getConnection('default', 'default');

      $query = $db->select($table_name)
        ->condition($column1_name, $column1_value, '=')
        ->condition($column2_name, $column2_value, '=');

      $num_rows = $query->countQuery()->execute()->fetchField();

      $message = 'Routines column_values/num_rows=' . $column1_value . ' ' . $column2_value . '/' . $num_rows;
      \Drupal::messenger()->addMessage($message);
      return (integer) $num_rows;

    }
    // Empty values and spaces are ignored, NULLed in submitForm.
    else {
      return 0;
    }
 }
  
 /**
   * Determines the [non]unique status of 3 column values of a table.
   *
   * @param mixed $table_name
   *   Table whose column is to be examined.
   * 
   * @param mixed $column1_name
   *   First of 3 oolumn names whose uniqueness/non-uniqueness is to be examined.
   * @param mixed $column1_value
   *   First of 3 values to test for uniqueness.
   *
   * @param mixed $column2_name
   *   Second column name whose uniqueness/non-uniqueness is to be examined.
   * @param mixed $column2_value
   *   Second of 3 values to test for uniqueness.
   * 
   * @param mixed $column3_name
   *   Third column name whose uniqueness/non-uniqueness is to be examined.
   * @param mixed $column3_value
   *   Third of 3 values to test for uniqueness.
   * 
   * @return int
   *   Returns number of existing values of $column_name, 0 if unique.
   */
  public static function triUniqueColumnsValuesTest($table_name,
  $column1_name,
  $column1_value,
  $column2_name,
  $column2_value,
  $column3_name,
  $column3_value,      
          ) {

    if (!(empty(($column1_value) || \ctype_space((string) $column1_value)
     && ($column2_value) || \ctype_space((string) $column2_value)
     && ($column3_value) || \ctype_space((string) $column3_value))
    )) {

      $db = Database::getConnection('default', 'default');

      $query = $db->select($table_name)
        ->condition($column1_name, $column1_value, '=')
        ->condition($column2_name, $column2_value, '=')
        ->condition($column3_name, $column3_value, '=');
      $num_rows = $query->countQuery()->execute()->fetchField();

      // $message = 'Routines column_value/num_rows=' . $column_value . '/' . $num_rows;
      //\Drupal::messenger()->addMessage($message);
      return (integer) $num_rows;

    }
    // Empty values and spaces are ignored, NULLed in submitForm.
    else {
      return 0;
    }

  }
 
  /**
   * Determines the [non]unique status of a table's column value .
   *
   * @param mixed $table_name
   *   Table whose column is to be examined.
   * @param mixed $column_name
   *   Column name whose uniqueness/non-uniqueness is to be examined.
   * @param mixed $column_value
   *   Value to test for uniqueness.
   *
   * @return int
   *   Returns number of existing values of $column_name, 0 if unique.
   */
  public static function uniqueColumnValueTest($table_name,
  $column_name,
          $column_value) {

    if (!(empty($column_value) || \ctype_space($column_value))) {

      $db = Database::getConnection('default', 'default');

      $query = $db->select($table_name)
        ->condition($column_name, $column_value, '=');

      $num_rows = $query->countQuery()->execute()->fetchField();

      // $message = 'Routines column_value/num_rows='
      // . $column_value . '/' . $num_rows;
      // \Drupal::messenger()->addMessage($message);
      return (integer) $num_rows;

    }
    // Empty values and spaces are ignored, NULLed in submitForm.
    else {
      return 0;
    }

  }

  /**
   * Provides address information of a unit.
   *
   * @param int $unit_id
   *   The unit identifier.
   * @return string $unit_address
   *   The unit's ID/code, street name, number and door ID.
   */
  public static function unitAddress($unit_id) {

    $db = Database::getConnection('default', 'default');

    // ->fields('e'); //->execute();
    $select = $db->select('unit', 'u')
      ->fields('u')
      ->condition('u.unit_id', $unit_id, '=')
      ->execute();
    $row = $select->fetchAssoc();

    $unit_address = (string) $unit_id;
    
    $unit_address = !empty($row['unit_code']) ? 
     ( $unit_address . '/' .  $row['unit_code'])  : $unit_address;
 
    if (!empty($row['unit_street_number'])) {
      $unit_address = $unit_address . ' ' . $row['unit_street_number'];
    }
    
    $unit_address = !empty($row['unit_street_name']) ? 
            $unit_address . ' ' .  $row['unit_street_name'] : $unit_address;
    
    $unit_address = !empty($row['unit_door_id']) ? 
            $unit_address . ' ' .  $row['unit_door_id'] : $unit_address;
   
    return $unit_address;
  }
  
}
