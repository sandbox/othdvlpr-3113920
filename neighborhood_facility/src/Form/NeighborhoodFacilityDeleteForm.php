<?php

namespace Drupal\neighborhood_facility\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\DatabaseExceptionWrapper;
Use Drupal\Core\Url;

/**
 * Class DefaultForm.
 *
 * @package Drupal\neighborhood_facility\Form\NeighborhoodFacilityDeleteForm
 *
 *  * Substitutions:
 * Tblname. Replace with Neighborhood_facility (init cap).
 * tblname.  Replace with neighborhood_facility.
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 */
class NeighborhoodFacilityDeleteForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'neighborhood_facility_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('neighborhood_facility.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

    /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $neighborhood_facility_id = '') {

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Do you wish to delete this Neighborhood Facility ID below? '),
      '#value' =>  $this->t("$neighborhood_facility_id"),
      '#size' => 60,
      '#maxlength' => 128,
    ];
    
    return parent::buildForm($form, $form_state, $neighborhood_facility_id = '');

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');

    $neighborhood_facility_id = (integer) \Drupal::request()->attributes->get('neighborhood_facility_id');

    $tid = $db->startTransaction();
    // $neighborhood_facility_id = 0;
    
    try {
    $nbr_deleted = $db->delete('neighborhood_facility')
        ->condition('neighborhood_facility_id', $neighborhood_facility_id, '=')
        ->execute();
      
       if ($nbr_deleted != 1) {
        $tid->rollBack();
       $this->messenger()->addError(
           $nbr_deleted .     \t(' rows deleted, 1 expected. Nothing done.'));
     }
      else {
      $this->messenger()->addMessage(\t('Neighborhood_facility delete successful'));
     }
    }
    
    catch (DatabaseExceptionWrapper $e) {
      
      $tid->rollBack();
      $this->messenger()->addMessage($this->t('Delete Failed. Nothing done. Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('neighborhood_facility.list');
  }

}
