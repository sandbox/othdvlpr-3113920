<?php

namespace Drupal\neighborhood_facility\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class NeighborhoodFacilityEditForm.
 *
 * @package Drupal\neighborhood_facility\Form\NeighborhoodFacilityAddForm
 *
 * Substitutions:
 * Tblname. Replace with Neighborhood_facility (init cap).
 * tblname.  Replace with neighborhood_facility.
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 */
class NeighborhoodFacilityAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'neighborhood_facility_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['neighborhood_facility_parent_id'] = [
      '#type'  => 'select',
      '#title' => t('Parent Facility Identifier'),
      '#options' => Routines::tableOptions('neighborhood_facility', 1, 5, \TRUE),
      '#required' => FALSE,
      '#empty_value' => 0,
      '#default_value' => 0,
      '#empty_option' => t('None'),
      '#description' => t("An optional container that holds many facilities,
      such as a shopping center or mall, or a government complex."),
    ];
    $form['nf_type'] = [
      '#type'  => 'textfield',
      '#title' => t('Neighborhood Facility Type'),
      '#size'  => 25,
      '#description' => t("The type of facility or facility complex.  Entries 
      could include: 'store', 'shopping center', 'grocery', 'school',
      'government'"),
    ];
    $form['nf_name'] = [
      '#type'  => 'textfield',
      '#title' => t('Neighborhood Facility Name'),
      '#size'  => 60,
      '#description' => t("The neighborhood facility's name."),
    ];
    $form['nf_address'] = [
      '#type'  => 'textfield',
      '#title' => t('Neighborhood Facility Address'),
      '#size'  => 80,
      '#description' => t("The address can be a street address or a location
      within a parent_facility such as a shopping center."),
    ];
    $form['nf_phone_nr'] = [
      '#type'  => 'textfield',
      '#title' => t('Neighborhood Facility Phone number'),
      '#size'  => 20,
      '#description' => t("The Neighborhood facility's phone number."),
    ];
    $form['nf_fax_nr'] = [
      '#type'  => 'textfield',
      '#title' => t('Neighborhood Facility Fax Number'),
      '#size'  => 20,
      '#description' => t("The Neighborhood facility's fax number."),
    ];
    $form['nf_web_site'] = [
      '#type'  => 'textfield',
      '#title' => t('Neighborhood Facility Web Site.'),
      '#size'  => 80,
      '#description' => t("The neighborhood facility's web site.
      ` Example: http://Drupal.org"),
    ];
    $form['nf_map_link'] = [
      '#type'  => 'textfield',
      '#title' => t('Neighborhood Facility Map Link'),
      '#size'  => 80,
      '#maxlength' => 4095,
      '#description' => t("A link to a map website, Google maps or Mapquest, that
      locates the facility. Note: Directions can be copied and pased from
            GOOGLE or MAPLINK direction pages."),
    ];
    $form['nf_comments'] = [
      '#type'  => 'textfield',
      '#title' => t('Neighborhood Facility Comments'),
      '#size'  => 80,
      '#description' => t("Comments on the neighborhood facility."),
    ];

    $form['submit'] = [
      '#type'  => 'submit',
      '#value' => t('AddMe'),
    ];

    $form['cancel'] = [
      '#type'  => 'submit',
      '#value' => t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
    ];

    return $form;
  }

  /**
   * Validates a form for neighborhood_facility (optional).
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (Routines::prependStringTest($form_state->getValue('nf_web_site'),
      ['HTTP://', 'HTTPS://', 'FTP://']) === \FALSE) {
      $form_state->setErrorByName('nf_web_site', t('Please prepend one 
     of the following to the website address:  http://, https://, 
     ftp://'));
    }

  }

  /**
   * Adds neighborhood_facility form data.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');
    $row = [];
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      $row[$key] = $value;
    }

    $tid = $db->startTransaction(); 
    try {
      $db->insert('neighborhood_facility')->fields($row)->execute();
      $message = $this->t('Insert of neighborhood_facility successful');
// Nf_name surpresses message . $row['nf_name'] . \t('--successful.');
      $this->messenger()->addMessage($message);
    }
    
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();

      $this->messenger()->addMessage($this->t('Insert Failed. Nothing done. Error message
        follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>   $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('neighborhood_facility.list');
  }

  /**
   * Cancels neighborhood_facility form addition.
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('neighborhood_facility.list');
  }

}
