<?php

namespace Drupal\neighborhood_facility\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class NeighborhoodFacilityEditForm.
 *
 * @package Drupal\neighborhood_facility\Form\NeighborhoodFacilityEditForm
 *
 * Substitutions:
 * Tblname. Replace with Neighborhood_facility (init cap).
 * tblname.  Replace with neighborhood_facility.
 * Note:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 * @todo. Consider moving BuildingAddForm::table_option routines.
 */
class NeighborhoodFacilityEditForm extends FormBase {

  /**
   * {@inheritdoc}
   */

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'neighborhood_facility_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $neighborhood_facility_id = '') {
    
    $db = Database::getConnection('default', 'default');
    $select = $db->select('neighborhood_facility', 'e')
      ->fields('e')
      ->condition('e.neighborhood_facility_id', $neighborhood_facility_id, "=")
      ->execute();
    $row = $select->fetchAssoc();

    $rowcoarr = Routines::tableOptions('neighborhood_facility', 1, 5, \TRUE);

    $form['neighborhood_facility_id'] = [
      '#type' => 'number',
      '#disabled' => TRUE,
      '#value' => $neighborhood_facility_id,
      '#title' => t('Choose entry to update'),
      '#description' => t("Neighborhood Facility ID"),
      '#ajax' => [
        'wrapper' => 'updateform',
        'callback' => 'neighborhood_facility_form_update_callback',
      ],
    ];
    $form['neighborhood_facility_parent_id'] = [
      '#type'  => 'select',
      '#options' => $rowcoarr,
      '#empty_option' => t('None'),
      '#empty_value' => 0,
      '#size'  => 10,
      '#title' => t('Updated Neighborhood Pacility Parent ID'),
      '#default_value' => $row['neighborhood_facility_parent_id'],
      '#description' => t("An optional container that holds many facilities,
        such as a shopping center or mall, or a government complex."),
    ];

    $form['nf_type'] = [
      '#type'  => 'textfield',
      '#title' => t('Neighborhood Facility Type'),
      '#size'  => 25,
      '#description' => t("The type of facility or facility complex.  Entries 
        could include: 'store', 'shopping center', 'grocery', 'school',
        'government'"),
      '#default_value' => $row['nf_type'],
    ];

    $form['nf_name'] = [
      '#type'  => 'textfield',
      '#title' => t('Neighborhood Facility Name'),
      '#size'  => 60,
      '#description' => t("The neighborhood facility's name."),
      '#default_value' => $row['nf_name'],
    ];

    $form['nf_address'] = [
      '#type'  => 'textfield',
      '#title' => t('Neighborhood Facility Address'),
      '#size'  => 80,
      '#description' => t("The address can be a street address or a location
        within a parent_facility such as a shopping center."),
      '#default_value' => $row['nf_address'],
    ];

    $form['nf_phone_nr'] = [
      '#type'  => 'textfield',
      '#title' => t('Neighborhood Facility Phone number'),
      '#size'  => 20,
      '#description' => t("The Neighborhood facility's phone number."),
      '#default_value' => $row['nf_phone_nr'],
    ];

    $form['nf_fax_nr'] = [
      '#type'  => 'textfield',
      '#title' => t('Neighborhood Facility Fax Number'),
      '#size'  => 20,
      '#description' => t("The Neighborhood facility's fax number."),
      '#default_value' => $row['nf_fax_nr'],
    ];

    $form['nf_web_site'] = [
      '#type'  => 'textfield',
      '#title' => t('Neighborhood Facility Web Site.'),
      '#size'  => 80,
      '#description' => t("The neighborhood facility's web site.
        Example: http://Drupal.org"),
      '#default_value' => $row['nf_web_site'],
    ];

    $form['nf_map_link'] = [
      '#type'  => 'textfield',
      '#title' => t('Neighborhood Facility Map Link'),
      '#size'  => 80,
      '#maxlength' => 4095,
      '#description' => t("A link to a map website, Google maps or Mapquest, that
        locates the facility. Note: Directions can be copied and pased from
            GOOGLE or MAPLINK direction pages."),
      '#default_value' => $row['nf_map_link'],
    ];

    $form['nf_comments'] = [
      '#type'  => 'textfield',
      '#title' => t('Neighborhood Facility Comments'),
      '#size'  => 80,
      '#description' => t("Comments on the neighborhood facility."),
      '#default_value' => $row['nf_comments'],
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => t('Submit'),
    ];

    $form['actions']['cancel'] = [
      '#type'  => 'submit',
      '#value' => t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
    ];
    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (Routines::prependStringTest($form_state->getValue('nf_web_site'),
      ['HTTP://', 'HTTPS://', 'FTP://']) === \FALSE) {
      $form_state->setErrorByName('nf_web_site', t('Please prepend one 
     of the following to the website address:  http://, https://, 
     ftp://'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection();

    $row = [];
    
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      $row[$key] = $value;
    }

    $tid = $db->startTransaction();
    try {
      $nbr_edited = $db->update('neighborhood_facility')->fields($row)
        ->condition('neighborhood_facility_id', $row['neighborhood_facility_id'], '=')
        ->execute();
      
      if ($nbr_edited != 1) {
        $tid->rollBack();
        $this->messenger()->addError(
            $nbr_edited . $this->t(' rows updated, 1 expected. Nothing done.'
                 . ' Neighborhood Facility ID = ')
                 . $row['neighborhood_facility_id'] ); 
      }
      else {
      //$messenger = $this->messenger();
      $this->messenger()->addMessage($this->t('Neighborhood Facility update Successful'));
      }
    }
    
    catch (DatabaseExceptionWrapper $e) {
    $tid->rollBack();
    
    $this->messenger()->addMessage($this->t('Neighborhood Facility update failed. 
        Nothing done. Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>   $e->getTraceAsString(), // $e->getTraceAsString()
      ]), 'error');

    }
    
    $form_state->setRedirect('neighborhood_facility.list');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('neighborhood_facility.list');
  }

}
