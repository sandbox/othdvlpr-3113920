<?php

namespace Drupal\neighborhood_facility\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Database\Database;

/**
 * Class NeighborhoodFacilityListController.
 *
 * @package Drupal\neighborhood_facility\NeighborhoodFacilityListController
 *
 * Substitutions:
 * Tblname. Replace with Neighborhood_facility (init cap).
 * tblname.  Replace with neighborhood_facility.
 * Note:  Apply replacements to namespace and use statements above.
 */
class NeighborhoodFacilityListController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {

    $db = Database::getConnection('default', 'default');

    $null = NULL;
    $rows = [];
    $header = [
    [
      'data' => t('neighborhood.<br>facility_id'),
      'field' => 't.neighborhood_facility_id',
      'sort' => 'asc',
    ],
    [
      'data' => t('neighborhood.<br>facility.<br>parent_id'),
      'field' => 't.neighborhood_facility_parent_id',
    ],
    ['data' => t('nf_type'), 'field' => 't.nf_type'],
    ['data' => t('nf_name'), 'field' => 't.nf_name'],
    ['data' => t('nf_address'), 'field' => 't.nf_address'],
    ['data' => t('nf_phone_nr'), 'field' => 't.nf_phone_nr'],
    ['data' => t('nf_fax_nr'), 'field' => 't.nf_fax_nr'],
    ['data' => t('nf_web_site'), 'field' => 't.nf_web_site'],
    ['data' => t('nf_map_link'), 'field' => 't.nf_map_link'],
    ['data' => t('nf_comments'), 'field' => 't.nf_comments'],

      ['data' => t('Delete'), 'field' => $null],
    ];

    $select = $db->select('neighborhood_facility', 't')
      ->fields('t')
      ->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header)
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit()
      ->execute();

    $parent_facility = [];

    foreach ($select as $row) {

      if (empty($row->neighborhood_facility_parent_id)
        && empty($row->nf_name)
        && empty($row->nf_address)) {
        $facility_name = $row->nf_type;
      }
      else {
        $facility_name = $row->nf_name;
      }
      $parent_facility[$row->neighborhood_facility_id] = $facility_name;
      if (!empty($row->neighborhood_facility_parent_id)) {
        $row->neighborhood_facility_parent_id = $parent_facility[$row->neighborhood_facility_parent_id];
      }
      else {
        $row->neighborhood_facility_parent_id = "";
      }

      if (!empty($row->nf_web_site)) {
        $url = Url::fromUri($row->nf_web_site);
        $row->nf_web_site = Link::fromTextAndUrl(t("website"), $url);
      }

      if (!empty($row->nf_map_link)) {
        $url = Url::fromUri($row->nf_map_link);
        $row->nf_map_link = Link::fromTextAndUrl(t("maplink"), $url);
      }

      $neighborhood_facility_id = (string) $row->neighborhood_facility_id;
      $row->neighborhood_facility_id = Link::fromTextAndUrl($this->t($neighborhood_facility_id),
        Url::fromRoute('neighborhood_facility.edit',
        ['neighborhood_facility_id' => $neighborhood_facility_id]))->toString();

      $row->eddel = Link::fromTextAndUrl('Delete',
        Url::fromRoute('neighborhood_facility.delete',
        ['neighborhood_facility_id' => $neighborhood_facility_id]))->toString();

      $rows[] = ['data' => (array) $row];

    }

    $build[] = [
      '#type' => 'link',
      '#title' => t('Add'),
      '#url' => Url::fromRoute('neighborhood_facility.add'),
    ];

    $build['neighborhood_facility_list_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => "The list is empty.  Please add data",
      '#sticky' => \TRUE,
      '#cache' => [
        'max-age' => 0,
      ],

    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;

  }

}
