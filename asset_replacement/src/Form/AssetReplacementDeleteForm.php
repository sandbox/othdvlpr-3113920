<?php

namespace Drupal\asset_replacement\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\DatabaseExceptionWrapper;
Use Drupal\Core\Url;

/**
 * Class DefaultForm.
 *
 * @package Drupal\asset_replacement\Form\AssetReplacementDeleteForm
 *
 *  * Substitutions:
 * Tblname. Replace with AssetReplacement (init cap).
 * tblname.  Replace with asset_replacement.
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 */
class AssetReplacementDeleteForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'asset_replacement_delete'; 
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return; //  t('delete question test')
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('asset_replacement.list'); // new url('tblname.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  /*
  public function getQuery() {
    return getQuery();
  }
  */
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state,
            $asset_replacement_id = '', $asset_replacement_year = '', $asset_replacement_type = '') {

    // $asset_replacement_year = \Drupal::request()->attributes->get('$asset_replacement_year');
    // $asset_replacement_type = \Drupal::request()->attributes->get('$asset_replacement_type');
    
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Do you wish to delete this asset_replacement below? '),
      '#value' =>  $this->t("$asset_replacement_id"  . '/' . "$asset_replacement_year"
              . '/' . "$asset_replacement_type"),
      '#size' => 60,
      '#maxlength' => 128,
    ];
    
    return parent::buildForm($form, $form_state, $asset_replacement_id = '', $asset_replacement_year = '', $asset_replacement_type = '');

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $get_values = (string) \implode(',', $form_state->getValues());
    $this->messenger()->addMessage('submitForm $get_values=' . $get_values);

    $db = Database::getConnection('default', 'default');

    $asset_replacement_id = (integer) \Drupal::request()->attributes->get('asset_replacement_id');
    $asset_replacement_year = (integer) \Drupal::request()->attributes->get('asset_replacement_year');
    $asset_replacement_type = (string) \Drupal::request()->attributes->get('asset_replacement_type');

    $tid = $db->startTransaction();
    try {
      $nbr_deleted = $db->delete('asset_replacement')
        ->condition('asset_replacement_id', $asset_replacement_id, '=')
        ->condition('asset_replacement_year', $asset_replacement_year, '=')
        ->condition('asset_replacement_type', $asset_replacement_type, '=')
        // ->condition('nocol', 'xyz' , '=')
        ->execute();

      if ($nbr_deleted != 1) {
        $tid->rollBack();
       $this->messenger()->addError(
           $nbr_deleted .     $this->t(' rows deleted, 1 expected. Nothing done.'));
     }
      else {
      $this->messenger()->addMessage($this->t('AssetReplacement deleted.'));
     }
    }
    
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      
      $this->messenger()->addMessage($this->t('Delete Failed. Nothing done. Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('asset_replacement.list');
  }

}
