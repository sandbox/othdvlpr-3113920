<?php

namespace Drupal\asset_replacement\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class AssetReplacementAddForm.
 *
 * @package Drupal\asset_replacement\Form\AssetReplacementAddForm
 *
 * Substitutions:
 * Tblname. Replace with AssetReplacement (init cap).
 * tblname. Replace with asset_replacement.
 * col02.   Replace with column name (i.e. tblname_id).
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 */
class AssetReplacementAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'asset_replacement_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $asset_replacement_id = '') {

    $form['add'] = [
      '#type'  => 'fieldset',
      '#prefix' => '<div id="formadd" style="background-color:Cornsilk"',
      '#suffix' => '</div>',
    ];
    $form['add']['asset_replacement_id'] = [
    '#type'  => 'select',
    '#options' => Routines::tableOptions('asset',1,4,NULL,$form_state),
    '#title' => \t('asset_replacement_id'),
    '#description' => \t("Unique asset_replacement ID, same as asset."),
    '#prefix' => '<table border=0 cellspacing=3 celladding=3><tr><td>',
    '#suffix' => '</td>',
    ];
    $this_year = \date("Y");
    $form['add']['asset_replacement_year'] = [
      '#type'  => 'number',
      '#title' => \t('asset_replacement_year'),
      '#size'  => 5,
      '#default_value' => $this_year,
      '#description' => \t("The latest four-digit year in which the asset was"
            . "replaced."),
      '#prefix' => '<table<<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['asset_replacement_type'] = [
      '#type'  => 'select',
      '#title' => \t('Asset Replacement Type'),
      '#options' => [
          'I' => \t('Initial Construction'),
          'R' => \t('Replacement'),
          'E' => \t('Life Extension Program'),
      ],
      '#default_value'  => \t('R'),
      '#description' => \t('Type of action taken.  Values:
        I - Initial Construction
        R - Replacement
        E - Life Extension Program'),
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['asset_replacement_number'] = [
      '#type'  => 'number',
      '#title' => \t('Asset Replacement Number'),
      '#size'  => 6,
      '#default_value' => 1,
      '#description' => \t("The number of assets replaced."),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['asset_orig_est_useful_life_yrs'] = [
      '#type'  => 'number',
      '#title' => \t('Asset Original Estimated Useful Life in Years'),
      '#size'  => 5,
      '#default_value' => 5,
      '#description' => \t("The number of years of estimated useful life of a"
        . "newly-replaced asset."),
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    ];
    $form['add']['asset_est_remaining_yrs'] = [
      '#type'  => 'number',
      '#title' => \t('Asset Estimated Remainng Years of Life'),
      '#size'  => 5,
      '#default_value' => 1,
      '#description' => \t("The number of years of useful life remaining to the"
              . " asset. The default estimate is the original estimated useful life"
              . " minus the number of years since its replacement. "),
      '#prefix' => '<td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['asset_replacement_unit_cost'] = [
      '#type'  => 'number',
      '#title' => \t('Asset Replacement Unit Cost'),
      '#size'  => 10,
      '#default_value' => 0,
      '#description' => \t("The unit cost of the replacement."),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];
    $form['add']['asset_replacement_est_future_unit_cost'] = [
      '#type'  => 'number',
      '#title' => \t('Asset Replacement Estimated Future Unit Cost'),
      '#size'  => 10,
      '#default_value' => 0,
      '#description' => \t("The estimated cost of a future replacement."
              . "  This estimate can be drawn from a reserve study or other"
              . " projection. "),
      '#prefix' => '<td>',
      '#suffix' => '</td>',
    ];
    $form['add']['asset_warranty_end_date'] = [
      '#type'  => 'date',
      '#title' => \t('Asset Warranty End Date'),
      '#required' => FALSE,
      '#description' => \t("The date on which an asset's warranty ends. "),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td></tr>',
    ];
    $form['add']['submit'] = [
      '#type'  => 'submit',
      '#value' => \t('Add'),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',];

    $form['add']['cancel'] = [
      '#type'  => 'submit',
      '#value' => \t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
      '#prefix' => '<td>',
      '#suffix' => '</td></tr></table>',
    ];

    return $form;
  }

  /**
   * Validates a form for asset_replacement (optional).
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  
  
if (Routines::triUniqueColumnsValuesTest('asset_replacement', 'asset_replacement_id',
        $form_state->getValue('asset_replacement_id'), 'asset_replacement_year',
        $form_state->getValue('asset_replacement_year'), 'asset_replacement_type',
        $form_state->getValue('asset_replacement_type'))!= 0 ) {
        $form_state->setErrorByName('Unique asset_replacement ID/year/type',
          \t('Please enter a unique value for Asset Replacement'));
        }
}
  /**
   * Adds asset_replacement form data.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');
    
    $row = [];
    
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      
      if ($key === 'asset_warranty_end_date') {
        $value = empty($value)  ? NULL : trim(strip_tags($value));
      }
     
      $value = (!is_numeric($value) && $form['add'][$key]['#type'] == 'number') ? 0 : $value;
      
      $row[$key] = $value;
    }

    $tid = $db->startTransaction();
    try {
      $db->insert('asset_replacement')->fields($row)->execute();
      
      
      $message = $this->t('Insert of AssetReplacement [')
              . $row['asset_replacement_id']   . \t('/') 
              . $row['asset_replacement_year'] . \t('/')
              . $row['asset_replacement_type'] . \t('] successful');
      $this->messenger()->addMessage($message);
      
    }
    
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $this->messenger()->addMessage(\t('Insert Failed. Nothing done. Error message
        follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');

      

    }

    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      $this->messenger()->addMessage($key . $this->t('=') . $value);
    }

    $form_state->setRedirect('asset_replacement.list');
  }

  /**
   * Cancels asset_replacement form addition.
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('asset_replacement.list');
  }

}
