<?php

namespace Drupal\asset_replacement\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;
 
/**
 * Class AssetReplacementEditForm.
 *
 * @package Drupal\asset_replacement\Form\AssetReplacementEditForm
 *
 * Substitutions:
 * Tblname. Replace with AssetReplacement (init cap).
 * tblname.  Replace with asset_replacement.
 * Note:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 * @todo. Consider moving BuildingAddForm::table_option routines.
 */
class AssetReplacementEditForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'asset_replacement_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $asset_replacement_id = '', $asset_replacement_year = '',
          $asset_replacement_type = '') {
   // $this->tblname_id = \Drupal::request()->attributes->get('tblname_id');
   // $tblname_id = (integer) $this->tblname_id;
    
    $db = Database::getConnection('default', 'default');
    
    $select = $db->select('asset_replacement', 't')
      ->fields('t')
      ->condition('t.asset_replacement_id', $asset_replacement_id, '=')
      ->condition('t.asset_replacement_year', $asset_replacement_year, '=')
      ->condition('t.asset_replacement_type', $asset_replacement_type, '=')
      ->execute();
    
    $row = $select->fetchAssoc();
    
    $form['asset_replacement_id'] = [
      '#type' => 'number',
      '#disabled' => TRUE,
      '#title' => \t('Asset Replacement ID'),
      '#value' => $row['asset_replacement_id'],
      '#description' => \t("Unique asset replacement ID, same as asset."),
    ];

    $form['asset_replacement_year'] = [
      '#type' => 'number',
      '#title' => \t('Updated Asset Replacement Year'),
      '#size' => 6,
      '#default_value' => $row['asset_replacement_year'],
      '#description' => \t("The latest four-digit year in which the asset was"
            . "replaced."),
    ];

    $form['asset_replacement_type'] = [
      '#type' => 'select',
      '#title' => \t('Asset Replacement Type'),
      '#default_value' => $row['asset_replacement_type'],
      '#description' => \t("Type of action taken."),
      '#options' => [
          'I' => \t('Initial Construction'),
          'R' => \t('Replacement'),
          'E' => \t('Life Extension Program'),
        ]   
    ];
    
    // adds read-only asset title to the form.
    $result = [];
    $query = $db->select('asset', 'n')
      ->fields('n', ['asset_title'])
      ->condition('n.asset_id', $asset_replacement_id, '=' )
      ->execute();
    $result = $query->fetchAssoc();
    $form['asset_title'] = [
      '#type'  => 'textfield',
      '#disabled' => TRUE,
      '#title' => \t('Asset Title'),
      '#size'  => 35,
      '#description' => \t("A formal name given to the asset, such as 
        by its community documents."),
      '#default_value' => $result['asset_title'],
    ];
    
    
    $form['asset_replacement_number'] = [
      '#type' => 'number',
      '#title' => \t('Asset Replacement Number'),
      '#size' => 6,
      '#default_value' => $row['asset_replacement_number'],
      '#description' => \t("The number of assets replaced."),
    ];

    $form['asset_orig_est_useful_life_yrs'] = [
      '#type' => 'number',
      '#title' => \t('Asset Original Estimated Useful Life in Years'),
      '#size' => 5,
      '#default_value' => $row['asset_orig_est_useful_life_yrs'],
      '#description' => \t("The number of years of estimated useful life of a"
        . "newly-replaced asset."),
    ];

    $form['asset_est_remaining_yrs'] = [
      '#type' => 'number',
      '#title' => \t('Asset Estimated Remainng Years of Life'),
      '#size' => 5,
      '#default_value' => $row['asset_est_remaining_yrs'],
      '#description' => \t("The number of years of useful life remaining to the"
              . " asset. The default estimate is the original estimated useful life"
              . " minus the number of years since its replacement. "),
    ];

    $form['asset_replacement_unit_cost'] = [
      '#type' => 'number',
      '#title' => \t('Asset Replacement Unit Cost'),
      '#size' => 10,
      '#default_value' => $row['asset_replacement_unit_cost'],
      '#description' => \t("The unit cost of the replacement."),
    ];

    $form['asset_replacement_est_future_unit_cost'] = [
      '#type' => 'number',
      '#title' => \t('Asset Replacement Estimated Future Unit Cost'),
      '#size' => 10,
      '#default_value' => $row['asset_replacement_est_future_unit_cost'],
      '#description' => \t("The estimated cost of a future replacement."
              . "  This estimate can be drawn from a reserve study or other"
              . " projection. "),
    ];

    $form['asset_warranty_end_date'] = [
      '#type' => 'date',
      '#title' => \t('Asset Warranty End Date'),
      '#required' => FALSE,
      '#default_value' => $row['asset_warranty_end_date'],
      '#description' => \t("The date on which an asset's warranty ends. "),
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => t('Submit'),
    // C '#prefix' => '<tr><td>',.
    // C '#suffix' => '</td></tr></table>',.
    ];

    $form['actions']['cancel'] = [
      '#type'  => 'submit',
      '#value' => t('Cancel'),
      '#limit_validation_errors' => [],
      '#submit' => ['::cancelForm'],
    // C '#prefix' => '<tr><td>',.
    // C '#suffix' => '</td></tr></table>',.
    ];
    // C return parent::buildForm($form, $form_state);.
    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /*
     * An Alternate way of inserting/updating table fields:
     *
     * @code
     * parent::submitForm($form, $form_state);
     * $this->config('tblname')
     * ->set('tblname_id', $form_state->getValue('tblname_id'))
     * ->set('tblname_type_code', $form_state->getValue('tblname_type_code'))
     * ->set('parent_complex_id', $form_state->getValue('parent_complex_id'))
     * ->set('tblname_name', $form_state->getValue('tblname_name'))
     * ->set('tblname_street_address',
     *    $form_state->getValue('tblname_street_address'))
     * ->save();
     * @endcode
     */

    $db = Database::getConnection();

    // C $rowobj = new StdClass();.
    $row['asset_replacement_id'] = $form_state->getValue('asset_replacement_id');
    $row['asset_replacement_year'] = $form_state->getValue('asset_replacement_year');
    $row['asset_replacement_type'] = $form_state->getValue('asset_replacement_type');
    $row['asset_replacement_number'] = $form_state->getValue('asset_replacement_number');
    $row['asset_orig_est_useful_life_yrs'] = $form_state->getValue('asset_orig_est_useful_life_yrs');
    $row['asset_est_remaining_yrs'] = $form_state->getValue('asset_est_remaining_yrs');
    $row['asset_replacement_unit_cost'] = $form_state->getValue('asset_replacement_unit_cost');
    $row['asset_replacement_est_future_unit_cost'] = $form_state->getValue('asset_replacement_est_future_unit_cost');
    $row['asset_warranty_end_date'] = $form_state->getValue('asset_warranty_end_date');
    
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
        
     if ($key === 'asset_warranty_end_date') {
        $value = empty($value) ? NULL : trim(strip_tags($value));
      }   
        
        
      $value =  (!is_numeric($value) && $form[$key]['#type'] === 'number')? 0 : $value;
      
      $row[$key] = $value;  // = 0
    }
    
    // $row['unit_id'] = 0;
    $tid = $db->startTransaction();
    try {
      unset ($row["asset_title"]);  // required for read-only column
      $nbr_edited = $db->update('asset_replacement')->fields($row)
        ->condition('asset_replacement_id', $row['asset_replacement_id'], '=')
        ->condition('asset_replacement_year', $row['asset_replacement_year'], '=')
        ->condition('asset_replacement_type', $row['asset_replacement_type'], '=')
        ->execute();
      
      if ($nbr_edited != 1) {
        $tid->rollBack();
        $this->messenger()->addError(
            $nbr_edited . $this->t(' rows updated, 1 expected. Nothing done. AssetReplacement ID = ')
                 . $row['asset_replacement_id'] ); 
      }
      else {

      $message = $this->t('Update of AssetReplacement [')
              . $row['asset_replacement_id']   . \t('/') 
              . $row['asset_replacement_year'] . \t('/')
              . $row['asset_replacement_type'] . \t('] successful');
      $this->messenger()->addMessage($message);
      }
    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $this->messenger()->addMessage($this->t('AssetReplacement update failed. Nothing done. 
        Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(), // $e->getTraceAsString()
      ]), 'error');
    }

    $form_state->setRedirect('asset_replacement.list');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('asset_replacement.list');
  }

}
