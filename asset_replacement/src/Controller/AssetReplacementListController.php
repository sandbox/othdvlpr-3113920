<?php

namespace Drupal\asset_replacement\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;

/**
 * Class AssetReplacementEditForm.
 *
 * @package Drupal\asset_replacement\AssetReplacementListController
 *
 * Substitutions:
 * AssetReplacement. Replace with AssetReplacement (init cap).
 * asset_replacement.  Replace with asset_replacement.
 * Notes:  Apply replacements to namespace and use statements above.
 *         Class names. Replace underscore and following letter with capital
 *         letter.
 */
class AssetReplacementListController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
   // $this->messenger()->addMessage(\t('AssetReplacementListController  ckpt1'));

    $db = Database::getConnection('default', 'default');

    $null = NULL;
    $assetheadingtitle = 'Asset Title';
    
    $rows = [];
    $select = $db->select('asset_replacement','e');
    $select->addField('e', 'asset_replacement_id');
    $select->addField('e', 'asset_replacement_year');
    $select->addField('e', 'asset_replacement_type');
    $select->addField('a', 'asset_title');
    $select->addField('e', 'asset_replacement_number');
    $select->addField('e', 'asset_orig_est_useful_life_yrs');
    $select->addField('e', 'asset_est_remaining_yrs');
    $select->addField('e', 'asset_replacement_unit_cost');
    $select->addField('e', 'asset_replacement_est_future_unit_cost');
    $select->addField('e', 'asset_warranty_end_date');
    
    $select->addJoin('INNER', 'asset', 'a', 'e.asset_replacement_id = a.asset_id');
    $select->addField('a','asset_title');
    $header = [
      ['data' => \t('edit'), 'field' => $null],
      [
        'data' => \t('Asset ID'),
        'field' => 'e.asset_replacement_id',
        'sort' => 'asc',
      ],
      ['data' => \t('Asset Replacement Year'), 'field' => 'e.asset_replacement_year'],
      ['data' => \t('Asset Replacement Type'), 'field' => 'e.asset_replacement_type'],
      ['data' => \t('Asset_Title'), 'field' => 'a.asset_title'],
      ['data' => \t('Asset Replacement Number'), 'field' => 'e.asset_replacement_number'],
      ['data' => \t('Asset Replacement Original_Estimated Useful_Life Yrs'), 'field' => 'e.asset_orig_est_useful_life_yrs'],
      ['data' => \t('Asset Replacement Estimated Remaining Yrs'), 'field' => 'e.asset_est_remaining_yrs'],
      ['data' => \t('Asset Replacement Unit_Cost'), 'field' => 'e.asset_replacement_unit_cost'],
      ['data' => \t('Asset Replacement Estimated_Future Unit_Cost'), 'field' => 'e.asset_replacement_est_future_unit_cost'],
      ['data' => \t('Asset Replacement Warranty End_Date'), 'field' => 'e.asset_warranty_end_date'],
      ['data' => \t('Delete'), 'field' => $null],
    ];

    $entries = $select // $db->select('asset_replacement', 'e')
      ->fields('e')
      ->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header)
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit()
      ->execute();

   // $this->messenger()->addMessage(\t('asset_replacementListController  ckpt2'));

    $row = [];
    foreach ($entries as $rowobj) {

      $asset_replacement_id = (string) $rowobj->asset_replacement_id;
      $asset_replacement_year = (string) $rowobj->asset_replacement_year;
      $asset_replacement_type = (string) $rowobj->asset_replacement_type;
      
      $rowobj->editrow = Link::fromTextAndUrl('edit', Url::fromRoute('asset_replacement.edit',
                [
                  'asset_replacement_id' => $asset_replacement_id,
                  'asset_replacement_year' => $asset_replacement_year,
                  'asset_replacement_type' => $asset_replacement_type,
                ]))->toString();
      // kint('$formlink', $formlink);
      $row[0] = $rowobj->editrow;
      $row[1] = $rowobj->asset_replacement_id;
      $row[2] = $rowobj->asset_replacement_year;
      $row[3] = $rowobj->asset_replacement_type;
      $row[4] = $rowobj->asset_title;
      $row[5] = $rowobj->asset_replacement_number;
      $row[6] = $rowobj->asset_orig_est_useful_life_yrs;
      $row[7] = $rowobj->asset_est_remaining_yrs;
      $row[8] = $rowobj->asset_replacement_unit_cost;        
      $row[9] = $rowobj-> asset_replacement_est_future_unit_cost;
      $row[10] = $rowobj->asset_warranty_end_date;
      $row[11] = Link::fromTextAndUrl('Delete', Url::fromRoute('asset_replacement.delete', ['asset_replacement_id' => $asset_replacement_id, 'asset_replacement_year' => $asset_replacement_year, 'asset_replacement_type' => $asset_replacement_type,]))->toString();
      
      $rows[] = ['data' => (array) $row];

    }

    $build[] = [
      '#type' => 'link',
      '#title' => \t('Add'),
      '#url' => Url::fromRoute('asset_replacement.add'),
    ];

    $build['asset_replacement_list_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => "The list is empty.  Please add data",
      '#sticky' => \TRUE,
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];
    
    $build['#cache']['max-age'] = 0;

    return $build;

  }

}
