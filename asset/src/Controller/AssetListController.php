<?php

namespace Drupal\asset\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;

/**
 * Class assetEditForm.
 *
 * @package Drupal\asset\assetListController
 *
 * Substitutions:
 * Tblname. Replace with asset (init cap).
 * tblname.  Replace with asset.
 * Note:  Apply replacements to namespace and use statements above.
 * 
 */
class AssetListController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {

    $db = Database::getConnection('default', 'default');

    $null = NULL;
    $rows = [];
    $header = [
      [
        'data' => t('Asset ID'),
        'field' => 'e.asset_id',
        'sort' => 'asc',
      ],
      ['data' => t('Asset Parent Assembly ID'), 'field' => 'e.asset_parent_assembly_id'],
      // C == '0' ? '' : 'e.asset_attr2'),.
      ['data' => t('Asset Class'), 'field' => 'e.asset_class'],
      ['data' => t('Asset Title'), 'field' => 'e.asset_title'],
      ['data' => t('Asset Location'), 'field' => 'e.asset_location'],
      ['data' => t('Asset Maint/Insp Schedule'), 'field' => 'e.asset_mntnc_inspctn_schedule'],
      ['data' => t('Asset Cost'), 'field' => 'e.asset_cost'],
      ['data' => t('Asset Nr of Units'), 'field' => 'e.asset_nr_of_units'],
      ['data' => t('Asset Unit Measure'), 'field' => 'e.asset_unit_measure'],
      ['data' => t('Asset Documen- tation ID'), 'field' => 'e.asset_documentation_id'],
      ['data' => t('Asset Remarks'), 'field' => 'e.asset_remarks'],
      // C 'field' => 'e.tblname_id'),.
      ['data' => t('Delete'), 'field' => $null],
    ];

    $select = $db->select('asset', 'e')
      ->fields('e')
      ->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header)
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit()
      // C ->range(0, 10).
      ->execute();
    
    $asset_mntnc_inspctn_schdl_desc = ['N/A' => 'Not Applicable','D' => 'Daily',
       'W' => 'Weekly', 'B' => 'Bi-weekly', 'M' => 'Monthly', 'S' => 'Bi-Montly',
       'O' => 'Semi-annually', 'A' => 'Annually', 'R' => 'As required'];

    foreach ($select as $row) {

      $asset_id = (string) $row->asset_id;

      if (empty($row->asset_parent_assembly_id)) {
        $row->asset_parent_assembly_id = \NULL;
      }

      $asset_documentation_id = (string) $row->asset_documentation_id;
      
      $row->asset_mntnc_inspctn_schedule = 
          $asset_mntnc_inspctn_schdl_desc[$row->asset_mntnc_inspctn_schedule];

      $row->asset_id = Link::fromTextAndUrl($this->t($asset_id),
          Url::fromRoute('asset.edit',
          [
            'asset_id' => $asset_id,
            'asset_docuumentation_id' => $asset_documentation_id,
          ]))->toString();
      $row->asset_documentation_id =
                Routines::displayManagedFile($row->asset_documentation_id);
      // $message = \t('asset_id =') . $row->asset_id;
      // \Drupal::messenger()->addMessage($message);
      $row->eddel = Link::fromTextAndUrl('Delete',
                Url::fromRoute('asset.delete',
                    [
                      'asset_id' => $asset_id,
                      'asset_documentation_id' => $asset_documentation_id,
                    ]))->toString();

      $rows[] = ['data' => (array) $row];

    }

    $build[] = [
      '#type' => 'link',
      '#title' => t('Add'),
      '#url' => Url::fromRoute('asset.add'),
    ];

    $build['asset_list_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => "The list is empty.  Please add data",
      '#sticky' => \TRUE,
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;

  }

}
