<?php

namespace Drupal\asset\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class AssetEditForm.
 *
 * @package Drupal\asset\Form\AssetAddForm
 *
 * Substitutions:
 * Tblname. Replace with Asset (init cap).
 * tblname.  Replace with asset.
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 * 
 */
class AssetAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'asset_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['add'] = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Add a asset entry'),
      // '#prefix' => '<div id="formadd" style="background-color:Cornsilk"',
      // '#suffix' => '</div>',
    ];

    $rowcoobj = Routines::tableOptions('asset', 1, 5, FALSE);
    $rowcoarr = (array) $rowcoobj;
    $form['add']['asset_parent_assembly_id'] = [
      '#type'  => 'select',
      '#title' => $this->t('Asset Parent Assembly Identifier'),
      '#required' => FALSE,
    // Need to add self-referencing table,.
      '#options' => $rowcoarr,
      '#empty_value' => 0,
      '#default_value' => 0,
      '#empty_option' => $this->t('None'),
      '#description' => $this->t("The identifier of an asset containing this asset,
        or of which it is a component."),
      // '#prefix' => '<table border=0 cellspacing=3 celladding=3><tr><td>',
      // '#suffix' => '</td>',
    ];
    $form['add']['asset_class'] = [
      '#type'  => 'textfield',
      '#title' => $this->t('Asset Class'),
      '#size'  => 35,
      '#description' => t("The category under which the asset is subsumed. 
        Examples:  Buildings, Pavement and  Parking Lots, Sidewalks, 
        Patios, Balconies, Walls, Fencing, Lighting, Signs, Recreational, 
        Electrical,Office Space, Heating and Cooling,  Piping and 
        Plumbing, Vehicles, Gates, Grounds."),
      // '#prefix' => '<td>',
      // '#suffix' => '</td></tr>',
    ];
    $form['add']['asset_title'] = [
      '#type'  => 'textfield',
      '#title' => $this->t('Asset Title'),
      '#size'  => 35,
      '#description' => $this->t("A formal name given to the asset, such as 
        by its community documents."),
      // '#prefix' => '<tr><td>',
      // '#suffix' => '</td></tr>',
    ];
    $form['add']['asset_location'] = [
      '#type'  => 'textfield',
      '#title' => $this->t('Asset Location'),
      '#size'  => 60,
      '#maxlength' => 2000,
      '#description' => $this->t('A location description, latitudinal and longitudinal coordinates,
            or a map-producing HTML insert.'),
      // '#prefix' => '<tr><td>',
      // '#suffix' => '</td></tr>',
    ];
    $form['add']['asset_mntnc_inspctn_schedule'] = [
        '#type' => 'select',
        '#title' => t('Asset Maintenance/Inspection Schedule.'),
        '#description' => t('The frequency with which an asset is inspected.
           and/or maintained. values: 
            Not Applicable, Daily, Weekly, Bi-weekly, Monthly, Bi-monthly, 
            Semi-annually, Annually, As required.'),
        '#size'  => 1,
        '#default_value' => 'R',
        '#options' => [
          'R' => t('As required'),
          'D' => t('Daily'),
          'W' => t('Weekly'),
          'B' => t('Bi-weekly'),
          'M' => t('Monthly'),
          'O' => t('Bi-monthly'),
          'S' => t('Semi-annually'),
          'A' => t('Annually'),
          'N/A' => t('Not Applicable'),
      ]
    ];
    $form['add']['asset_cost'] = [
      '#type'  => 'number',
      '#title' => $this->t('Asset Cost'),
      '#size'  => 8,
      '#default_value' => '0',  
      '#description' => t("The monetary replacement cost of an asset. 
        The source for this information could be the latest replacement/reserve 
        study."),
      // '#prefix' => '<tr><td>',
      // '#suffix' => '</td></tr>',
    ];
    $form['add']['asset_nr_of_units'] = [
      '#type'  => 'number',
      '#title' => $this->t('Asset Number of Units'),
      '#size'  => 5,
      '#default_value' => '0',
      '#description' => $this->t("The number of this asset on the property.
        This number is usually given in a replacement/reserve study."),
      // '#prefix' => '<tr><td>',
      // '#suffix' => '</td>',
    ];
    $form['add']['asset_unit_measure'] = [
      '#type'  => 'select',
      '#title' => $this->t('Asset Unit of Measure'),
      '#size'  => 1,
      '#description' => t("Measurement units describing this asset
        or the units by which it is measured. Values can include:
        EA - Each, LF - Linear Feet, SF - Square Feet, CF - Cubic Feet,
        LY - Linear Yards, SY - Square Yards, CY - Cubic Yards,
        LM - Linear Meters, SM - Sqare Measures, CM - Cubic Meters."),
      '#options' => [
        'EA' => $this->t('EA - Each'),
        'LF' => $this->t('LF - Linear Feet'),
        'SF' => $this->t('SF - Square Feet'),
        'CF' => $this->t('CF - Cubic Feet'),
        'LY' => $this->t('LY - Linear Yards'),
        'SY' => $this->t('SY - Square Yards'),
        'CY' => $this->t('CY - Cubic Yards'),
        'LM' => $this->t('LM - Linear Meters'),
        'SM' => $this->t('SM - Sqare Measures'),
        'CM' => $this->t('CM - Cubic Meters'),
      ],
      // '#prefix' => '<td>',
      // '#suffix' => '</td></tr>',
    ];
    $form['add']['asset_documentation_id'] = [
      '#type'  => 'managed_file',
      '#title' => $this->t('Asset Documentation Identifier'),
      '#description' => t("The identifier of a file ,stored online, 
        containing an asset's description, specification or location. 
        Such a file could be a PDF, text or ZIP file containing multiple document,
        or a list of hardcopy document locations.  "),
      // '#empty_value' => NULL, //zeros appear 2 have no effect
      '#default_value' => NULL,
      '#multiple' => \FALSE,
      '#tree' => \TRUE,
      '#upload_validators' => [
        'FileExtension' => ['extensions' => 'txt wpd odt ods rtf pdf doc zip jzip gz'],
      ],
      '#upload_location' => 'public://',
      // '#prefix' => '<tr><td>',
      // '#suffix' => '</td></tr>',
    ];
    $form['add']['asset_remarks'] = [
      '#type'  => 'textarea',
      '#title' => $this->t('Asset Remarks'),
      '#cols'  => 60,
      '#description' => $this->t("Miscellaneous asset comments."),
      // '#prefix' => '<tr><td>',
      // '#suffix' => '</td></tr>',
    ];

    $form['add']['submit'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Add'),
        // '#prefix' => '<tr><td>',
        // '#suffix' => '</td></tr></table>',

    ];

    $form['add']['cancel'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Cancel'),
      '#submit' => ['::cancelForm'],
      // '#prefix' => '<td>',
      // '#suffix' => '</td></tr></table>',
    ];

    return $form;
  }

  /**
   * Validates a form for asset (optional).
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Adds asset form data.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');
    $row = [];
    
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
    
      if (\gettype($value) === 'array') {
        if (empty(\current($value))) {
          $value = 0;
        }
        else {
          $value = \current($value);
        }
      }
      else {
      }

      $row[$key] = $value;
    }
    
    $tid = $db->startTransaction();
    try {
      $db->insert('asset')->fields($row)->execute();

      $message = $this->t('Insert of asset [') . $row['asset_title'] . $this->t('] successful');
      $this->messenger()->addMessage($message);
      // );
      if (!empty($row['asset_documentation_id'])) {
        Routines::addManagedFile($row['asset_documentation_id'], 'asset');
      }

    }
    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $messenger = $this->messenger();
      $messenger->addMessage(\t('Add Failed. Nothing done. Error message
        follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
    }

    /*
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {

    $message = $key . '=' . $value;
    \Drupal::messenger()->addMessage($message);
    }
     */
    $form_state->setRedirect('asset.list');
  }

  /**
   * Cancels asset form addition.
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('asset.list');
  }

}
