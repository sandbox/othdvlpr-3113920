<?php

namespace Drupal\asset\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class AssetEditForm.
 *
 * @package Drupal\asset\Form\AssetEditForm
 *
 * Substitutions:
 * Tblname. Replace with Asset (init cap).
 * tblname.  Replace with asset.
 * Note:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 * 
 */
class AssetEditForm extends FormBase {

  /**
   * {@inheritdoc}
   */

  /**
   * Construct temporary storage gsvyptufor original greeting_pic.
   *
   * @var typetempStoreFactory
   *  Temporary store factory
   */
  private $tempStoreFactory;

  /**
   * Construct temporary storage for original asset_documentation_id.
   *
   * @var typestringstore
   *  Store name
   */
  protected $store;

  /**
   * Construct temporary store object.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $tempStoreFactory
   *   Temporary store factory.
   */
  public function __construct(
        PrivateTempStoreFactory $tempStoreFactory
    ) {
    $this->tempStoreFactory = $tempStoreFactory;
    $collection = 'collection_name';
    $this->store = $this->tempStoreFactory->get($collection);
  }

  /**
   * Create private tempstore for greeting_pic value.
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('tempstore.private')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'asset_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state,
          $asset_id = '', $asset_documentation_id = '') {

    $db = Database::getConnection('default', 'default');
    $select = $db->select('asset', 'e')
      ->fields('e')
      ->condition('e.asset_id', $asset_id, '=')
      ->execute();
    $row = $select->fetchAssoc();

    $form['asset_id'] = [
      '#type' => 'number',
      '#disabled' => TRUE,
      '#options' => $asset_id,
      '#title' => t('Choose an asset to update'),
    // $default_entry->asset_id,
      '#default_value' => $row['asset_id'],
      '#description' => $this->t("The unique Asset Identifier."),
      '#ajax' => [
        'wrapper' => 'updateform',
        'callback' => 'asset_form_update_callback',
      ],
    ];
    
    $rowcoobj = Routines::tableOptions('asset', 1, 5, TRUE);
    $rowcoarr = (array) $rowcoobj;
    $form['asset_parent_assembly_id'] = [
      '#type'  => 'select',
      '#title' => t('Asset Parent Assembly Identifier'),
      '#description' => $this->t("The numeric identifier of an asset containing this asset,
      or of which it is a component."),
      '#options' => $rowcoarr,
      '#empty_value' => 0,
      '#empty_option' => t('None'),
      '#default_value' => $row['asset_parent_assembly_id'],
    ];

    $form['asset_class'] = [
      '#type'  => 'textfield',
      '#title' => $this->t('Asset Class'),
      '#size'  => 35,
      '#description' => t("The category under which the asset is subsumed. 
        Examples:  Buildings, Pavement and  Parking Lots, Sidewalks, 
        Patios, Balconies, Walls, Fencing, Lighting, Signs, Recreational, 
        Electrical,Office Space, Heating and Cooling,  Piping and 
        Plumbing, Vehicles, Gates, Grounds."),
      '#default_value' => $row['asset_class'],
    ];

    $form['asset_title'] = [
      '#type'  => 'textfield',
      '#title' => $this->t('Asset Title'),
      '#size'  => 35,
      '#description' => $this->t("A formal name given to the asset, such as 
        by its community documents."),
      '#default_value' => $row['asset_title'],
    ];
    $form['asset_mntnc_inspctn_schedule'] = [
        '#type' => 'select',
        '#title' => t('Asset Maintenance/Inspection Schedule.'),
        '#description' => t('The frequency with which an asset is inspected.
           and/or maintained. values: 
            Not Applicable, Daily, Weekly, Bi-weekly, Monthly, Bi-monthly, 
            Semi-annually, Annually, As required.'),
        '#size'  => 1,
        '#default_value' => 'R',
        '#options' => [
          'R' => t('As required'),
          'D' => t('Daily'),
          'W' => t('Weekly'),
          'B' => t('Bi-weekly'),
          'M' => t('Monthly'),
          'O' => t('Bi-monthly'),
          'S' => t('Semi-annually'),
          'A' => t('Annually'),
          'N/A' => t('Not Applicable'),
      ]
    ];

    $form['asset_location'] = [
      '#type'  => 'textfield',
      '#title' => $this->t('Asset Location'),
      '#size'  => 60,
      '#maxlength' => 2000,
      '#description' => t("A location description, latitudinal and longitudinal coordinates,
        or a map-producing HTML insert."),
      '#default_value' => $row['asset_location'],
    ];

    $form['asset_cost'] = [
      '#type'  => 'number',
      '#title' => $this->t('Asset Cost'),
      '#size'  => 8,
      '#description' => $this->t("The monetary replacement cost of an asset. 
        The source for this information could be the latest replacement/reserve 
        study."),
      '#default_value' => $row['asset_cost'],
    ];

    $form['asset_nr_of_units'] = [
      '#type'  => 'number',
      '#title' => $this->t('Asset Number of Units'),
      '#size'  => 5,
      '#description' => $this->t("The number of this asset on the property.
        This number is usually given in a replacement/reserve study."),
      '#default_value' => $row['asset_nr_of_units'],
    ];

    $form['asset_unit_measure'] = [
      '#type'  => 'select',
      '#title' => $this->t('Asset Unit of Measure'),
      '#size'  => 2,
      '#description' => $this->t("Measurement units describing this asset
        or the units by which it is measured. Values can include:
        EA - Each, LF - Linear Feet, SF - Square Feet, CF - Cubic Feet,
        LY - Linear Yards, SY - Square Yards, CY - Cubic Yards,
        LM - Linear Meters, SM - Sqare Measures, CM - Cubic Meters."),
      '#options' => [
        'EA' => $this->t('EA - Each'),
        'LF' => $this->t('LF - Linear Feet'),
        'SF' => $this->t('SF - Square Feet'),
        'CF' => $this->t('CF - Cubic Feet'),
        'LY' => $this->t('LY - Linear Yards'),
        'SY' => $this->t('SY - Square Yards'),
        'CY' => $this->t('CY - Cubic Yards'),
        'LM' => $this->t('LM - Linear Meters'),
        'SM' => $this->t('SM - Sqare Measures'),
        'CM' => $this->t('CM - Cubic Meters'),
      ],
      '#default_value' => $row['asset_unit_measure'],
    ];

    $this->store->set('asset_documentation_id_orig', $row['asset_documentation_id']);

    $form['asset_documentation_id'] = [
      '#type'  => 'managed_file',
      '#title' => $this->t('Asset Documentation Identifier'),
      '#description' => $this->t("The identifier of a file ,stored online, 
        containing an asset's description, specification or location.
        Such a file could be a PDFP, a ZIP file containing multiple document,
        or a list of hardcopy document locatioins.  Note: only one Add/Remove
        action is supported per edit. Attempting more than one can have 
        undesirable effects."),
      // '#empty_value' => NULL,
      // '#empty_option' => t('None'),
      '#upload_location' => 'public://',
      '#default_value' => [$row['asset_documentation_id']],
      '#multiple' => FALSE,
      '#tree' => TRUE,
      '#upload_validators' => [
       'FileExtension' => ['extensions' => 'txt wpd odt ods rtf pdf doc zip jzip gz'],
      ],
    ];

    $form['asset_remarks'] = [
      '#type'  => 'textarea',
      '#title' => $this->t('Asset Remarks'),
      '#cols'  => 60,
      '#description' => t("Miscellaneous asset comments."),
      '#default_value' => $row['asset_remarks'],
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Submit'),
    // C '#prefix' => '<tr><td>',.
    // C '#suffix' => '</td></tr></table>',.
    ];

    $form['actions']['cancel'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Cancel'),
      '#submit' => ['::cancelForm'],
    // C '#prefix' => '<tr><td>',.
    // C '#suffix' => '</td></tr></table>',.
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection();
    $asset_documentation_id_orig = $this->store->get('asset_documentation_id_orig');
    $row = [];
    
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      if ((\gettype($value) === 'array')) {
        if (empty(\current($value))) {
          $value = NULL;
        }
        else {
          $value = \current($value);
        }
      }
      $row[$key] = $value;
    }

    $tid = $db->startTransaction();
    try {
     // $row['asset_id'] = 0;  
      $nbr_edited  = $db->update('asset')->fields($row)
        ->condition('asset_id', $row['asset_id'], '=')
        ->execute();
      
      if ($nbr_edited != 1) {
        $tid->rollBack();
        $this->messenger()->addError(
            $nbr_edited . \t(' rows updated, 1 expected. Nothing done.helloid = ')
                 . $row['asset_id'] ); // $row['helloid']

      }
      else {
        $asset_documentation_id_orig = $this->store->get('asset_documentation_id_orig');
      
        Routines::editManagedFiles($row['asset_documentation_id'],
              $asset_documentation_id_orig, 'asset');

        $message = $this->t("Update of asset [") . $row['asset_title'] . $this->t("] successful");
        $this->messenger()->addMessage($message);
        }
    }  
    catch (DatabaseExceptionWrapper $e) {

      $tid->rollBack();

      $this->messenger()->addMessage($this->t('Asset update Failed. Nothing done. 
        Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(), // $e->getTraceAsString()
      ]), 'error');
    }

    $this->store->delete('asset_documentation_id_orig');
    
    $form_state->setRedirect('asset.list');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('asset.list');
  }

}
