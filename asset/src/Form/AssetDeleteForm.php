<?php

namespace Drupal\asset\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;
Use Drupal\Core\Url;

/**
 * Class DefaultForm.
 *
 * @package Drupal\asset\Form\AssetDeleteForm
 *
 *  * Substitutions:
 * Tblname. Replace with Asset (init cap).
 * tblname.  Replace with asset.
 * Notes:  Apply replacements to namespace and use statements above.
 * Adjust HTML prefixes/suffixes as needed.
 * Remove underscore from Setup_Params in class, below, and $package, above.
 */
class AssetDeleteForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'asset_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('asset.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

    /**
   * {@inheritdoc}
   */
  public function buildForm(array $form,FormStateInterface $form_state,
          $asset_id = '', $asset_documentation_id = '') {

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Do you wish to delete these asset[/asset-documentation] IDs below? '),
      '#value' =>  $this->t("$asset_id" . '/' .  "$asset_documentation_id"),
      '#size' => 60,
      '#maxlength' => 128,
    ];
    
    return parent::buildForm($form, $form_state, $asset_id = '',
            $asset_documentation_id = '');

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');

    $asset_id = (integer) \Drupal::request()->attributes->get('asset_id');
    $asset_documentation_id = (integer) \Drupal::request()->query->get('asset_documentation_id');

    $tid = $db->startTransaction();
    try {
     //   $row['asset_id'] = 0; doesnt work for deletes
     $nbr_deleted = $db->delete('asset')
        ->condition('asset_id', $asset_id, '=')
        // ->condition('asset_id', '0', '=')
        ->execute();

      if ($nbr_deleted != 1) {
        $this->messenger()->addError(
           $nbr_deleted .     \t(' rows deleted, 1 expected. Nothing done.'));
               $tid->rollBack();
     }
      else {
        $this->messenger()->addMessage(\t('Asset delete successful'));
        
        if (!empty($asset_documentation_id)) {
            Routines::deleteManagedFile($asset_documentation_id, 'asset');
        }
      }
    }
    
    catch (DatabaseExceptionWrapper $e) {

      $tid->rollBack();
      $this->messenger()->addMessage($this->t('Asset delete Failed. Nothing done. 
              . Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>  $e->getTraceAsString(),
      ]), 'error');
    }
    $form_state->setRedirect('asset.list');
  }

}
