<?php

namespace Drupal\hello\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Database\Database;
// Needed for file storage;.
use Drupal\Component\Utility\Unicode;
use Drupal\common\Routines;
use Drupal\Core\Datetime\DateHelper;
use Drupal\Core\Database\DatabaseExceptionWrapper;

/**
 * Class HelloEditForm.
 *
 * @package Drupal\hello\FormHelloEditForm
 *
 * Substitutions:
 * Tblname. Replace with Hello (init cap).
 * tblname.  Replace with hello.
 * Note:  Apply replacements to namespace and use statements above.
 */
/**
 * Class HelloAddForm extends FormBase.
 */
class HelloAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hello_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['add'] = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Add a hello entry'),
      '#prefix' => '<div id="formadd" style="background-color:Cornsilk">',
      '#suffix' => '</div>',
    ];
    /*
    $form['add']['col01'] = array(
    '#type'  => 'textfield',
    '#title' => t('col01'),
    '#size'  => 20,
    '#description' => t("col01"),
    '#prefix' => '<table border=0 cellspacing=3 celladding=3><tr><td>',
    '#suffix' => '</td>',
    );
     */

    $form['add']['greeting_text'] = [
      '#type'  => 'textarea',
      '#title' => t('Greeting Text'),
      '#size'  => 20,
      '#rows' => 2,
      '#description' => t('Greeting Description'),
      '#default_value' => HelloAddForm::defaultGreeting($form_state->getValue('greeting_date'), $form_state->getValue('helloid'), $form_state->getValue('greeting_date')),
      // '#prefix' => '<table border=0 cellspacing=3 celladding=3><tr><td>',
      // '#suffix' => '</td></tr>',
    ];

    $form['add']['greeting_date'] = [
      '#type'  => 'date',
      '#title' => \t('greeting_date'),
      '#required' => FALSE,
      '#description' => \t("The most recent date on which the unit was purchased."),
      // '#prefix' => '<tr><td>',
      // '#suffix' => '</td></tr>',
    ];
    $form['add']['greeting_pic'] = [
      '#type' => 'managed_file',
      '#title' => \t('Greeting Picture'),
      '#description' => \t('Image of greeter or greetee'),
      '#upload_location' => 'public://images',
      '#default_value' => NULL,
      '#upload_validators' => [
        'FileIsImage' => [],
        'FileExtension' => ['extensions' => 'png gif jpg jpeg'],
        'FileImageDimensions' => ['maxDimensions' => 50 * 50],
      ],
      '#states' => [
      // Only show this field when the date field is filled.
        'visible' => [
          ':input[name="greeting_date"]' => [
            'fillrd' => TRUE,
          ],
        ],
      ],
        // '#prefix' => '<tr><td>',
      // '#suffix' => '</td></table>',
    ];

    $form['add']['submit'] = [
      '#type'  => 'submit',
      '#value' => t('AddMe'),
      '#prefix' => '<tr><td>',
      '#suffix' => '</td>',
    ];

    $form['add']['cancel'] = [
      '#type'  => 'submit',
      '#value' => t('Cancel'),
      '#submit' => ['::cancelForm'],
      '#prefix' => '<td>',
      '#suffix' => '</td></tr></table>',
    ];

    return $form;
  }

  /**
   * Validates a form for hello (optional).
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * Adds hello form data.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');
    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      if (\gettype($value) === 'array') {
        if (empty(\current($value))) {
          $value = NULL;
        }
        else {
          $value = \current($value);
        }
      }
      if ($key === 'greeting_date') {
        $value = empty($value) && !is_numeric($value) ? NULL : trim(strip_tags($value));

        // kint('$value=',$value);.
      }
      else {
      }
     
      $row[$key] = $value;

    }
    
    $tid = $db->startTransaction();
    try {
      $db->insert('hello')->fields($row)->execute();

      \Drupal::messenger()->addMessage('Insert successful.');

      if (!empty($row['greeting_pic'])) {
        Routines::addManagedFile($row['greeting_pic'], 'hello');
      }
    }

    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $messenger = $this->messenger();
      $messenger->addMessage($this->t('Insert Failed. Nothing done. Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>   $e->getTraceAsString(),
      ]), 'error');
    }

   $form_state->setRedirect('hello.list');
  }

  /**
   * Cancels hello form addition.
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('hello.list');
  }

  /**
   * Provides a two-line default greeting if blank and greeting_date entered.
   */
  public function defaultGreeting($greeting_date, $formkey, $greeting) {
    // $retval = t('cat got your tongue?');
    $line1 = "Greetings!";
    $line2 = "Nice day this " . DateHelper::dayOfWeekName($greeting_date,\FALSE);
    $lines = [$line1, $line2];
    $dfltgrtg = (implode("\r\n", $lines));
    // $dfltgrtg = "Cat got your tongue?" . "\n" . "Well, speak up!";
    // $retval = Unicode::convertToUtf8($dfltgrtg);
    if (!empty($greeting_date) || (empty($greeting))) {
      // If ($formkey % 2 == 0) {.
      return Unicode::convertToUtf8($dfltgrtg, NULL);
    }
    else {
      return Unicode::convertToUtf8($greeting, NULL);
    }
  }

}
