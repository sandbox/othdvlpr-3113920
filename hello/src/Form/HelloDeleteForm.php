<?php

namespace Drupal\hello\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\Core\Url;


/**
 * Class DefaultForm.
 *
 * @package Drupal\hello\Form
 */
class HelloDeleteForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hello_delete';
  }

  /**
   * Returns the question to ask the user.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The form question. The page title will be set to this value.
   */
  public function getQuestion() {
    return;
  }

  /**
   * Returns the route to go to if the user cancels the action.
   *
   * @return \Drupal\Core\Url
   *   A URL object.
   */
  public function getCancelUrl() {
    return Url::fromRoute('hello.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuery() {
    return getQuery();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $helloid = '', $greeting_pic = '') {
    $helloid = (integer) \Drupal::request()->attributes->get('helloid');
    $greeting_pic = (integer) \Drupal::request()->query->get('greeting_pic');
    
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Do you wish to delete this greeting/picture IDs below? '),
      '#value' =>  $this->t("$helloid" . '/' .  "$greeting_pic"), // 'NULL',
      '#size' => 60,
      '#maxlength' => 128,
    ];
    
    return parent::buildForm($form, $form_state, $helloid = '', $greeting_pic = '');

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection();

    $helloid = (integer) \Drupal::request()->attributes->get('helloid');
    $greeting_pic = (integer) \Drupal::request()->query->get('greeting_pic');
    
    $tid = $db->startTransaction();
    try {
      $nbr_deleted =  $db->delete('hello')
        ->condition('helloid', $helloid, '=')
        ->execute();
      
      if ($nbr_deleted != 1) {
        $this->messenger->addError(
            $nbr_deleted . \t(' rows updated, 1 expected. Nothing done.helloid = ')
                 . $helloid );
        $tid->rollBack();
     }
      else {
      if (!empty($greeting_pic)) {
        Routines::deleteManagedFile($greeting_pic, 'hello');
      }
      $this->messenger()->addMessage(\t('Greeting deleted.'));
      }
    }

    catch (DatabaseExceptionWrapper $e) {
      $tid->rollBack();
      $this->messenger()->addMessage($this->t('Delete Failed. Nothing done. Error message follows below:
              . <br />%message<br>%trace',
      [
        '%message' =>$e->getMessage(),
        '%trace' =>   $e->getTraceAsString(),
      ]), 'error');
    }

    $form_state->setRedirect('hello.list');
  }

}
