<?php

namespace Drupal\hello\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class HelloEditForm.
 *
 * @package Drupal\hello\Form\HelloEditForm
 *
 * Substitutions:
 * Tblname. Replace with Hello (init cap).
 * tblname.  Replace with hello.
 * Note:  Apply replacements to namespace and use statements above.
 * @todo. Consider moving BuildingAddForm::table_option routines.
 *
 * @var type string store
 *  Store name.
 *
 * @var type string messenger
 *  Storage for messages
 */
class HelloEditForm extends FormBase {

  /**
   * {@inheritdoc}
   */

  private $tempStoreFactory;
  /**
   * Construct temporary storage for original greeting_pic.
   *
   * @var typestringstore
   *  Store name
   */
  protected $store;

  /**
   * Storage for messages.
   *
   * @var typestringmessenger
   *  Storage for messages
   */
  protected $messenger;

  /**
   * Construct temporary store object.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $tempStoreFactory
   *   Temporary store factory.
   */
  public function __construct(
        PrivateTempStoreFactory $tempStoreFactory
    ) {
    $this->tempStoreFactory = $tempStoreFactory;
    $collection = 'collection_name';
    $this->store = $this->tempStoreFactory->get($collection);
  }

  /**
   * Create private tempstore for greeting_pic value.
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('tempstore.private')
      );
  }

  /*
   * Original greeting_pic value
   * @var type int greeting_pic_orig
   *  The original greeting_pic value
   */

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hello_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // $this->messenger()->addMessage(\t('HelloEditForm  ckpt1'));
    $helloid = \Drupal::request()->attributes->get('helloid');
    $greeting_text = \Drupal::request()->attributes->get('greeting_text');
    $greeting_pic = \Drupal::request()->attributes->get('greeting_pic');

    $db = Database::getConnection('default', 'default');
    $select = $db->select('hello', 'e')
      ->fields('e')
      ->condition('e.helloid', $helloid, '=')
      ->execute();
    $row = $select->fetchAssoc();
    // $this->messenger()->addMessage(\t('HelloEditForm  ckpt2'));
    // comment
    $form['helloid'] = [
      '#type' => 'number',
      '#disabled' => TRUE,
      '#title' => t('Choose entry to update'),
      '#value' => $helloid,
      '#description' => t("helloid"),
    ];

    $form['greeting_text'] = [
      '#type' => 'textarea',
      '#title' => t('Updated greeting_text'),
      '#size' => 20,
      '#rows' => 1,
      '#resizable' => 'none',
      '#default_value' => HelloEditForm::defaultGreeting($form_state->getValue('greeting_date'), $helloid, $row['greeting_text']),
      '#description' => t("The actual greeting"),
    ];

    $form['greeting_date'] = [
      '#type'  => 'date',
      '#title' => \t('greeting_date'),
      '#date_format' => 'Y-m-d',
      '#required' => FALSE,
      '#description' => \t("The most recent date on which the unit was purchased."),
      '#default_value' => $row['greeting_date'],

    ];

    $this->store->set('greeting_pic_orig', $row['greeting_pic']);

    $form['greeting_pic'] = [
      '#type' => 'managed_file',
      '#title' => \t('Greeting Picture'),
      '#description' => \t('Image of greeter or greetee'),
      '#upload_location' => 'public://images',
      '#multiple' => FALSE,
      '#tree' => TRUE,
      '#default_value' => [$row['greeting_pic']],
      '#upload_validators' => [
        'FileIsImage' => [],
        'FileExtension' => ['extensions' => 'png gif jpg jpeg'],
        'FileImageDimensions' => ['maxDimensions' => 50 * 50],
      ],
    ];

    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => t('Submit'),
    ];

    $form['actions']['cancel'] = [
      '#type'  => 'submit',
      '#value' => t('Cancel'),
      '#submit' => ['::cancelForm'],
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $db = Database::getConnection('default', 'default');

    $greeting_pic_orig = $this->store->get('greeting_pic_orig');

    foreach ($form_state->cleanValues()->getValues() as $key => $value) {
      if ((\gettype($value) === 'array')) {
        if (empty(\current($value))) {
          $value = 0;
        }
        else {
          $value = \current($value);
        }
      }
      if ($key === 'greeting_date') {
        $value = empty($value) && !is_numeric($value) ? NULL : trim(strip_tags($value));
      }
      /*
       * if $key = 'greeting_pic' and $value = 0 then set $value to NULL
       */
      $row[$key] = $value;
    }

    $tid = $db->startTransaction();
    try {
      $id = $db->update('hello')->fields($row)
        ->condition('helloid', $row['helloid'], '=')
        ->execute();
      $this->messenger()->addMessage(\t('Greeting updated.'));

      $greeting_pic_orig = $this->store->get('greeting_pic_orig');

      Routines::editManagedFiles($row['greeting_pic'], $greeting_pic_orig, 'hello');

    }
    catch (Exception $e) {

      $tid->rollBack();

      watchdog_exception('HelloEditFail', $e);
      \Drupal::messenger()->addError(
            \t('Update failed. Message =') . $e->getMessage .
            \t('query=') . $e->query_string
        );
    }

    $this->store->delete('greeting_pic_orig');

    $form_state->setRedirect('hello.list');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('hello.list');
  }

  /**
   * Instantiate a default greeting.*.
   */
  public function defaultGreeting($greeting_date, $formkey, $greeting) {

    $line1 = "Greetings!";
    $line2 = "Nice day this ";
    $lines = [$line1, $line2];
    $dfltgrtg = (implode("\r\n", $lines));

    if (!empty($greeting_date) || (empty($greeting))) {
      return Unicode::convertToUtf8($dfltgrtg, NULL);
    }
    else {
      return Unicode::convertToUtf8($greeting, NULL);
    }
  }

}
