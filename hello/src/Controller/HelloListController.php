<?php

namespace Drupal\hello\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Database\Database;
use Drupal\common\Routines;

/**
 * Class HelloEditForm.
 *
 * @package Drupal\hello\HelloListController
 *
 * Substitutions:
 * Tblname. Replace with Hello (init cap).
 * hello.  Replace with hello.
 * Note:  Apply replacements to namespace and use statements above.
 *         
 */
class HelloListController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {

    $db = Database::getConnection('default', 'default');
    $rows = [];
    $null = NULL;
    $header = [
      [
        'data' => t('helloid'),
        'field' => 'e.helloid',
        'sort' => 'asc',
      ],
      ['data' => \t('Greeting_text'), 'field' => 'e.greeting_text'],
      ['data' => \t('Greeting_date'), 'field' => 'e.greeting_date'],
      ['data' => \t('Greeting_pic'), 'field' => 'e.greeting_pic'],
      ['data' => \t('Ed/Del'), 'field' => $null],
    ];

    $select = $db->select('hello', 'e')
      ->fields('e')
      ->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header)
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit()
      ->execute();

    foreach ($select as $row) {

      $helloid = (string) $row->helloid;
      $greeting_pic = (integer) $row->greeting_pic;
      $greeting_pic_int = (integer) $row->greeting_pic;
      $row->greeting_pic = Routines::displayManagedFileImage($greeting_pic);

      $row->helloid = Link::fromTextAndUrl(
            $helloid,
            Url::fromRoute(
                'hello.edit',
                ['helloid' => $helloid, 'greeting_pic' => $greeting_pic_int]
            )
              )->toString();

      $row->eddel = Link::fromTextAndUrl(
            'Delete',
            Url::fromRoute(
                'hello.delete',
                ['helloid' => $helloid, 'greeting_pic' => $greeting_pic_int]
            )
        )->toString();

      $rows[] = ['data' => (array) $row];

    }

    $build[] = [
      '#type' => 'link',
      '#title' => t('Add'),
      '#url' => Url::fromRoute('hello.add'),
    ];

    $build['hello_list_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => "The list is empty.  Please add data",
      '#sticky' => \TRUE,
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    $build['#cache']['max-age'] = 0;

    return $build;

  }

}
